#! /usr/bin/python3

#####################################
#### Count Delta Atom Composition ###
#### Written by Boshen Wang #########
#####################################


import os


# dictionary Residue Tri_Letter => One Letter

dic_single_trio = {
    "A": "ALA",
    "R": "ARG", 
    "N": "ASN", 
    "D": "ASP", 
    "C": "CYS",
    "Q": "GLN",
    "E": "GLU", 
    "G": "GLY",
    "H": "HIS", 
    "I": "ILE",
    "L": "LEU",
    "K": "LYS",
    "M": "MET",
    "F": "PHE",
    "P": "PRO",
    "S": "SER",
    "T": "THR",
    "W": "TRP",
    "Y": "TYR",
    "V": "VAL", 
}


list_amino_acids = [
    "A",
"R",
"N",
"D",
"C",
"Q",
"E",
"G",
"H",
"I",
"L",
"K",
"M",
"F",
"P",
"S",
"T",
"W",
"Y",
"V"
]


# the dictionary residue_type[key] ==> heavy atoms[value]
# Add C terminal (OXT)
# residue type as string, heavy atoms as tuples
dic_residue_heavyatom = {
    "ALA": ( "CB"),
    "ARG": ( "CB", "CG", "CD", "NE", "CZ", "NH1", "NH1"),
    "ASN": ( "CB", "CG", "OD1", "ND2"),
    "ASP": ( "CB", "CG", "OD1", "OD2"),
    "CYS": ( "CB", "SG"),
    "GLN": ( "CB", "CG", "CD", "OE1", "NE2"),
    "GLU": ( "CB", "CG", "CD", "OE1", "OE2"),
    "GLY": ( ),
    "HIS": ( "CB", "CG", "ND1", "CD2", "CE1", "NE2"),
    "ILE": ( "CB", "CG1", "CG2", "CD1"),
    "LEU": ( "CB", "CG", "CD1", "CD2"),
    "LYS": ( "CB", "CG", "CD", "CE", "NZ"),
    "MET": ( "CB", "CG", "SD", "CE"),
    "PHE": ( "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ"),
    "PRO": ( "CB", "CG", "CD"),
    "SER": ( "CB", "OG"),
    "THR": ( "CB", "OG1", "CG2"),
    "TRP": ( "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2"),
    "TYR": ( "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH"),
    "VAL": ( "CB", "CG1", "CG2"),
}


dic_residue_countC = {
    "ALA": 0,
    "ARG": 3,
    "ASN": 1, 
    "ASP": 1, 
    "CYS": 0,
    "GLN": 2,
    "GLU": 2, 
    "GLY": 0,
    "HIS": 3,
    "ILE": 3,
    "LEU": 3,
    "LYS": 3,
    "MET": 2,
    "PHE": 6,
    "PRO": 2,
    "SER": 0,
    "THR": 1,
    "TRP": 8,
    "TYR": 6,
    "VAL": 2,
}

dic_residue_countN = {
    "ALA": 0,
    "ARG": 3,
    "ASN": 1,
    "ASP": 0,
    "CYS": 0,
    "GLN": 1,
    "GLU": 0,
    "GLY": 0,
    "HIS": 2,
    "ILE": 0,
    "LEU": 0,
    "LYS": 1,
    "MET": 0,
    "PHE": 0,
    "PRO": 0,
    "SER": 0,
    "THR": 0,
    "TRP": 1,
    "TYR": 0,
    "VAL": 0,
}

dic_residue_countO = {
    "ALA": 0,
    "ARG": 0,
    "ASN": 1,
    "ASP": 2,
    "CYS": 0,
    "GLN": 1,
    "GLU": 2,
    "GLY": 0,
    "HIS": 0,
    "ILE": 0,
    "LEU": 0,
    "LYS": 0,
    "MET": 0,
    "PHE": 0,
    "PRO": 0,
    "SER": 1,
    "THR": 1,
    "TRP": 0,
    "TYR": 1,
    "VAL": 0,
}

dic_residue_countS = {
    "ALA": 0,
    "ARG": 0,
    "ASN": 0,
    "ASP": 0,
    "CYS": 1,
    "GLN": 0,
    "GLU": 0,
    "GLY": 0,
    "HIS": 0,
    "ILE": 0,
    "LEU": 0,
    "LYS": 0,
    "MET": 1,
    "PHE": 0,
    "PRO": 0,
    "SER": 0,
    "THR": 0,
    "TRP": 0,
    "TYR": 0,
    "VAL": 0,
}

## Need to dealing with GLY 
## Since backbone
dic_residue_BKcountC = {
    "ALA": 1,
    "ARG": 1,
    "ASN": 1,
    "ASP": 1,
    "CYS": 1,
    "GLN": 1,
    "GLU": 1,
    "GLY": 0,
    "HIS": 1,
    "ILE": 1,
    "LEU": 1,
    "LYS": 1,
    "MET": 1,
    "PHE": 1,
    "PRO": 1,
    "SER": 1,
    "THR": 1,
    "TRP": 1,
    "TYR": 1,
    "VAL": 1,
}


def Atom_Compos_Change(ref_res, mut_res):

    ref_res_inTri = dic_single_trio [ref_res]
    mut_res_inTri = dic_single_trio [mut_res]

    delta_BK_C = dic_residue_BKcountC [mut_res_inTri] -\
                 dic_residue_BKcountC [ref_res_inTri]
    delta_SC_C = dic_residue_countC [mut_res_inTri] -\
                 dic_residue_countC [ref_res_inTri]
    delta_SC_N = dic_residue_countN [mut_res_inTri] -\
                 dic_residue_countN [ref_res_inTri]
    delta_SC_O = dic_residue_countO [mut_res_inTri] -\
                 dic_residue_countO [ref_res_inTri]
    delta_SC_S = dic_residue_countS [mut_res_inTri] -\
                 dic_residue_countS [ref_res_inTri]

    
    out_entry = ref_res + ',' + mut_res + ',' +\
                str(delta_BK_C) + ',' + str(delta_SC_C) + ',' +\
                str(delta_SC_N) + ',' + str(delta_SC_O) + ',' +\
                str(delta_SC_S)
    
    print(out_entry)

    return 0

for i in range(0,20):
    for j in range(0,20):
        if i != j:
            Atom_Compos_Change(list_amino_acids[i], list_amino_acids[j]) 