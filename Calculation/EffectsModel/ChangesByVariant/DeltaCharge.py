
dic_single_trio = {
    "A": "ALA",
    "R": "ARG", 
    "N": "ASN", 
    "D": "ASP", 
    "C": "CYS",
    "Q": "GLN",
    "E": "GLU", 
    "G": "GLY",
    "H": "HIS", 
    "I": "ILE",
    "L": "LEU",
    "K": "LYS",
    "M": "MET",
    "F": "PHE",
    "P": "PRO",
    "S": "SER",
    "T": "THR",
    "W": "TRP",
    "Y": "TYR",
    "V": "VAL", 
}



dic_residue_charge = {
    "ALA": 0,
    "ARG": 1,
    "ASN": 0,
    "ASP": -1,
    "CYS": 0,
    "GLN": 0,
    "GLU": -1,
    "GLY": 0,
    "HIS": 1,
    "ILE": 0,
    "LEU": 0,
    "LYS": 1,
    "MET": 0,
    "PHE": 0,
    "PRO": 0,
    "SER": 0,
    "THR": 0,
    "TRP": 0,
    "TYR": 0,
    "VAL": 0,
}


def DeltaCharge(wild_single, mutated_single):

    wild_tri = dic_single_trio [wild_single]
    mutated_tri = dic_single_trio [mutated_single]

    change_charge = dic_residue_charge [mutated_tri] -\
                    dic_residue_charge [wild_tri]
    
    out_entry = wild_single + ',' + mutated_single + ',' + str(change_charge)
    print(out_entry)
    
    return change_charge


list_amino_acids = [
"A",
"R",
"N",
"D",
"C",
"Q",
"E",
"G",
"H",
"I",
"L",
"K",
"M",
"F",
"P",
"S",
"T",
"W",
"Y",
"V"
]


for i in range(0,20):
    for j in range(0,20):
        if i != j:
            DeltaCharge(list_amino_acids[i], list_amino_acids[j]) 