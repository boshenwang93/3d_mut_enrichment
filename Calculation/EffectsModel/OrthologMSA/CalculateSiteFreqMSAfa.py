#! /usr/bin/python3

import re
import os
import math
from multiprocessing import Process, Pool

###########################################################
#### From aligned MSA fasta, Obtain site-specific Freq ####
###########################################################

# list for Amino Acid One Letter including gap '-'
list_amino_acid = [
    "A",
    "R",
    "N",
    "D",
    "C",
    "Q",
    "E",
    "G",
    "H",
    "I",
    "L",
    "K",
    "M",
    "F",
    "P",
    "S",
    "T",
    "W",
    "Y",
    "V",
    # Gap, Unknown, (E,Q), (D,N), Selenocysteine
    "-", "X", "Z", "B", "U"
]


def ObtainFakeSiteFreq(aligned_fasta_file):

    # initalize hash table for fake_site_index Residue => Count
    dic_fakeindexRes_count = {}
    count_homologs = 0

    # Pass the file content to a string
    file_content = ''
    f = open(aligned_fasta_file, 'r')
    for line in f:
        file_content += line
    f.close()
    file_content = file_content.upper()

    # Split the file content to each sequence
    list_seqs = re.split('>', file_content)
    count_homologs += len(list_seqs) - 1

    # Iterate every seq to obtain actual aligned sequence list
    list_actual_aligned_sequence = []

    for j in range(1, len(list_seqs)):
        each_seq_w_header = list_seqs[j]
        list_lines = re.split("\n", each_seq_w_header)
        # sequence ID header
        header = list_lines[0]
        # actual sequence
        actual_sequence = ''
        for i in range(1, len(list_lines)):
            actual_sequence += list_lines[i]
        list_actual_aligned_sequence.append(actual_sequence)

    fake_length = len(list_actual_aligned_sequence[1])

    # Iterate every site to count the AminoAcid frequency
    for i in range(0, fake_length):
        actual_fake_index = i + 1

        for j in range(0, 25):
            current_AA = list_amino_acid[j]
            freq = 0

            for k in range(0, count_homologs):
                current_seq = list_actual_aligned_sequence[k]

                if current_seq[i] == current_AA:
                    freq += 1

            residue_ID = str(actual_fake_index) + '_' + current_AA
            dic_fakeindexRes_count[residue_ID] = freq

    return dic_fakeindexRes_count, count_homologs

#######################################
## Map Fake Index to Query Index ######
#######################################
def MapFakeindexQueryindex(aligned_fasta_file, query_ref_sequence_ID):

    # initalize hash table for query_index  => fake_index
    dic_queryindex_fakeindex = {}

    # Pass the file content to a string
    file_content = ''
    f = open(aligned_fasta_file, 'r')
    for line in f:
        file_content += line
    f.close()
    file_content = file_content.upper()

    # Split the file content to each sequence
    list_seqs = re.split('>', file_content)

    # Iterate every seq to obtain actual query sequence
    actual_query_sequence = ''
    for j in range(1, len(list_seqs)):
        each_seq_w_header = list_seqs[j]
        list_lines = re.split("\n", each_seq_w_header)
        
        #####!!!! OPTIONS here
        ## 1. sequence ID header in trimmed format
        header = list_lines[0]
        # print(header)

        # 2. sequence ID header in full uniprot format 
        # header_full = list_lines[0]
        # header = header_full.split("|")[1].strip()

        # actual sequence
        actual_sequence = ''
        for i in range(1, len(list_lines)):
            actual_sequence += list_lines[i]

        if header == query_ref_sequence_ID:
            actual_query_sequence += actual_sequence
    # Done

    fake_length = len(actual_query_sequence)

    # obtain hash table query_index => fake_index
    for i in range(0, fake_length):
        actual_fake_index = i + 1

        count_gaps = 0
        # count how many gaps before current position
        for j in range(0, i):
            if actual_query_sequence[j] == '-':
                count_gaps += 1
            else:
                pass

        actual_query_index = actual_fake_index - count_gaps

        if actual_query_index != 0:
            key_text = 'query' + str(actual_query_index)
            value_text = str(actual_fake_index)
            dic_queryindex_fakeindex[key_text] = value_text

    return dic_queryindex_fakeindex


#######################################
## Allocate Freq to QuerySeq Index#####
#######################################

def ConvertToQueryIndexFreq(dic_fakeindex_freq, dic_queryindex_fake):
    # Initalize the hash table for query_index_residue => freq
    dic_queryIndexRes_freq = {}

    for query_index, fake_index in dic_queryindex_fake.items():
        for i in range(0, 25):
            query_index_Residue = query_index + '_' + list_amino_acid[i]
            fake_index_Residue = fake_index + '_' + list_amino_acid[i]
            frequency_value = dic_fakeindex_freq[fake_index_Residue]

            dic_queryIndexRes_freq[query_index_Residue] = frequency_value

    return dic_queryIndexRes_freq

###########################################################
#### Calculate site-specific Score is quite Ad-hoc ########
#### Adopt two simple measurement #########################
#### Valdar, W.S., 2002. Scoring residue conservation. ####
###########################################################

#######################################
#### Calculate Site Entropy ###########
#######################################

def CalculateSiteEntropy(dic_queryindexres_freq):

    # initalize the hash table queryindex => site entropy  Sum(P*logP)
    dic_queryindex_entropy = {}
    dic_queryindexres_prob = {}

    list_queryindex = []
    dic_queryindex_actualAAcount = {}

    # To obtain the list of query index
    for queryindexres, frequency_count in dic_queryindexres_freq.items():
        queryindex = re.split('_', queryindexres)[0].strip()
        if queryindex not in list_queryindex:
            list_queryindex.append(queryindex)
    # Done

    # Obtain the actual AA count in each site
    for each_queryindex in list_queryindex:
        actual_AA_count = 0

        for queryindexres, frequency_count in dic_queryindexres_freq.items():
            queryindex = re.split('_', queryindexres)[0].strip()
            if queryindex == each_queryindex:
                for i in range(0, 20):
                    selected_AA = each_queryindex + '_' + list_amino_acid[i]
                    if queryindexres == selected_AA:
                        actual_AA_count += frequency_count
        dic_queryindex_actualAAcount[each_queryindex] = actual_AA_count
    # Done

    # calculate the probability for each site residue
    for queryindexres, frequency_count in dic_queryindexres_freq.items():
        queryindex = re.split('_', queryindexres)[0].strip()
        probability = frequency_count / \
            dic_queryindex_actualAAcount[queryindex]
        dic_queryindexres_prob[queryindexres] = probability
    # Done

    # Calculate Site-specific Entropy
    for each_queryindex in list_queryindex:
        entropy = 0

        for queryindexres, prob in dic_queryindexres_prob.items():
            queryindex = re.split('_', queryindexres)[0].strip()
            if queryindex == each_queryindex:
                for i in range(0, 20):
                    selected_AA = each_queryindex + '_' + list_amino_acid[i]
                    if queryindexres == selected_AA:
                        if prob != 0:
                            entropy -= math.log(prob) * prob
        dic_queryindex_entropy[each_queryindex] = entropy
    # Done

    return dic_queryindex_entropy, dic_queryindexres_prob


# function which combine the above and output

def Merge(file_abs_path):

    try:
        out_folder = '/home/bwang/data/cancer/calculate/MSA/homology/VALUE/'

        if file_abs_path.endswith('.fasta'):
            file_name = re.split('/', file_abs_path)[-1]

            humrefUniprot = file_name.split('_')[0].strip()

            out_entropy_file = out_folder + file_name + '.entropy'
            out_f1 = open(out_entropy_file, 'w')
            out_prob_file = out_folder + file_name + '.prob'
            out_f2 = open(out_prob_file, 'w')

            dic_FakeIndexRes_Count, count_sequences =\
                ObtainFakeSiteFreq(aligned_fasta_file= file_abs_path)
            # print(dic_FakeIndexRes_Count, count_sequences)

            dic_QueryIndex_FakeIndex =\
                MapFakeindexQueryindex(aligned_fasta_file= file_abs_path,
                                        query_ref_sequence_ID=humrefUniprot)
            # print(dic_QueryIndex_FakeIndex)
            
            dic_QueryIndex_Freq =\
                ConvertToQueryIndexFreq(dic_fakeindex_freq=dic_FakeIndexRes_Count,
                                        dic_queryindex_fake=dic_QueryIndex_FakeIndex)
            # print(dic_QueryIndex_Freq)

            dic_QueryIndex_Entropy, dic_QueryIndexRes_Prob =\
                CalculateSiteEntropy(
                    dic_queryindexres_freq=dic_QueryIndex_Freq)

            for k, v in dic_QueryIndex_Entropy.items():
                index = k[5:]
                out_line_entropy = humrefUniprot + \
                    ',' + index + ',' + str(v) + "\n"
                out_f1.write(out_line_entropy)

            for k, v in dic_QueryIndexRes_Prob.items():
                residue = re.split("_", k)[1]
                query_raw = re.split("_", k)[0]
                index = query_raw[5:]
                out_line_prob = humrefUniprot + ',' + index + ',' + residue + ',' +\
                    str(v) + "\n"
                out_f2.write(out_line_prob)

            out_f1.close()
            out_f2.close()
    except:
        print(file_abs_path)

    return 0


# Merge(file_abs_path =
#       '/home/bwang/Downloads/chengxin/MSA/Q8IY17.fasta')

def main():
    list_MSA_file = []
     
    in_folder = '/home/bwang/data/cancer/calculate/MSA/homology/iden30_group/'

    for root, subdir_list, file_list in os.walk(in_folder):
        for each_file in file_list:
            if each_file.endswith('.fasta'):
                each_file_abs_path = root + each_file
                list_MSA_file.append(each_file_abs_path)
    
    print(list_MSA_file)
    # for each_MSA_file in list_MSA_file:
    #     try:
    #         Merge(each_MSA_file)
    #     except:
    #         print(each_MSA_file)
    
    with Pool(processes= 28 ) as pool:
        pool.map(Merge, list_MSA_file)

    return 0


if __name__ == '__main__':
    main()
