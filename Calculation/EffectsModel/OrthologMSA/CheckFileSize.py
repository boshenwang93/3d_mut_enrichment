import os
import argparse
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter


###########################################################
#### Check the file size and find Outlier #################
#### Plot the histogram ###################################
###########################################################

## Setting the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input folder files need to check Size')
args = parser.parse_args()

input_dir = args.indir


list_size = [] 


dic_size_file = {} 
for root, subdir_list, file_list in os.walk(input_dir):
    os.chdir(root)
    for each_file in file_list:
        tmp_file_size = os.path.getsize(each_file)
        tmp_size_MB = tmp_file_size / (1024*1025)
        list_size.append(tmp_size_MB)
        dic_size_file [tmp_size_MB] = each_file

list_size.sort()
print(list_size)


for i in range(-10, 0):
    size_value = list_size[i]
    print(dic_size_file[size_value])

num_bins = 15
n, bins, patches = plt.hist(list_size, num_bins, facecolor='blue', alpha=0.5)
plt.show()