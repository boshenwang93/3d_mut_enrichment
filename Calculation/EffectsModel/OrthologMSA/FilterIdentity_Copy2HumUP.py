import re 
import os 
import argparse
from multiprocessing import Process, Pool
from collections import defaultdict 

###########################################################
#### A Quick filtering based on identity ##################
###########################################################
#### Setting the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--incsv', type=str,
                    help = 'input Identity CSV File')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output dir contain Raw Group fasta')
parser.add_argument('-q', '--querydir', type=str,
                    help = 'dir contain Query sequence fasta')
parser.add_argument('-d', '--dbdir', type=str,
                    help = 'dir contain candidate sequence fasta')
parser.add_argument('-c', '--cutoff', type=str,
                    help = 'cut-off value for identity')
args = parser.parse_args()

input_identity_CSV = args.incsv 
output_group_fasta_dir = args.outdir 
query_fasta_dir = args.querydir
database_fasta_dir = args.dbdir
threshold = float(args.cutoff)




#####################################
list_uniprotHum = []

f2 = open(input_identity_CSV, 'r')
for line in f2:
    uniprot_Human = re.split(',', line)[0]
    uniprot_Human = re.split('>', uniprot_Human)[1]
    if uniprot_Human not in list_uniprotHum:
        list_uniprotHum.append(uniprot_Human)
        print(len(list_uniprotHum))
f2.close()

print("unique human query is done")

dic_HumanUniprot_listNonHumUniprot = defaultdict (list)
f_in = open(input_identity_CSV, 'r') 
for line in f_in:
    uniprot_Human = re.split(',', line)[0]
    uniprot_Human = re.split('>', uniprot_Human)[1]

    uniprot_nonHum = re.split(',', line)[1]
    uniprot_nonHum = re.split('>', uniprot_nonHum)[1]

    iden_seq1 = re.split(',', line)[2].strip()
    iden_seq1 = float(iden_seq1)
    iden_seq2 = re.split(',', line)[3].strip()
    iden_seq2 = float(iden_seq2)

    # source_fasta = database_fasta_dir + uniprot_nonHum + '.fasta'
    # dest_fasta = assigned_new_dir 
            
    if iden_seq1 >= threshold:
        if iden_seq2 >= threshold:
            dic_HumanUniprot_listNonHumUniprot [uniprot_Human].append(uniprot_nonHum)
            # cp2_cmd = 'cp ' + source_fasta + '  ' + dest_fasta + "\n"
            # os.system(cp2_cmd)
f_in.close()

print("hash table for including sequence ID is done")



for each_uniprotHum in list_uniprotHum:
    assigned_new_dir = output_group_fasta_dir  + each_uniprotHum + '/'
    mkdir_cmd = 'mkdir ' + assigned_new_dir
    os.system(mkdir_cmd)
    
    source_fasta = query_fasta_dir + each_uniprotHum + '.fasta'
    dest_fasta = assigned_new_dir 

    cp1_cmd = 'cp ' + source_fasta + '  ' + dest_fasta + "\n"
    os.system(cp1_cmd)

    list_nonHum = dic_HumanUniprot_listNonHumUniprot [each_uniprotHum]
    for each_nonHum in list_nonHum:
        source_fasta = database_fasta_dir + each_nonHum + '.fasta'

        cp2_cmd = 'cp ' + source_fasta + '  ' + dest_fasta + "\n"
        os.system(cp2_cmd)

