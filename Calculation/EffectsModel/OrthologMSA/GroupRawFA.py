#! /usr/bin/python3 

import re 
import os


for root, subdir_list, file_list in os.walk('/home/bwang/data/cancer/calculate/MSA/homology/rawgroup/'):
    for each_subdir in subdir_list:
        subdir_abs = os.path.join(root, each_subdir)

        seq_combined_content = ''

        for root2, subdir_list2, file_list2 in os.walk(subdir_abs):
            for each_file in file_list2:
                os.chdir(subdir_abs)
                if each_file.endswith('.fasta'):
                    f = open(each_file, 'r')
                    for line in f:
                        seq_combined_content += line 
                    f.close()

                    seq_combined_content += "\n"
        
        new_file_abs =  '//home/bwang/data/cancer/calculate/MSA/homology/iden30_group/' + each_subdir + '_Iden30.fasta'
        out_f = open(new_file_abs, 'w')
        out_f.write(seq_combined_content)
        out_f.close()

