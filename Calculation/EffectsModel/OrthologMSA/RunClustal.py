#! /usr/bin/python3

import os
import argparse 
from multiprocessing import Process, Pool


#### Set the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input Parent dir containing Raw Pairwise fasta file')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output Parent dir for aliged pairwise fasta file')
parser.add_argument('-t', '--thread', type=int,
                    help = 'assign how many threads')
args = parser.parse_args()

input_raw_dir = args.indir
output_aligned_dir = args.outdir
count_threads = args.thread



#### Run clustalW for alignment
def RunClustw(file_abs_path):

    input_fasta = file_abs_path
    
    relative_path = input_fasta.split('/')[-2].strip()

    ## For need of sub-folders 
    # out_son_dir = output_aligned_dir + relative_path + '/'
    # ## Check new dir exist or not 
    # if os.path.isdir(out_son_dir) == 0:
    #     cmd_mkdir = 'mkdir ' + out_son_dir
    #     os.system(cmd_mkdir)
    # else:
    #     pass
    # output_fasta = out_son_dir +  input_fasta.split('/')[-1].strip()
    

    #### For need of No subfolders
    output_fasta = output_aligned_dir + \
                   input_fasta.split('/')[-1].strip()

    ## Traditional clustalW2 options 
    W2_cmd = 'clustalw -TYPE=PROTEIN -OUTPUT=FASTA -PWMATRIX=BLOSUM ' +\
          ' -INFILE=' + input_fasta +\
          ' -OUTFILE=' + output_fasta 
    
    OMEGA_cmd = 'clustalo  --threads=1  ' +\
        ' --infile=' + input_fasta +\
        ' --outfile=' + output_fasta 
    
    print(OMEGA_cmd)
    os.system(OMEGA_cmd)

    #print(W2_cmd)
    # os.system(W2_cmd)
    
    return 0


def main():
    list_file_abs = []
    
    #### For Pair in Son folders 
    # for root, subdir_list, file_list in os.walk(input_raw_dir):
    #     for each_subdir in subdir_list:
    #         subdir_abs = input_raw_dir + each_subdir + '/'
    #         for root2, subdir_list2, file_list2 in os.walk(subdir_abs):
    #             for each_file in file_list2:
    #                 if each_file.endswith('.fasta'):
    #                     raw_fasta_abs = subdir_abs + each_file
    #                     list_file_abs.append(raw_fasta_abs)

    #### For Group in One folder without any sub folders 
    for root, subdir_list, file_list in os.walk(input_raw_dir):
        for each_file in file_list:
            if each_file.endswith('.fasta'):
                raw_fasta_abs = root + each_file
                list_file_abs.append(raw_fasta_abs)

    with Pool(processes= count_threads ) as pool:
        pool.map(RunClustw, list_file_abs)

    return 0


if __name__ == '__main__':
    main()