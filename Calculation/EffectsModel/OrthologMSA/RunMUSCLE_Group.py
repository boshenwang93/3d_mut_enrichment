#! /usr/bin/python3

import os
import argparse 
from multiprocessing import Process, Pool


#### Set the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input dir containing Raw Grouped fasta file')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output dir for aliged Grouped fasta file')
parser.add_argument('-t', '--thread', type=int,
                    help = 'assign how many threads')
args = parser.parse_args()

input_raw_dir = args.indir
output_aligned_dir = args.outdir
count_threads = args.thread



#### Run MUSCLE for alignment in large quantity
def RunMUSCLE(file_abs_path):

    input_fasta = file_abs_path
    
    output_fasta = output_aligned_dir +  input_fasta.split('/')[-1].strip()

    cmd = 'muscle3.8.31_i86linux64 ' +\
          ' -in ' + input_fasta +\
          ' -out ' + output_fasta
    # print(cmd)
    os.system(cmd)

    return 0


def main():
    list_file_abs = []
    
    for root, subdir_list, file_list in os.walk(input_raw_dir):
        for each_file in file_list:
            if each_file.endswith('.fasta'):
                raw_fasta_abs = root + each_file
                list_file_abs.append(raw_fasta_abs)
                # print(raw_fasta_abs)
      
    with Pool(processes= count_threads ) as pool:
        pool.map(RunMUSCLE, list_file_abs)

    return 0


if __name__ == '__main__':
    main()