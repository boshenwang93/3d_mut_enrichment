#! /usr/bin/python3

import os
import argparse


###########################################################
#### Calculate two-way identity from Fasta ################
#### Modified by B.W. May 2020  ###########################
###########################################################

#### Set the parser parameter
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input dir containing aligned pairwise fasta file')
parser.add_argument('-o', '--outcsv', type=str,
                    help = 'output csv file abs path recording identity')
parser.add_argument('-l', '--log', type=str,
                    help = 'log file path')
args=parser.parse_args()

aligned_dir = args.indir
csv_path = args.outcsv
log_path = args.log


#### Obtain basic information from aligned fasta file
#### Two-way identity, coverage segment index 
def ReadIdentity(alignedFasta_file):
    uniprotID1 = ''
    uniprotID2 = ''

    with open(alignedFasta_file) as f:
        all_context = f.read()
    f.close()

    list_slices = all_context.split('>')

    seq1_w_header = '>' + list_slices[1]
    seq2_w_header = '>' + list_slices[2]

    list_lines_seq1 = seq1_w_header.split("\n")
    list_lines_seq2 = seq2_w_header.split("\n")

    seq1_context = ''
    seq2_context = ''

    for eachline in list_lines_seq1:
        if eachline.startswith('>') == 0:
            if eachline != '':
                seq1_context += eachline
        else:
            uniprotID1 += eachline

    for eachline in list_lines_seq2:
        if eachline.startswith('>') == 0:
            if eachline != '':
                seq2_context += eachline
        else:
            uniprotID2 += eachline

    # check length equal
    m = len(seq1_context)
    n = len(seq2_context)

    # count identity
    gaps_seq1 = 0
    gaps_seq2 = 0
    identity_chars = 0

    list_match_index_seq1 = []
    list_match_index_seq2 = []

    for i in range(0, m, 1):        
        
        if seq1_context[i] == '-':
            gaps_seq1 += 1

        if seq2_context[i] == '-':
            gaps_seq2 += 1

        if seq1_context[i] == seq2_context[i]:
            identity_chars += 1
    ## 

    print(gaps_seq1, gaps_seq2)


    # without gap, the length of sequence
    seq1_wo_gap = m - gaps_seq1
    seq2_wo_gap = n - gaps_seq2
    # calculate the identity of sequence
    seq1_identity = identity_chars / seq1_wo_gap
    seq2_identity = identity_chars / seq2_wo_gap
    

    seq_identity_info = uniprotID1 + ',' +\
        uniprotID2 + ',' +\
        str(seq1_identity) + ',' +\
        str(seq2_identity)

    return seq_identity_info


#### iterate the aligned fasta folder 
def main(out_file_PATH, aligned_fasta_folder, log_file):
    # initialize the file handle
    out_F = open(out_file_PATH, 'w')
    log_F = open(log_file, 'w')

    # iterate all the aligned file from aligned folder
    for root, subdir_list, file_list in os.walk(aligned_fasta_folder):
        for each_dir in subdir_list:
            subdir_abs = os.path.join(root, each_dir)
            for root2, subdir_list2, file_list2 in os.walk(subdir_abs):
                for each_file in file_list2:
                    try:
                        if each_file.endswith('.fasta'):
                            each_file_abs_path = os.path.join(subdir_abs, each_file)

                            alignment_identity_info = ReadIdentity(
                                alignedFasta_file=each_file_abs_path)
                            ## For General Purpose 
                            out_F.write(alignment_identity_info)
                            out_F.write("\n")

                    except:
                        error_entry = each_file + ' has issue' + "\n"
                        log_F.write(error_entry)

    out_F.close()
    log_F.close()

    return 0


if __name__ == '__main__':
    main(out_file_PATH = csv_path,
         aligned_fasta_folder = aligned_dir,
         log_file = log_path)