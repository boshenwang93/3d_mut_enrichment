import os 


origin_path = '/home/bwang/data/cancer/mapPDB/ensemblFasta'
dest_folder = '/home/bwang/data/cancer/calculate/MSA/includefasta'

f = open('/home/bwang/data/cancer/gene_refIsoform_refStructure/partPDB_gene_isoform.csv')
for line in f:
    uniprot = line.split(',')[1]
    for root, subdirs, files in os.walk(origin_path):
        for each_file in files:
            if each_file.startswith(uniprot):
                orig_fasta = os.path.join(origin_path,each_file)
                cp_cmd = 'cp ' + orig_fasta + "\t" + dest_folder
                os.system(cp_cmd)
f.close()
