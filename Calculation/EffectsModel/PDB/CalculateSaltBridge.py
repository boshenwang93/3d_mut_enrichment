#! /usr/bin/python3

import re
import math
import os
from multiprocessing import Process, Pool

###########################################################
#### According to Refined Conformation ####################
#### !!!!! HIS => HSE #####################################
#### XPLOR Format for Atoms ###############################
#### Calculate Salt bridge  ###############################
#### Work for Monomer #####################################
#### Written by Boshen Wang ###############################
###########################################################

###########################################################
#### Structure of the Script ##############################
#### 1. Obtain Oxygen in Acidic, Nitrogen in Basic ########
#### 2. Function of Distance calculation ##################
#### 3. Search the salt bridge for each residue ##########
###########################################################


###########################################################
#### Restricted acidic/basic residue ######################
###########################################################

restrict_acidic_residue = [
    'ASP',
    'GLU',
    'CTR'
]

restrict_basic_residue = [
    'ARG',
    'HIS',
    'HSE'
    'LYS',
    'NTR'
]

# Oxygen in side chain of acidic residues
restrict_acidic_oxygen = ["ASP_OD1", "ASP_OD2",
                          "GLU_OE1", "GLU_OE2",
                          "CTR_OT1", "CTR_OT2"]

# Nitrogen atom in side chain of basic residues
restrict_basic_nitrogen = ["ARG_NE", "ARG_NH1", "ARG_NH2",
                           # "HSE_ND1", "HSE_NE2", 
                           'HIS_ND1', 'HIS_NE2',
                           'HSE_ND1', 'HSE_NE2',
                           "LYS_NZ",
                           "NTR_N"]

###########################################################
#### Expanded acidic/basic residue ########################
###########################################################

expand_acidic_residue = [
    'ASP',
    'GLU',
    'TYR',
    'CYS',
    'CTR'
]

expand_basic_residue = [
    'ARG',
    'HSE', 'HIS',
    'LYS',
    'NTR'
]

# Oxygen in side chain of acidic residues
expand_acidic_oxygen = ["ASP_OD1", "ASP_OD2",
                        "GLU_OE1", "GLU_OE2",
                        "TYR_OH",
                        "CYS_SG",
                        "CTR_OT1", "CTR_OT2"]

# Nitrogen atom in side chain of basic residues
expand_basic_nitrogen = ["ARG_NE", "ARG_NH1", "ARG_NH2",
                         "HSE_ND1", "HSE_NE2", 'HIS_ND1', 'HIS_NE2',
                         "LYS_NZ",
                         "NTR_N"]

# INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

# INDEX list_residue
# [0] => residue tri letter
# [1] => sequence number
# [2] => chain ID


def RetrieveXYZ(pdb_file_path):

    # list for interested residues
    list_charged_residue = []

    # list for nitrogen/oxygen atoms 
    list_oxygen_acidic = []
    list_nitrogen_basic = []

    # list for residue sequence number/index
    list_sequence_index = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:
        if line.startswith("ATOM"):
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            #### Fetch the interesting Polar Residue record
            temp_residue_entry = residue + "," + residue_seq_number + "," + chain
            if residue in restrict_acidic_residue:
                if temp_residue_entry not in list_charged_residue:
                    list_charged_residue.append(temp_residue_entry)
            elif residue in restrict_basic_residue:
                if temp_residue_entry not in list_charged_residue:
                    list_charged_residue.append(temp_residue_entry)
            else:
                pass

            #### Fetch the interesting Polar Atom record
            temp_heavy_atom_entry = residue + "," + residue_seq_number + \
                "," + chain + "," + atom + "," + x + "," + y + "," + z

            # identify residue + atom Type
            res_atom_type = residue + "_" + atom 

            if res_atom_type in restrict_acidic_oxygen:
                list_oxygen_acidic.append(temp_heavy_atom_entry)
            elif res_atom_type in restrict_basic_nitrogen:
                list_nitrogen_basic.append(temp_heavy_atom_entry)
            else:
                pass

            #### push the sequence number/index to list
            residue_seq_number = int(residue_seq_number)
            if residue_seq_number not in list_sequence_index:
                list_sequence_index.append(residue_seq_number)

    f.close()

    # obtain the N termini C termini Index
    # N termini => minimum seq index
    # C termini => maximum seq index
    min_seq_num = list_sequence_index[0]
    max_seq_num = list_sequence_index[0]
    for each_seq_num in list_sequence_index:
        if min_seq_num >= each_seq_num:
            min_seq_num = each_seq_num
        if max_seq_num <= each_seq_num:
            max_seq_num = each_seq_num

    ## Push the NTR CTR and corresponding acidic/basic atom to the list
    f = open(pdb_file_path, 'r')
    for line in f:
        if line.startswith("ATOM"):
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # atom x, y, z
            x = ""
            for i in range(30, 38, 1):
                x += line[i]
            x = x.strip()

            y = ""
            for i in range(38, 46, 1):
                y += line[i]
            y = y.strip()

            z = ""
            for i in range(46, 54, 1):
                z += line[i]
            z = z.strip()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            #### Fetch the interesting Polar Residue record
            temp_residue_entry = residue + "," + residue_seq_number + "," + chain

            #### Fetch the interesting Polar Atom record
            temp_heavy_atom_entry = residue + "," + residue_seq_number + \
                "," + chain + "," + atom + "," + x + "," + y + "," + z

            ## Push the CTR NTR
            if int(residue_seq_number) == min_seq_num:
                if atom == 'N':
                    list_nitrogen_basic.append(temp_heavy_atom_entry)
                    if temp_residue_entry not in list_charged_residue:
                        list_charged_residue.append(temp_residue_entry)

            if int(residue_seq_number) == max_seq_num:
                if atom in ['OT1', 'OT2']:
                    list_oxygen_acidic.append(temp_heavy_atom_entry)
                    if temp_residue_entry not in list_charged_residue:
                        list_charged_residue.append(temp_residue_entry)
    f.close()

    return list_charged_residue, list_nitrogen_basic, list_oxygen_acidic


#### Function for distance calculation
def CaluclateDistance(Xa, Ya, Za, Xb, Yb, Zb):

    square_distance = (Xa - Xb) * (Xa - Xb) +\
                      (Ya - Yb) * (Ya - Yb) +\
                      (Za - Zb) * (Za - Zb)
    distance = math.sqrt(square_distance)
    return distance


## Calculate salt bridge for each polar residue 
## Threshold as 3.2 A
def CalculateSaltBridge(nitrogen_list, oxygen_list):
    ## initalize the list for storing salt bridge residue pair
    list_saltbridge_residue_pair = []

    for i in range(0, len(nitrogen_list)):
        N_info = nitrogen_list[i]
        N_residue =  re.split(',', N_info)[0] + "," +\
                     re.split(',', N_info)[1]

        N_x = re.split(',', N_info)[-3]
        N_y = re.split(',', N_info)[-2]
        N_z = re.split(',', N_info)[-1]

        N_x = float(N_x)
        N_y = float(N_y)
        N_z = float(N_z)

        for j in range(0, len(oxygen_list)):
            O_info = oxygen_list[j]
            O_residue =  re.split(',', O_info)[0] + "," +\
                         re.split(',', O_info)[1]

            O_x = re.split(',', O_info)[-3]
            O_y = re.split(',', O_info)[-2]
            O_z = re.split(',', O_info)[-1]

            O_x = float(O_x)
            O_y = float(O_y)
            O_z = float(O_z)

            DIST = CaluclateDistance(Xa = N_x, Ya = N_y, Za = N_z, Xb = O_x, Yb = O_y, Zb = O_z)

            contact_pair = N_residue + ';' + O_residue
            if DIST <= 3.2:
                list_saltbridge_residue_pair.append(contact_pair)

    return list_saltbridge_residue_pair



## Merge these steps
def Merge(assigned_pdb_file_abs_path):
    list_results = []

    pdb_name = assigned_pdb_file_abs_path.split('/')[-1]
    pdb_name = pdb_name.split('.pdb')[0]

    list_charged_res, list_N_basic, list_O_acidic =\
         RetrieveXYZ(pdb_file_path= assigned_pdb_file_abs_path)
    list_saltbridge_pair =  CalculateSaltBridge(nitrogen_list = list_N_basic, oxygen_list = list_O_acidic)

    ## For log
    # for each_salt_pair in list_saltbridge_pair:
    #     log_entry = pdb_name + ',' + each_salt_pair
    #     print(log_entry)

    # Counting the salt bridge for each Polar residue
    list_unique_residue = []
    for each_pair in list_saltbridge_pair:
        residue1 = re.split(';', each_pair)[0]
        residue2 = re.split(';', each_pair)[1]
        if residue1 not in list_unique_residue:
            list_unique_residue.append(residue1)
        if residue2 not in list_unique_residue:
            list_unique_residue.append(residue2)
    ## Done

    #### Print OUT results

    for each_residue in list_unique_residue:
        count_saltbridge = 0
        for each_pair in list_saltbridge_pair:
            residue1 = re.split(';', each_pair)[0]
            residue2 = re.split(';', each_pair)[1]
            if residue1 == each_residue:
                count_saltbridge += 1
            if residue2 == each_residue:
                count_saltbridge += 1
        out_entry = pdb_name + ',' + each_residue + ',' + str(count_saltbridge)

        list_results.append(out_entry)

    return list_results


def main(assigned_pdb_folder):
    list_pdb_file = []

    os.chdir(assigned_pdb_folder)

    for root, dirs, files in os.walk(assigned_pdb_folder):
        for each_dir in dirs:
            each_dir_abs = os.path.join(root, each_dir)
            for root2, subdir_list2, file_list2 in os.walk(each_dir_abs):
                for each_file in file_list2:
                    if each_file.endswith(".pdb"):
                        if each_file.endswith('poc.pdb') == 0:
                            file_path = os.path.join(each_dir_abs, each_file)
                            list_results = Merge(file_path)
                            for each_entry in list_results:
                                out_line = each_entry
                                print(out_line)

                            list_pdb_file.append(file_path)


    with Pool(processes= 8 ) as pool:
        pool.map(Merge, list_pdb_file)
    return 0


if __name__ == '__main__':
    main(assigned_pdb_folder = '/home/bwang/data/cancer/calculate/structure/AF/part3/')
