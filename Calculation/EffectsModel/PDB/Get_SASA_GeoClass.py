#! /usr/bin/python3

import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--INPUT', type=str,
                    help = 'Input castp folder')
parser.add_argument('-o', '--OUTFILE', type=str,
                    help = 'Output SASA with GeoClass file')
args = parser.parse_args()

input_dir = args.INPUT
out_file = args.OUTFILE


# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}
###########################################
#### Reading Poc information ##############
###########################################

def read_castp_poc_output(castp_poc_file):
    # initalize list for residue in pocket
    list_poc_residue = []

    f = open(castp_poc_file, "r")
    for line in f:
        if line.startswith("ATOM"):
            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_ThreeAbbv = ""
            for i in range(17, 20, 1):
                residue_ThreeAbbv += line[i]
            residue_ThreeAbbv = residue_ThreeAbbv.upper().strip()

            # pocket id
            poc_id = ""
            for i in range(68, 72, 1):
                poc_id += line[i]
            poc_id = poc_id.strip()

            residue_id = dic_tri_single[residue_ThreeAbbv] + ',' +\
                residue_seq_number + ',' + chain
            if residue_id not in list_poc_residue:
                list_poc_residue.append(residue_id)
    f.close()

    return list_poc_residue


###########################################
#### Capture Buried Residues     ##########
###########################################

def get_buried_residues(castp_contrib_file):
    ## initalize the output list for all residues, buried residues
    ## dictionary[residues] = SASA
    list_buried_residue = []
    list_unique_residue = []
    dic_res_sasa = {}

    list_atom_entry = []

    f = open(castp_contrib_file, "r")
    for line in f:
        if line.startswith("ATOM"):
            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()
            residue = dic_tri_single[residue]

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            # atom's sasa
            atom_sasa = ""
            for i in range(39, 46, 1):
                if line[i] != '-':
                    atom_sasa += line[i]
            atom_sasa = atom_sasa.strip()

            residue_id = residue + "," + residue_seq_number + "," + chain

            atom_entry = residue_id + "," + atom + "," + atom_sasa

            if atom_entry not in list_atom_entry:
                list_atom_entry.append(atom_entry)

            if residue_id not in list_unique_residue:
                list_unique_residue.append(residue_id)
    f.close()

    for residue in list_unique_residue:
        residue_sasa = 0

        for atom_entry in list_atom_entry:
            atom_entry_id = atom_entry.split(",")[0] + "," +\
                atom_entry.split(",")[1] + "," +\
                atom_entry.split(",")[2]
            
            if residue == atom_entry_id:
                atom_sasa = atom_entry.split(",")[4]
                atom_sasa = float(atom_sasa)
                residue_sasa += atom_sasa

        # keep 4 decimal
        residue_sasa = "%.4f" % residue_sasa
        dic_res_sasa[residue]  = residue_sasa

        if float(residue_sasa) == 0.0:
            if residue not in list_buried_residue:
                list_buried_residue.append(residue)

    return list_buried_residue, list_unique_residue, dic_res_sasa


def main(Assigned_folder, Result_file_path):

    out_F = open(Result_file_path, 'w')

    list_pdbID = []

    for root, dirs, files in os.walk(Assigned_folder):
        for each_dir in dirs:
            list_pdbID.append(each_dir)


    for each_pdb in list_pdbID:
        poc_file_name = each_pdb + '.poc'
        working_dir = os.path.join(Assigned_folder, each_pdb)
        poc_file_path = os.path.join(working_dir, poc_file_name)

        pdb_wo_chain = each_pdb[0:4]
        try:
            sasa_file_name = pdb_wo_chain + '.4.contrib'
            sasa_file_path = os.path.join(working_dir, sasa_file_name)

            list_poc_res = read_castp_poc_output(castp_poc_file = poc_file_path)

            list_buried_res, list_all_res, dictionary_res_SASA =\
                get_buried_residues(castp_contrib_file = sasa_file_path)

            for each_res in list_all_res:
                if each_res in list_buried_res:
                    out_line = each_pdb + ',' + each_res + ',Buried,' + dictionary_res_SASA[each_res] + "\n"
                    # print(out_line)
                    out_F.write(out_line)
                elif each_res in list_poc_res:
                    out_line = each_pdb + ',' + each_res + ',Pocket,' + dictionary_res_SASA[each_res] + "\n"
                    # print(out_line)
                    out_F.write(out_line)
                else:
                    out_line = each_pdb + ',' + each_res + ',Surface,' + dictionary_res_SASA[each_res] + "\n"
                    # print(out_line)
                    out_F.write(out_line)
        except:
            print(each_pdb)
            pass

        # for e in list_poc_res:
        #     print(e, 'poc')
        # for e in list_buried_res:
        #     print(e, 'Buried')
        # for e in list_all_res:
        #     print(e, 'ALL')
        
        # for each_res in list_all_res:
        #     if float(dictionary_res_SASA[each_res]) > 300:
        #         print(each_pdb ,each_res, dictionary_res_SASA[each_res])
    out_F.close()
    return 0

if __name__ == "__main__":
    main(Assigned_folder = input_dir, Result_file_path = out_file)
