#! /usr/bin/python3

import re
import os
from multiprocessing import Process, Pool
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--INDIR', type=str,
                    help = 'Input Parent castp raw output folder')
parser.add_argument('-o', '--OUTDIR', type=str,
                    help = 'Output Value folder')
parser.add_argument('-t', '--THREADS', type=int,
                    help = 'Assigned Threads')
args = parser.parse_args()

input_dir = args.INDIR
out_dir = args.OUTDIR
count_threads = args.THREADS


# list for 20 amino acid
list_amino_acids = [
    "ALA",
    "ARG",
    "ASN",
    "ASP",
    "CYS",
    "GLN",
    "GLU",
    "GLY",
    "HIS",
    "ILE",
    "LEU",
    "LYS",
    "MET",
    "PHE",
    "PRO",
    "SER",
    "THR",
    "TRP",
    "TYR",
    "VAL"
]

# Dictionary detailed atom => Simplified Atom
dic_DetailedAtom_SimplifiedAtom = {
    "N":   "N",
    "CA":  "C",
    "C":   "C",
    "CB":  "C",
    "CG":  "C",
    "CD":  "C",
    "CD1": "C",
    "CZ":  "C",
    "CD2": "C",
    "CE1": "C",
    "CG1": "C",
    "CG2": "C",
    "CD1": "C",
    "CE":  "C",
    "CE2": "C",
    "CE3": "C",
    "CZ2": "C",
    "CZ3": "C",
    "CH2": "C",
    "NE":  "N",
    "NH1": "N",
    "ND2": "N",
    "NE2": "N",
    "ND1": "N",
    "NZ":  "N",
    "NE1": "N",
    "NH2": "N",
    "O":   "O",
    "OXT": "O",
    "OD1": "O",
    "OD2": "O",
    "OE1": "O",
    "OE2": "O",
    "OG":  "O",
    "OG1": "O",
    "OH":  "O",
    ## Add OT1 OT2 are old alternative O, OXT
    "OT1": "O",
    "OT2": "O",
    "SG":  "S",
    "SD":  "S"
}

# List for all simplified atom contact pairs
list_contact_atom_pair = [
    "CC",
    "NN",
    "OO",
    "SS",
    "CN",
    "CO",
    "CS",
    "NO",
    "NS",
    "OS",
    "NC",
    "OC",
    "SC",
    "ON",
    "SN",
    "SO"
]


def Get_Edge_Pair(Alpha_Edge_File):
    # Extract PDB
    PDB_ID = Alpha_Edge_File.split('/')[-1]
    PDB_ID = PDB_ID.split('.alpha')[0]

    # initialize edge pair for inter/intra residue
    list_interRes_edge_pair = []

    # initialize the unique atom/residue
    list_unique_atom = []
    list_unique_residue = []

    f = open(Alpha_Edge_File, 'r')
    for line in f:
        line = line.strip()
        if line != '':
            iterator = re.finditer(':', line)
            iterator = tuple(iterator)
            count = len(iterator)

            if count == 8:
                list_line_slices = re.split(' ', line)
                atom1 = list_line_slices[0]
                atom2 = list_line_slices[1]

                list_s_atom1 = re.split(':', atom1)
                res_1 = list_s_atom1[0] + ':' +\
                    list_s_atom1[1] + ':' +\
                    list_s_atom1[2]

                list_s_atom2 = re.split(':', atom2)
                res_2 = list_s_atom2[0] + ':' +\
                    list_s_atom2[1] + ':' +\
                    list_s_atom2[2]

                if atom1 not in list_unique_atom:
                    list_unique_atom.append(atom1)
                if atom2 not in list_unique_atom:
                    list_unique_atom.append(atom2)
                if res_1 not in list_unique_residue:
                    list_unique_residue.append(res_1)
                if res_2 not in list_unique_residue:
                    list_unique_residue.append(res_2)

                atom1_detailed = list_s_atom1[4]
                atom2_detailed = list_s_atom2[4]

                res1_contact = dic_DetailedAtom_SimplifiedAtom[atom1_detailed] +\
                    dic_DetailedAtom_SimplifiedAtom[atom2_detailed]

                res2_contact = dic_DetailedAtom_SimplifiedAtom[atom2_detailed] +\
                    dic_DetailedAtom_SimplifiedAtom[atom1_detailed]

                edge = atom1 + ',' + atom2 +\
                    ',' + res1_contact + ',' + res2_contact

                if res_1 != res_2:
                    list_interRes_edge_pair.append(edge)
    f.close()

    # Dictionary key(residue)   => value ( atom contact by this residue)
    dic_residue_contact = {}
    for each_residue in list_unique_residue:
        tmp_list = []

        # obtain the contact list
        for each_edge in list_interRes_edge_pair:

            atom1_edge_info = re.split(',', each_edge)[0]
            list_atom1_edge = re.split(':', atom1_edge_info)
            res1_in_edge = list_atom1_edge[0] + ':' +\
                list_atom1_edge[1] + ':' +\
                list_atom1_edge[2]
            atom1_contact = re.split(',', each_edge)[2]

            atom2_edge_info = re.split(',', each_edge)[1]
            list_atom2_edge = re.split(':', atom2_edge_info)
            res2_in_edge = list_atom2_edge[0] + ':' +\
                list_atom2_edge[1] + ':' +\
                list_atom2_edge[2]
            atom2_contact = re.split(',', each_edge)[3]

            if each_residue == res1_in_edge:
                tmp_list.append(atom1_contact)
                ## log info
                for i in range(0,len(list_contact_atom_pair)):
                    reference_atomContact = list_contact_atom_pair[i]
                    if atom1_contact == reference_atomContact:
                        log_info = PDB_ID + ',' + each_residue + ',' +\
                                   reference_atomContact + ',Has,' + each_edge
                        # print(log_info)
                ## log info complete
            elif each_residue == res2_in_edge:
                tmp_list.append(atom2_contact)
                ## log info
                for i in range(0,len(list_contact_atom_pair)):
                    reference_atomContact = list_contact_atom_pair[i]
                    if atom1_contact == reference_atomContact:
                        log_info = PDB_ID + ',' + each_residue + ',' +\
                                   reference_atomContact + ',Has,' + each_edge
                        # print(log_info)
                ## log complete
            else:
                continue
        # Done

        # counting different contact pairs
        list_count_contact = []
        for i in range(0, len(list_contact_atom_pair)):
            count = 0
            for every_contact in tmp_list:
                if every_contact == list_contact_atom_pair[i]:
                    count += 1
            list_count_contact.append(count)
        # Done
        dic_residue_contact[each_residue] = list_count_contact

    # print(dic_residue_contact)

    # For print out
    out_file_path = out_dir + \
        PDB_ID + '.count.AtomContact'
    out_f = open(out_file_path, 'w')

    ## setting the header line
    header_line = "PDB_chain,residue,"
    for i in range(0, len(list_contact_atom_pair)):
        header_line += list_contact_atom_pair[i] + ','
    # iteration complete
    header_line = header_line[0:-1] + "\n"
    out_f.write(header_line)

    ## insert feature vector to file handle
    for k, v in dic_residue_contact.items():
        out_line = ''
        out_line += PDB_ID + ',' + k + ','

        for i in range(0, len(v)):
            out_line +=  str(v[i]) + ','
        ## complete iteration

        out_line = out_line[0:-1] +  "\n"
        # print(out_line)
        out_f.write(out_line)

    out_f.close()

    return dic_residue_contact


def main(assigned_folder):
    # iterating the pdb folder
    list_pdb_file = []

    for root, dirs, files in os.walk(assigned_folder):
        for each_dir in dirs:
            each_dir_abs = os.path.join(root, each_dir)
            for root2, subdir_list2, file_list2 in os.walk(each_dir_abs):
                for each_file in file_list2:
                    if each_file.endswith(".alpha.edges"):
                        file_path = os.path.join(each_dir_abs, each_file)
                        list_pdb_file.append(file_path)

    return list_pdb_file

if __name__ == "__main__":
    list_alpha_edge = main(
        assigned_folder= input_dir)

    with Pool(processes = count_threads) as pool:
            pool.map(Get_Edge_Pair, list_alpha_edge)
