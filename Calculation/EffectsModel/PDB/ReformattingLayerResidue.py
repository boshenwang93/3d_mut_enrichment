

import os 
from multiprocessing import Process, Pool


list_unique_pdbwChain = [] 
f = open('/home/bwang/tmp/3Layer_ListFormat.important', 'r')
for line in f:
    line = line.strip()
    line_slices = line.split(',')

    pdb_w_chain = line_slices[0].strip()
    if pdb_w_chain not in list_unique_pdbwChain:
        list_unique_pdbwChain.append(pdb_w_chain)
f.close()

print('Unique PDB done!')

# out_folder = '/home/bwang/tmp/3layerTmp/'
# def GenerateRaw_3LayerInfo (pdbID):
#     out_file = out_folder + pdbID + '.raw3layer'
#     outF = open(out_file, 'w')

#     f = open('/home/bwang/tmp/3Layer_ListFormat.important', 'r')
#     for line in f:
#         line = line.strip()
#         line_slices = line.split(',')
#         pdb_w_chain = line_slices[0].strip()
#         if pdb_w_chain == pdbID:
#             new_line = line + "\n"
#             outF.write(new_line)
#     f.close()
#     outF.close()
#     return 0

# with Pool(processes= 15 ) as pool:
#         pool.map(GenerateRaw_3LayerInfo, list_unique_pdbwChain)

################### *****************************



def ReformattingLayer (pdbID):

    Raw_Layer_Info_File = '/home/bwang/tmp/3layerTmp/' + pdbID + '.raw3layer'
    out_cleanLayer_file = '/home/bwang/tmp/3layerClean/' + pdbID + '.1layer'
    outF = open(out_cleanLayer_file, 'w')

    list_host_residue = []
    dic_residue_firstLayerComp = {}
    dic_residue_secondLayerComp = {}
    dic_residue_thirdLayerComp = {}

    f = open(Raw_Layer_Info_File, 'r')
    for line in f:
        line = line.strip()
        line_slices = line.split(',')

        try:
            pdb_w_chain = line_slices[0].strip()
            nested_residue_interest = line_slices[1].strip()
            host_residue_index = nested_residue_interest.split(':')[1].strip()
            host_residue_aa =  nested_residue_interest.split(':')[2].strip()
            host_residue_info = pdb_w_chain + '_' +\
                                host_residue_index + '_' + host_residue_aa 

            if host_residue_info not in list_host_residue:
                list_host_residue.append(host_residue_info)

            layer_info = line_slices[2].strip()


            list_surround_composition = []
            for i in range(3, len(line_slices)):
                residue_index = line_slices[i].split(':')[1].strip()
                residue_aa = line_slices[i].split(':')[2].strip()
                residue_aa = residue_aa[0:3]
                residue_sum= residue_index + '_' + residue_aa
                list_surround_composition.append(residue_sum)


            if layer_info == 'firstlayer':
                dic_residue_firstLayerComp [host_residue_info] = list_surround_composition
            elif layer_info == 'secondlayer':
                dic_residue_secondLayerComp [host_residue_info] = list_surround_composition
            elif layer_info == 'thirdlayer':
                dic_residue_thirdLayerComp [host_residue_info] = list_surround_composition

        except:
            pass
    f.close()

    for each_residue in list_host_residue:

        ##
        new_line = each_residue + ','
        try:
            for e in dic_residue_firstLayerComp [each_residue]:
                new_line += e + ','
            # for e in dic_residue_secondLayerComp [each_residue]:
            #     new_line += e + ','
            # for e in dic_residue_thirdLayerComp [each_residue]:
            #     new_line += e + ','
            # print(new_line)

            new_line += "\n"
            outF.write(new_line)
        except:
            # print(each_residue)
            pass

    outF.close()
    return 0


with Pool(processes= 15 ) as pool:
        pool.map(ReformattingLayer, list_unique_pdbwChain)
