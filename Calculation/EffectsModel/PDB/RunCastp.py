#! /usr/bin/python3
 
import os
from multiprocessing import Process, Pool

def RunCastp (castp_file_abs_path):

    entering_dir = os.path.dirname(castp_file_abs_path)
    os.chdir(entering_dir)
    # print(entering_dir)
    
    castp_file = ''
    for root, subdirs, files in os.walk(entering_dir):
        for each_file in files:
            if each_file.endswith('.pdb'):
                castp_file += each_file

    ## for pocket contribution 
    poc_cmd = "newcast -fp . " + castp_file
    ## output file focus on "PDB.poc"

    ## for SASA calculation 
    SASA_cmd = "newcast -fq4 . " + castp_file
    ## output file focus on "PDB.4.contrib"

    ## for Delone Triangulation Edge Calculation
    del_cmd = "newcast -fd . " + castp_file

    ## for surface contact calculation
    surface_cmd = "newcast -ft . " + castp_file

    ## for alpha edges calculation
    alpha_edge_cmd = "newcast -fa . " + castp_file
    ## output file focus on 'XXXX.alpha.edges'

    # os.system(surface_cmd)

    os.system(poc_cmd)
    os.system(SASA_cmd)
    # os.system(del_cmd)
    os.system(alpha_edge_cmd)

    return 0 

def main(assigned_Parent_folder):
    # iterating the pdb folder
    list_pdb_file = []

    for root, dirs, files in os.walk(assigned_Parent_folder):
        for each_dir in dirs:
            sub_folder = os.path.join(root, each_dir)
            # print(sub_folder)
            for root2, subdir_list2, file_list2 in os.walk(sub_folder):
                for each_file in file_list2:
                    print(each_file)
                    if each_file.endswith("pdb"):
                        file_abs_path = os.path.join(sub_folder, each_file)
                        list_pdb_file.append(file_abs_path)
    # print(list_pdb_file)
    return list_pdb_file


if __name__ == "__main__":
    list_pdb = main(assigned_Parent_folder =\
           '/home/bwang/data/cancer/calculation/structure/experimental/refined/')
    
    with Pool(processes= 18 ) as pool:
        pool.map(RunCastp, list_pdb)