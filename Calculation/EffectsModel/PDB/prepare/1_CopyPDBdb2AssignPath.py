
import os

list_unique_pdb = []
list_unique_pdbWchain = []
list_pdb = []
tmpF = open('/home/bwang/data/cancer/mapPDB/pdbstructure/Results/fully_covered_unique_PDB.csv')
for line in tmpF:
    line = line.strip()
    pdb_w_index = line.split(',')[1]

    if pdb_w_index not in list_unique_pdbWchain:
        list_unique_pdbWchain.append(pdb_w_index)
    pdb_four = pdb_w_index[1:5]
    list_pdb.append(pdb_four)
    if pdb_four not in list_unique_pdb:
        list_unique_pdb.append(pdb_four)
tmpF.close()

tmpF = open('/home/bwang/data/cancer/mapPDB/pdbstructure/Results/partial_covered_nonoverlapping_PDB.csv')
for line in tmpF:
    line = line.strip()
    pdb_w_index = line.split(',')[1]

    if pdb_w_index not in list_unique_pdbWchain:
        list_unique_pdbWchain.append(pdb_w_index)
    pdb_four = pdb_w_index[1:5]
    list_pdb.append(pdb_four)
    if pdb_four not in list_unique_pdb:
        list_unique_pdb.append(pdb_four)
tmpF.close()




print(len(list_pdb), len(list_unique_pdb), len(list_unique_pdbWchain))



# def DownloadPdb(pdb, download_dir):
#     rcsb_link = "https://files.rcsb.org/download/" + pdb + ".pdb"
#     command_text = "wget " + rcsb_link + " -P " + download_dir 
#     os.system(command_text)

#     return 0


# def main():

#     for e in list_unique_pdb:
#         DownloadPdb(pdb = e,
#                     download_dir =\
#                          '/home/bwang/data/cancer/calculation/structure/experimental/RawPDB/')
#     return 0

# if __name__ == '__main__':
#     main()







# for each_pdb in list_unique_pdb:
#     original_path = '/mnt/data/database/structure/UnzipPDB/' +\
#                     each_pdb[1:3].lower() + '/' + 'pdb' + each_pdb.lower() + '*' 
#     dest_path = '/home/bwang/data/cancer/calculation/structure/experimental/RawPDB/'

#     copy_cmd = 'cp' + "\t" + original_path + "\t" + dest_path
#     os.system(copy_cmd)