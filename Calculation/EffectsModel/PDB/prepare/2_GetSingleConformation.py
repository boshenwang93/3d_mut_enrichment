#! /usr/bin/python3
import re
import os

# check the pdb which contains multiple conformations

def check_pdb(pdb_file_path, new_folder):

    pdb = re.split(".pdb", pdb_file_path)[0]
    pdb = re.split("/", pdb)[-1].strip()

    label_multiconform = 0
    f = open(pdb_file_path, 'r')
    for line in f:
        line_record_identify = ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "MODEL":
            label_multiconform += 1
    f.close()

    if label_multiconform == 0:
        command_line = 'cp ' + pdb_file_path + "\t" + new_folder
        os.system(command_line)
    else:
        new_file_name = pdb + '.pdb'
        single_conformation_file_path = os.path.join(new_folder, new_file_name)
        print(pdb_file_path, 'MultiConformation')

        out = open(single_conformation_file_path, 'w')

        f = open(pdb_file_path, 'r')
        count = 0
        for line in f:
            line_record_identify = ""
            # column 1-6 is the identfication for the line record
            for i in range(0, 6, 1):
                line_record_identify += line[i]
            line_record_identify = line_record_identify.strip()

            if line_record_identify == "MODEL":
                count += 1

            if count < 2:
                out.write(line)
        f.close()
        out.close()
    return 0


def main(Assigned_folder, SingleConformation_folder):
    list_pdbID = []

    for root, dirs, files in os.walk(Assigned_folder):
        for file in files:
            if file.endswith('pdb'):
                file_path = root + file
                check_pdb(pdb_file_path = file_path,
                         new_folder = SingleConformation_folder)
    return 0


if __name__ == "__main__":
    main(Assigned_folder = '/home/bwang/data/cancer/calculation/structure/experimental/RawPDB/' ,
         SingleConformation_folder = '/home/bwang/data/cancer/calculation/structure/experimental/SingleConformation/')