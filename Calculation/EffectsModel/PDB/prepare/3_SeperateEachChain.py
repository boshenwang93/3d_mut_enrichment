#! /usr/bin/python3

import re 
import os 

## Read Raw pdb, Generate Single Chain PDB
def SeparateChain(pdb_file, out_dir):

    pdb_id = ''
    if pdb_file.endswith('pdb'):
        if re.search('/', pdb_file):
            pdb_w_f = re.split('/', pdb_file)[-1]
            pdb_id += re.split('.pdb', pdb_w_f)[0]
        else:
            pdb_id += re.split('.pdb', pdb_w_f)[0]
    elif pdb_file.endswith('ent'):
        if re.search('/', pdb_file):
            pdb_w_f = re.split('/', pdb_file)[-1]
            pdb_id += re.split('.ent', pdb_w_f)[0]
        else:
            pdb_id += re.split('.ent', pdb_w_f)[0]

    pdb_id = pdb_id[-4:].strip().upper()

    print(pdb_id)

    ## get all chains
    list_NR_chain = []
    f = open(pdb_file, 'r')
    for line in f:
        line_record_identify =  ""
        # column 1-6 is the identfication for the line record
        for i in range(0, 6, 1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # chain identifier
            chain = line[21]
            chain = chain.upper()
            if chain not in list_NR_chain:
                list_NR_chain.append(chain)
    f.close()

    ## generate pdb files by chain

    for each_chain in list_NR_chain:
        new_pdb_file_name = pdb_id + '_' + each_chain + '.pdb'
        new_pdb_path = out_dir + new_pdb_file_name

        out = open(new_pdb_path, 'w')

        f = open(pdb_file, 'r')
        for line in f:
            line_record_identify = ""
            # column 1-6 is the identfication for the line record
            for i in range(0, 6, 1):
                line_record_identify += line[i]
                line_record_identify = line_record_identify.strip()

            if line_record_identify == "ATOM":
                # chain identifier
                chain = line[21]
                chain = chain.upper()

                if chain == each_chain:
                    out.write(line)
        f.close()

        out.close()
    return 0


def main(in_dir, out_dir):

    ## Set the raw PDB folder
    for root, dirs, files in os.walk(in_dir):
        for single_file in files:
            file_abs_path = root + single_file
            SeparateChain(pdb_file = file_abs_path ,
                  out_dir = out_dir)

    return 0

if __name__ == '__main__':
    main(in_dir = '/home/bwang/data/cancer/calculation/structure/experimental/SingleConformation/',
         out_dir = '/home/bwang/data/cancer/calculation/structure/experimental/SingleChain/')