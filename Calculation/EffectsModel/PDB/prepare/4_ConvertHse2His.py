

import os 

def ConvertHse2His(origin_pdb_path, dest_pdb_path):

    out_F = open(dest_pdb_path, 'w')

    in_F = open(origin_pdb_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in in_F:
        line = line.strip()

        if line.startswith("ATOM"):
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            if residue == "HSE":
                new_line = line[0:17] + 'HIS' + line[20:] + "\n"
                out_F.write(new_line)
                print(line)
            else:
                new_line = line + "\n"
                out_F.write(new_line)

    in_F.close()

    out_F.close()

    return 0

for root, subdir_list, file_list in os.walk('/home/bwang/data/cancer/calculation/structure/experimental/SingleChain/'):
    for each_file in file_list:
        each_file_path = root + each_file
        print(each_file)
        new_PDB_path = os.path.join('/home/bwang/data/cancer/calculation/structure/experimental/unifyHIS', each_file)

        ConvertHse2His(origin_pdb_path= each_file_path,
                       dest_pdb_path = new_PDB_path)