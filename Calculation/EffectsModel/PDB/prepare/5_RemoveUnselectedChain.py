#! /usr/bin/python3

import os

###########################################################
#### After single conform single chain Selection ##########
#### Remove the chain which is not selected ###############
###########################################################

list_unique_pdb = []
list_unique_pdbWchain = []
list_pdb = []
tmpF = open('/home/bwang/data/cancer/mapPDB/pdbstructure/Results/fully_covered_unique_PDB.csv')
for line in tmpF:
    line = line.strip()
    pdb_w_index = line.split(',')[1]

    pdb_w_chain = pdb_w_index[1:7]
    if pdb_w_chain not in list_unique_pdbWchain:
        list_unique_pdbWchain.append(pdb_w_chain)
    pdb_four = pdb_w_index[1:5]
    list_pdb.append(pdb_four)
    if pdb_four not in list_unique_pdb:
        list_unique_pdb.append(pdb_four)
tmpF.close()

tmpF = open('/home/bwang/data/cancer/mapPDB/pdbstructure/Results/partial_covered_nonoverlapping_PDB.csv')
for line in tmpF:
    line = line.strip()
    pdb_w_index = line.split(',')[1]

    pdb_w_chain = pdb_w_index[1:7]
    if pdb_w_chain not in list_unique_pdbWchain:
        list_unique_pdbWchain.append(pdb_w_chain)
    pdb_four = pdb_w_index[1:5]
    list_pdb.append(pdb_four)
    if pdb_four not in list_unique_pdb:
        list_unique_pdb.append(pdb_four)
tmpF.close()


# print(list_unique_pdbWchain)


for root, subdirs, files in os.walk('/home/bwang/data/cancer/calculation/structure/experimental/unifyHIS/'):
    for each_file in files:
        each_pdb_w_chain = each_file[0:6]
        file_abs_path = os.path.join(root, each_file)
        if each_pdb_w_chain not in list_unique_pdbWchain:
            remove_cmd = 'rm ' + file_abs_path
            os.system(remove_cmd)


# list_contained = []
# for root, subdirs, files in os.walk('/mnt/data/project/cancer/calculation/Structure/SingleConformation_SingleChain/'):
#     for each_file in files:
#         each_pdb_w_chain = each_file[0:6]
#         list_contained.append(each_pdb_w_chain)


# for each_pdb in list_selected_chain:
#     if each_pdb not in list_contained:
#         print(each_pdb)
