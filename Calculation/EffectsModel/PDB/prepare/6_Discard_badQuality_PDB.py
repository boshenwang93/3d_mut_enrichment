#! /usr/bin/python3

import re
import os

###########################################
#### Check Alias for Residue/Atom #########
#### Check Missing Heavy Atoms ############
#### Written by Boshen Wang ###############
###########################################

###########################################
#### Structure of the Script ##############

# the standard residue
list_standard_residue = [
    'ALA',
    'ARG',
    'ASN',
    'ASP',
    'CYS',
    'GLN',
    'GLU',
    'GLY',
    'HSE', 'HIS',
    'ILE',
    'LEU',
    'LYS',
    'MET',
    'PHE',
    'PRO',
    'SER',
    'THR',
    'TRP',
    'TYR',
    'VAL',
    ## In rare case, SEC serenocysteine
    'SEC' 
]


# the standard dictionary residue => heavy atom including terminal OXT
dic_standard_residue_heavyatom = {
    "ALA": ("N", "CA", "C", "O", "OT1", "CB", "OT2", "OXT"),
    "ARG": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD", "NE", "CZ", "NH1", "NH2", "OT2", "OXT"),
    "ASN": ("N", "CA", "C", "O", "OT1", "CB", "CG", "OD1", "ND2", "OT2", "OXT"),
    "ASP": ("N", "CA", "C", "O", "OT1", "CB", "CG", "OD1", "OD2", "OT2", "OXT"),
    "CYS": ("N", "CA", "C", "O", "OT1", "CB", "SG", "OT2", "OXT"),
    "GLN": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD", "OE1", "NE2", "OT2", "OXT"),
    "GLU": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD", "OE1", "OE2", "OT2", "OXT"),
    "GLY": ("N", "CA", "C", "O", "OT1", "OT2", "OXT"),
    ## HSE HIS are the same 
    "HIS": ("N", "CA", "C", "O", "OT1", "CB", "CG", "ND1", "CD2", "CE1", "NE2", "OT2", "OXT"),   
    ## From ILE error info, CD1 is more popular among PDB
    "ILE": ("N", "CA", "C", "O", "OT1", "CB", "CG1", "CG2", "CD1", "OT2", "OXT"),
    "LEU": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD1", "CD2", "OT2", "OXT"),
    "LYS": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD", "CE", "NZ", "OT2", "OXT"),
    "MET": ("N", "CA", "C", "O", "OT1", "CB", "CG", "SD", "CE", "OT2", "OXT"),
    "PHE": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD1", "CD2", "CE1", 'CE2', "CZ", "OT2", "OXT"),
    "PRO": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD", "OT2", "OXT"),
    "SER": ("N", "CA", "C", "O", "OT1", "CB", "OG", "OT2", "OXT"),
    "THR": ("N", "CA", "C", "O", "OT1", "CB", "OG1", "CG2", "OT2", "OXT"),
    "TRP": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD1", "CD2", "NE1", "CE2", "CE3", "CZ2", "CZ3", "CH2", "OT2", "OXT"),
    "TYR": ("N", "CA", "C", "O", "OT1", "CB", "CG", "CD1", "CD2", "CE1", "CE2", "CZ", "OH", "OT2", "OXT"),
    "VAL": ("N", "CA", "C", "O", "OT1", "CB", "CG1", "CG2", "OT2", "OXT"),
    ## In rare case, SEC will appear
    'SEC': ("N", "CA", "C", "O", "OT1", "CB", "SE", "OT2", "OXT"),
}


# INDEX list_heavy_atom
# [0] -- residue name
# [1] residue sequence number
# [2] chain ID
# [3] atom name
# [4][5][6] x y z

# INDEX list_residue
# [0] => residue tri letter
# [1] => sequence number
# [2] => chain ID


def RetrieveHeavyAtomInPDBfile(pdb_file_path):

    # list for unique residue
    list_residue = []

    # list for heavy atom entry
    list_heavy_atom_entry = []

    f = open(pdb_file_path, 'r')
    #######################################################################################
    ######## http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html####
    ######## pdb file format explanation ##################################################

    for line in f:

        if line.startswith("ATOM"):
            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip().upper()

            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip().upper()

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # element symbol
            element = line[76:78].strip().upper()

            # the residue info
            residue_info = residue + "," + residue_seq_number + "," + chain
            if residue_info not in list_residue:
                list_residue.append(residue_info)

            # the atom info
            temp_heavy_atom_entry = residue_info + "," + atom
            if atom.startswith('H') == 0:
                list_heavy_atom_entry.append(temp_heavy_atom_entry)
            else:
                pass

    f.close()

    # dictionary residue => list of heavy atoms
    dic_PDBfile_residue_atom = {}

    for each_residue in list_residue:
        tmp_list_heavy_atoms = []
        for each_heavy_atom in list_heavy_atom_entry:
            if each_heavy_atom.startswith(each_residue):
                atom_type = re.split(',', each_heavy_atom)[3]
                tmp_list_heavy_atoms.append(atom_type)
        dic_PDBfile_residue_atom [each_residue] = tmp_list_heavy_atoms


    return list_residue, dic_PDBfile_residue_atom


# Check the residue symbol alias
def CheckResidueAlias(residue_list):

    list_residue_alias = []

    for each_residue in residue_list:
        residue_tri_letter = re.split(',', each_residue)[0]
        if residue_tri_letter not in list_standard_residue:
            out_entry = each_residue + "\t" + 'Alias Residue Symbol'
            list_residue_alias.append(out_entry)

    return list_residue_alias


# check the atom alias
def CheckAtomAlias(dic_residue_atom_InPdbFile):

    list_atom_alias = []

    for k, v in dic_residue_atom_InPdbFile.items():
        residue_tri_letter = re.split(',', k)[0]

        for each_atom in v:
            if each_atom != 'OXT':
                if residue_tri_letter != 'UNK':
                    if each_atom not in dic_standard_residue_heavyatom[residue_tri_letter]:
                        out_entry = k + ',' + each_atom + "\t" + 'Alias Atom type'
                        list_atom_alias.append(out_entry)

    return list_atom_alias

# Check the missing Heavy atoms


def CheckMissingAtoms(dic_residue_atom_InPdbFile):
    list_res_wMissing = []
    list_missing_atoms = []

    for k, v in dic_residue_atom_InPdbFile.items():
        residue_tri_letter = re.split(',', k)[0]

        if residue_tri_letter != 'UNK':
            list_corresponding_standard_atoms = dic_standard_residue_heavyatom[residue_tri_letter]

            for each_atom in list_corresponding_standard_atoms:
                if each_atom not in ['OT1', 'OT2', 'OXT']:
                    if each_atom not in v:
                        if k not in list_res_wMissing:
                            list_res_wMissing.append(k)

                        out_entry = k + "," + 'Has Missing Atom Type' + "," + each_atom
                        list_missing_atoms.append(out_entry)

    return list_res_wMissing, list_missing_atoms


def main(pdb_folder):

    list_residue_wMissing = []

    for root, subdirs, files in os.walk(pdb_folder):
        os.chdir(root)

        for each_file in files:
            if each_file.endswith('pdb'):

                try:
                    pdb_id = each_file[0:6]

                    list_residue_CurrentPdbFile, dic_residue_atom_CurrentPdbFile = \
                        RetrieveHeavyAtomInPDBfile(pdb_file_path=each_file)

                    list_residue_alias  = CheckResidueAlias(residue_list = list_residue_CurrentPdbFile)
                    list_atom_alias = CheckAtomAlias(dic_residue_atom_InPdbFile = dic_residue_atom_CurrentPdbFile)
                    list_res_miss, list_missing_atoms = CheckMissingAtoms(dic_residue_atom_InPdbFile = dic_residue_atom_CurrentPdbFile)

                    count_chain_length = len(list_residue_CurrentPdbFile)
                    count_missing_residue = len(list_res_miss)
                    count_missing_atom = len(list_missing_atoms)

                    sum_info = pdb_id + ',' + str(count_chain_length) + ',' +\
                            str(count_missing_residue) + ',' + str(count_missing_atom) 
                    # print(sum_info)

                    proportion_missingAtom_ByRes = count_missing_residue / count_chain_length
                    if proportion_missingAtom_ByRes >= 0.1:
                        print(sum_info)
                        file_abs_path = os.path.join(root, each_file)
                        remove_cmd = 'rm ' + file_abs_path
                        os.system(remove_cmd)

                    # try:
                    #     for each_res in list_res_miss:
                    #         out_entry = pdb_id + ',' + each_res
                    #         if out_entry not in list_residue_wMissing:
                    #             list_residue_wMissing.append(out_entry)
                    #             print(out_entry)

                    #     for e in list_missing_atoms:
                    #         out_line = pdb_id + ',' + e
                    #         # print(out_line)
                    # except:
                    #     error_info = pdb_id + "has ERROR"
                    #     print(error_info)
                except:
                    # error_info = pdb_id + ' has unrecognized residue or atom'
                    # print(error_info)
                    pass

    return 0


if __name__ == "__main__":
    main(pdb_folder = '/home/bwang/data/cancer/calculation/structure/experimental/unifyHIS/')
