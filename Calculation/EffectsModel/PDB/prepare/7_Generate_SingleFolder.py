
import os 

assigned_foler = '/home/bwang/data/cancer/calculation/structure/experimental/unifyHIS/'
new_parent_folder = '/home/bwang/data/cancer/calculation/structure/experimental/refined/'

for root, subdirlist, file_list in os.walk(assigned_foler):
    for each_file in file_list:
        if each_file.endswith('pdb'):
            pdb_w_chainID = each_file.split('.pdb')[0]
            file_abs_path = os.path.join(root, each_file)

            dest_new_folder = os.path.join(new_parent_folder, pdb_w_chainID)
            mkdir_cmd = 'mkdir ' + dest_new_folder
            os.system(mkdir_cmd)

            copy_cmd = 'cp ' + file_abs_path + "\t" + dest_new_folder
            os.system(copy_cmd)
