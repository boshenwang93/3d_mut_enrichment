




#### Hash table for Entropy 
dic_seqIndex_entropy = {}
f = open('MSA_entropy.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    seqID = slices[0]
    seqIndex = slices[1]

    entropy = slices[2]

    seq_info = seqID + '_' + seqIndex
    dic_seqIndex_entropy [seq_info] = entropy
f.close()

# print(dic_seqIndex_entropy)

#### Hash table for All a.a. Probability SITE-SPECIFIC
dic_residue_prob = {}
f = open('MSA_prob.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    seqID = slices[0]
    wild_type = slices[2]
    seqIndex = slices[1]

    prob = slices[3]

    seq_info = seqID + '_' + wild_type + '_' + seqIndex
    dic_residue_prob [seq_info] = prob
f.close()

# print(dic_residue_prob)

print('sequence info DONE')

# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}

#### Hash table for atom contact
dic_pdbIndex_atomContact = {}
f = open('atomcontact.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    try:
        pdb_w_chain = slices[0]
        index_nested = slices[1]
        index = index_nested.split(':')[1].strip()
        pdbIndex = pdb_w_chain + '_' + index 

        atom_info = ''
        for i in range(2,len(slices)):
            atom_info += slices[i] + ','

        dic_pdbIndex_atomContact [pdbIndex] = atom_info
    except:
        pass
f.close()

print("Atom Connect done")

#### Hash table for residue layer
dic_pdbIndex_3layer = {}
f = open('longrangecontact.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    try:
        pdb_w_chain = slices[0]
        index_nested = slices[1]
        index = index_nested.split(':')[1].strip()
        pdbIndex = pdb_w_chain + '_' + index 

        layer_info = ''
        for i in range(2,len(slices)):
            layer_info += slices[i] + ','

        dic_pdbIndex_3layer [pdbIndex] = layer_info
    except:
        pass
f.close()

print('residue long range contact DONE')


### Hash table for Salt bridge
dic_pdbIndex_saltbridge = {}
f = open('saltbridge.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    pdb_w_chain = slices[0]
    index = slices[2]
    pdbIndex = pdb_w_chain + '_' + index 

    salt = slices[-1]
    dic_pdbIndex_saltbridge [pdbIndex] = salt
f.close()

# print(dic_pdbIndex_saltbridge)

#### Hash table for SASA, location
dic_pdbIndex_SASA = {}
dic_pdbIndex_location = {} 
f = open('sasa_geo.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    pdbChain = slices[0]
    aaIndex = slices[2]
    pdbIndex = pdbChain + "_" + aaIndex 

    location = slices[-2]
    sasa = slices[-1]

    dic_pdbIndex_location [pdbIndex] = location 
    dic_pdbIndex_SASA [pdbIndex] = sasa
f.close()

print("Other Structure DOne")



dic_mut_charge = {}
f = open('Charge.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    wildAA = slices[0]
    mutAA = slices[1]
    mut_pattern = wildAA + mutAA 

    charge = slices[2]
    dic_mut_charge [mut_pattern] = charge
f.close()


dic_mut_B62 = {}
f = open('B62score.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    wildAA = slices[0]
    mutAA = slices[1]
    mut_pattern = wildAA + mutAA 

    blosum = slices[2]
    dic_mut_B62 [mut_pattern] = blosum
f.close()


dic_mut_atomcharge = {}
f = open('AtomChange.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    wildAA = slices[0]
    mutAA = slices[1]

    mut_pattern = wildAA + mutAA 

    delte_atom = ''
    for i in range(2, len(slices)):
        stringValue = str(slices[i])
        delte_atom += stringValue + ','

    dic_mut_atomcharge [mut_pattern] = delte_atom
f.close()

print("change Done")


#### Original Mutation File
f = open('mapped_allMut_wPancount.csv', 'r')
for line in f:
    line = line.strip().upper()
    slices = line.split(',')

    protID = slices[1].strip()

    wild_type = slices[3].strip()
    mut_type = slices[4].strip()
    seq_index = slices[2].strip()


    pdb_seqindex_identifier = slices[0]

    identifier_entropy = protID + '_' + seq_index
    identifier_wildAA = protID + '_' + wild_type + '_' + seq_index
    identifier_mutAA = protID + '_' + mut_type + '_' + seq_index

    identifier_change = wild_type + mut_type

    # print(identifier_entropy)

    try:
        saltbridgeCount = 0
        try:
            saltbridgeCount +=int(dic_pdbIndex_saltbridge [pdb_seqindex_identifier])
        except:
            pass
        saltbridgeCount = str(saltbridgeCount)

        new_line = line + ',' +\
                dic_seqIndex_entropy [identifier_entropy] + ',' +\
                dic_residue_prob [identifier_wildAA] + ',' +\
                dic_residue_prob[identifier_mutAA] + ',' +\
                dic_mut_B62 [identifier_change] + ',' +\
                dic_mut_charge [identifier_change] + ',' +\
                dic_mut_atomcharge [identifier_change] +\
                saltbridgeCount + ',' +\
                dic_pdbIndex_SASA [pdb_seqindex_identifier] + ',' +\
                dic_pdbIndex_location [pdb_seqindex_identifier] + ',' +\
                dic_pdbIndex_atomContact [pdb_seqindex_identifier] +\
                dic_pdbIndex_3layer [pdb_seqindex_identifier]
        new_line = new_line.upper()
        print(new_line)
    except:
        pass

f.close()

