

# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}


dic_pdbIndex_uniprotIndex = {}
F_residueMapping = open('/data/database/Cancer/COSMIC/fullcov/residue.mapping', 'r')
for line in F_residueMapping:
    line = line.strip()
    slices = line.split(',')
    uniprotID = slices[0].strip()
    uniprot_index = slices[2].strip()

    residue_AA =  slices[1].strip()
    pdbID =  slices[3].strip()
    pdb_index =  slices[4].strip()

    pdb_w_index = pdbID + '_' + residue_AA + '_' + pdb_index
    uniprot_w_index = uniprotID + '_' + uniprot_index

    dic_pdbIndex_uniprotIndex [pdb_w_index] = uniprot_w_index
F_residueMapping.close()


dic_uniprotIndex_entropy = {}
F_entropy = open('/data/database/Cancer/COSMIC/fullcov/entropy.csv', 'r')
for line in F_entropy:
    line = line.strip()
    slices = line.split(',')
    uniprotID = slices[0].strip()
    uniprot_index = slices[1].strip()
    entropyValue = slices[2].strip()
    entropyValue = float(entropyValue)
    
    uniprot_w_index = uniprotID + '_' + uniprot_index
    dic_uniprotIndex_entropy [uniprot_w_index] = entropyValue
F_entropy.close()


dic_uniprotIndex_freqCOSMIC = {}
F_COSMIC = open('/data/database/Cancer/COSMIC/fullcov/ResidueMutFrequency.csv', 'r')
for line in F_COSMIC:
    line = line.strip()
    slices = line.split(',')
    uniprotID = slices[0].strip()
    uniprot_index = slices[1].strip()
    uniprot_index = uniprot_index[1:]
    mut_frequency = slices[2].strip()

    mut_frequency = int(mut_frequency)
    
    uniprot_w_index = uniprotID + '_' + uniprot_index
    dic_uniprotIndex_freqCOSMIC [uniprot_w_index] = mut_frequency
F_COSMIC.close()

# print(dic_pdbIndex_uniprotIndex)

## The structural Cluster 
f_resComp = open('/data/database/Cancer/COSMIC/fullcov/upThirdLayer.residue', 'r')
for line in f_resComp:
    line = line.strip()
    slices = line.split(',')

    sum_entropy = 0
    sum_frequency = 0 

    count_res = len(slices) -1
     
    host_residue = slices[0]
    pdb_id = host_residue.split('_')[0]
    pdb_chain = host_residue.split('_')[1]
    pdb_index = host_residue.split('_')[2]
    tri_aa = host_residue.split('_')[3]
    single_AA = dic_tri_single [tri_aa]

    pdb_w_chain = pdb_id + '_' + pdb_chain
    
    host_pdb_identifier = pdb_w_chain + '_' + single_AA + '_' + pdb_index
    try:
        host_uniprot_identifier = dic_pdbIndex_uniprotIndex [host_pdb_identifier]
        sum_entropy += dic_uniprotIndex_entropy [host_uniprot_identifier]
        sum_frequency += dic_uniprotIndex_freqCOSMIC [host_uniprot_identifier]
    except:
        pass
    
    # print(count_res)
    for i in range(1,count_res):
        guest_nested = slices[i]
        guest_tri_aa = guest_nested.split('_')[-1]
        guest_AA = dic_tri_single [guest_tri_aa]
        guest_index = guest_nested.split('_')[0]

        guest_pdb_identifier = pdb_w_chain + '_' + guest_AA + '_' + guest_index
        
        try:
            guest_uniprot_identifier = dic_pdbIndex_uniprotIndex [guest_pdb_identifier]
            sum_entropy += dic_uniprotIndex_entropy [host_uniprot_identifier]
            sum_frequency += dic_uniprotIndex_freqCOSMIC [host_uniprot_identifier]
        except:
            pass
    
    avg_entropy = sum_entropy / count_res 
    avg_frequency = sum_frequency / count_res

    out_line = host_pdb_identifier + ',' + host_uniprot_identifier + ',' +\
               str(avg_entropy) + ',' + str(sum_frequency) + ',' + str(avg_frequency)
    print(out_line)


f_resComp.close()