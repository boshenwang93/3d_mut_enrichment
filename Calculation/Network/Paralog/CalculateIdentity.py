#! /usr/bin/python3

import os 
import argparse


#### Setting the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input Parent dir that contains aligned fasta by each Uniprot ')
parser.add_argument('-o', '--outIden', type=str,
                    help = 'output Identity file in CSV format')
args = parser.parse_args()

input_aligned_fasta_ParentDIR = args.indir 
out_csv_path = args.outIden

###########################################################
#### Calculate two-way identity  ##########################
###########################################################

def ReadIdentity(alignedFasta_file):
    uniprotID1 =''
    uniprotID2 = ''

    with open(alignedFasta_file) as f:
        all_context = f.read()
    f.close()

    list_slices = all_context.split('>')

    seq1_w_header = '>' + list_slices[1]
    seq2_w_header = '>' + list_slices[2]

    list_lines_seq1 = seq1_w_header.split("\n")
    list_lines_seq2 = seq2_w_header.split("\n")
    
    seq1_context = ''
    seq2_context = ''

    for eachline in list_lines_seq1:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq1_context += eachline
        else:
            uniprotID1 += eachline
                
    for eachline in list_lines_seq2:
        if eachline.startswith('>') == 0 :
            if eachline != '':
                seq2_context += eachline
        else:
            uniprotID2 += eachline

    ## check length equal 
    m = len(seq1_context)
    n = len(seq2_context)

    ## count identity
    gaps_seq1 = 0 
    gaps_seq2 = 0 
    identity_chars = 0 
    for i in range(0, m ,1):
        if seq1_context[i] == seq2_context[i]:
            identity_chars += 1
        if seq1_context[i] == '-':
            gaps_seq1 += 1
        if seq2_context[i] == '-':
            gaps_seq2 += 1
    ## without gap, the length of sequence 
    seq1_wo_gap = m - gaps_seq1
    seq2_wo_gap = n - gaps_seq2
    ## calculate the identity of sequence
    seq1_identity = identity_chars / seq1_wo_gap
    seq2_identity = identity_chars / seq2_wo_gap
    
    
    seq_identity_info = uniprotID1 + ',' +\
                        uniprotID2 + ',' +\
                        str(seq1_identity) + ',' +\
                        str(seq2_identity) + "\n"
    
    return seq_identity_info


def main():

    out_F = open(out_csv_path, 'a')
    
    for root, subdir_list, file_list in os.walk(input_aligned_fasta_ParentDIR):
        for each_subdir in subdir_list:
            each_subdir_abs = root + each_subdir + '/'
            for root2, subdir_list2, file_list2 in os.walk(each_subdir_abs):
                for each_file in file_list2:
                    if each_file.endswith('.fasta'):
                        each_file_abs = each_subdir_abs + each_file
                        iden_info = ReadIdentity(alignedFasta_file = each_file_abs)
                        out_F.write(iden_info)
    out_F.close()
    
    return 0 


if __name__ == '__main__':
    main()