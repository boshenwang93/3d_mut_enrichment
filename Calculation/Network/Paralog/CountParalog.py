import re 

input_identity_CSV = '/mnt/data/project/cancer/paralog/Iden50.csv'

list_unique_uniprot = [] 

f_in = open(input_identity_CSV, 'r') 
for line in f_in:
    line = line.strip()
    uniprot_Human = re.split(',', line)[0]
    uniprot_Human = re.split('>', uniprot_Human)[1]

    if uniprot_Human not in list_unique_uniprot:
        list_unique_uniprot.append(uniprot_Human)
f_in.close()

for each_uniprot in list_unique_uniprot:
    count = 0

    f_in = open(input_identity_CSV, 'r') 
    for line in f_in:
        line = line.strip()
        uniprot_Human = re.split(',', line)[0]
        uniprot_Human = re.split('>', uniprot_Human)[1]

        uniprot_2nd = re.split(',', line)[1]
        uniprot_2nd = re.split('>', uniprot_2nd)[1]

        if uniprot_Human == each_uniprot:
            if uniprot_Human != uniprot_2nd:
                count += 1
    f_in.close()

    out_line = each_uniprot + ',' + str(count)
    print(out_line)
