
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter


###########################################################
#### Check the file size and find Outlier #################
#### Plot the histogram ###################################
###########################################################

dic_gene_uniprot = {} 
f_tmp = open('uniprot2HGNC.tab', 'r')
for line in f_tmp:
    line = line.strip()
    uniprot = line.split("\t")[0].strip()
    geneID = line.split("\t")[1].strip()
    dic_gene_uniprot [geneID] = uniprot
f_tmp.close()


list_cancer_gene = [] 
f = open('cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    cancer_gene = line.split(',')[0].strip()
    list_cancer_gene.append(cancer_gene)
f.close()

# print(list_cancer_gene)

list_cancer_uniprot = []
for each_cancer_gene in list_cancer_gene:
    try:
        paired_uniprot = dic_gene_uniprot [each_cancer_gene]
        list_cancer_uniprot.append(paired_uniprot)
    except:
        pass 
# print(list_cancer_uniprot, len(list_cancer_uniprot))

list_size = [] 
list_cancer_size = [] 
dic_uniprot_size = {} 


f_tmp = open('/mnt/data/project/cancer/paralog/paralogCount_iden30.csv', 'r')
for line in f_tmp:
    line = line.strip()
    uniprot = line.split(',')[0].strip()
    paralogs = line.split(',')[1].strip()
    paralogs = int(paralogs)

    dic_uniprot_size [uniprot] = paralogs 

    if uniprot in list_cancer_uniprot:
        list_cancer_size.append(paralogs)
    else:
        list_size.append(paralogs)

    # print(uniprot)
f_tmp.close()

for e in list_cancer_size:
    print(e)

# print(list_size)
# for i in range(-10, 0):
#     size_value = list_size[i]
#     print(dic_size_file[size_value])

# num_bins = 10
# n, bins, patches = plt.hist(list_size, num_bins, facecolor='blue', alpha=0.5)
# plt.show()

# num_bins = 10
# n, bins, patches = plt.hist(list_cancer_size, num_bins, facecolor='blue', alpha=0.5)
# plt.show()

plt.boxplot(list_size)
plt.show()