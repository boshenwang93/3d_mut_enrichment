import re 
import os 
import argparse
from multiprocessing import Process, Pool
from collections import defaultdict 

###########################################################
#### A Quick filtering based on identity ##################
###########################################################
#### Setting the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--incsv', type=str,
                    help = 'input Identity CSV File')
args = parser.parse_args()

input_identity_CSV = args.incsv 


list_ensembl_wPDB = [] 

f_in = open(input_identity_CSV, 'r') 
for line in f_in:
    line = line.strip()
    uniprot_Human = re.split(',', line)[0]
    uniprot_Human = re.split('>', uniprot_Human)[1]

    uniprot_nonHum = re.split(',', line)[1]
    uniprot_nonHum = re.split('>', uniprot_nonHum)[1]

    iden_seq1 = re.split(',', line)[2].strip()
    iden_seq1 = float(iden_seq1)
    iden_seq2 = re.split(',', line)[3].strip()
    iden_seq2 = float(iden_seq2)

    if iden_seq1 >= 0.5:
        if iden_seq2 > 0.5:
            print(line)
            if uniprot_Human not in list_ensembl_wPDB:
                list_ensembl_wPDB.append(uniprot_Human)
            # print(line)
f_in.close()

for each_ensembl in list_ensembl_wPDB:
    origin_path = '/mnt/data/project/cancer/searchPDB/fasta/' + each_ensembl + '.fasta'
    dest_path = '/mnt/data/project/cancer/MSA/fasta/' 

    # cp_cmd = 'cp ' + origin_path + "\t" + dest_path 
    # os.system(cp_cmd)