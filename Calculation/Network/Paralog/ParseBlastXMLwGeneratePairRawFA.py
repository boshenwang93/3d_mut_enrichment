#! /usr/bin/python3

import xml.etree.ElementTree as ET
import os
import argparse
from multiprocessing import Process, Pool


#### Setting the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input dir containing blast XML')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output PARENT dir contain Raw pairwise fasta')
parser.add_argument('-q', '--querydir', type=str,
                    help = 'dir contain Query sequence fasta')
parser.add_argument('-d', '--dbdir', type=str,
                    help = 'dir contain database sequence fasta')
parser.add_argument('-l', '--log', type=str,
                    help = 'log file path')
parser.add_argument('-t', '--thread', type=int,
                    help = 'assign how many threads')
args = parser.parse_args()

input_xml_dir = args.indir 
output_pairwise_fasta_dir = args.outdir 
query_fasta_dir = args.querydir
database_fasta_dir = args.dbdir
log_file_path = args.log
count_threads = args.thread


###########################################################
#### Parse BLAST XML Out &&&& #############################
#### Generate Raw Paired Fasta File #######################
#### Modified  by B.W. July 2020 ##########################
###########################################################

#### Given the fasta path Return the whole sequence content
def RetrieveSeqContent(fasta_file_path):
    seq_content = ''
    with open(fasta_file_path, 'r') as f:
        seq_content += f.read()
    f.close()
    return seq_content


#### Parse the BLAST XML output
#### Return the hash table {ReferenceSeqHeader => list Candidate header}
def ObtainCandidates(BLAST_XML_Output_file):

    # initialize the query/reference sequence ID & list for hit/candidate IDs
    # Hash table (refID => List Candidates)
    reference_seq_header = ''
    list_candidates_header = []
    dic_RefSeqHeader_listCandidatesHeader = {}

    # read the XML file into a ET parser
    tree = ET.parse(BLAST_XML_Output_file)
    root = tree.getroot()

    # find hits
    for first_layer in root:
        if 'BlastOutput_query-def' == first_layer.tag:
            query_info = first_layer.text
            reference_seq_header += query_info.upper().strip()

        # find the iterations segments
        elif 'BlastOutput_iterations' == first_layer.tag:
            for second_layer in first_layer:
                if 'Iteration' == second_layer.tag:
                    for third_layer in second_layer:
                        if 'Iteration_hits' == third_layer.tag:
                            for fourth_layer in third_layer:
                                if 'Hit' == fourth_layer.tag:
                                    candidate_header = ''

                                    for fifth_layer in fourth_layer:
                                        if 'Hit_def' == fifth_layer.tag:
                                            candidate_header += fifth_layer.text
                                    list_candidates_header.append(
                                        candidate_header)

    # Push the result to the hash table
    dic_RefSeqHeader_listCandidatesHeader[reference_seq_header] =\
        list_candidates_header

    return dic_RefSeqHeader_listCandidatesHeader


def GenerateRawPairedFasta(dictionary_ref_Lhits,
                           ref_seq_folder,
                           candidate_seq_folder,
                           output_dir):
    # Invoke the hash table
    for key, value in dictionary_ref_Lhits.items():
        ref_seq_header = key
        ## retreive the uniprot ID from the header UNIPROT RULE
        ## In trimmed uniprot ID 
        ref_seq_ID = ref_seq_header.upper().strip()

        ref_seq_PATH = ref_seq_folder + ref_seq_ID + '.fasta'
        ref_seq_content = RetrieveSeqContent(fasta_file_path=ref_seq_PATH)
        
        # print(ref_seq_ID, ref_seq_content)

        for each_candidate_header in value:
            ########!!!!!!!!@@@@@@@@@@@@@@@@

            ## Condition 1, For Uniprot Full Header 
            each_candidate_ID = each_candidate_header.split("|")[1].upper().strip()

            ## Condition 2, For Trimmed Uniprot header ONLY uniprot ID
            # each_candidate_ID = each_candidate_header.strip()
            
            ## Condition 3, For PDB full name Entry 
            # each_candidate_ID = each_candidate_header.strip()
            ## Complete

            ## retrieve the candidate sequence
            candidate_seq_PATH = candidate_seq_folder + \
                str(each_candidate_ID) + '.fasta'
            candidate_seq_content = RetrieveSeqContent(fasta_file_path = candidate_seq_PATH)
            
            # print(each_candidate_ID, candidate_seq_content)

            # Combine two sequence content to a new fasta file
            raw_pairwise_fasta_PATH = output_dir + ref_seq_ID + '_' + each_candidate_ID + '.fasta'
            out = open(raw_pairwise_fasta_PATH, 'w')
            new_content = ref_seq_content + "\n" + candidate_seq_content
            out.write(new_content)
            out.close()
    return 0






def CombineForSingleXML(BLAST_XML_abs_path):

    log_F = open(log_file_path, 'w')
    
    file_name = BLAST_XML_abs_path.split('/')[-1]
    seqID = file_name[0:-4].strip()
    out_son_dir = output_pairwise_fasta_dir + seqID + '/'
    cmd_mkdir = 'mkdir ' + out_son_dir
    os.system(cmd_mkdir)
                
    try:
        dic_ref_listHits = ObtainCandidates(
                            BLAST_XML_Output_file=BLAST_XML_abs_path)
        GenerateRawPairedFasta(dictionary_ref_Lhits=dic_ref_listHits,
                                ref_seq_folder=query_fasta_dir,
                                candidate_seq_folder=database_fasta_dir,
                                output_dir=out_son_dir)
    except:
        out_info = BLAST_XML_abs_path + "\n"
        log_F.write(out_info)
    log_F.close()

    return 0 




def main(BLAST_XML_dir):

    list_BlastXML_file = [] 
    # BLAST all fasta in folder
    for root, dirs, files in os.walk(BLAST_XML_dir):
        for single_file in files:
            file_asb_path = BLAST_XML_dir + single_file
            if single_file.endswith('.XML'):
                list_BlastXML_file.append(file_asb_path)
    
    with Pool(processes= count_threads ) as pool:
        pool.map(CombineForSingleXML, list_BlastXML_file)

    return 0

if __name__ == '__main__':
    main(BLAST_XML_dir= input_xml_dir)
