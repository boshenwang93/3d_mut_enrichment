#! /usr/bin/python3

import os
import argparse

# Make BLAST DATABASE CMD
# makeblastdb -in allPDB.fasta -dbtype prot -out pdb

# BLAST Search options
# E-value < 1E-6,  Max hits < 100, outformat XML
# blastp -db pdb -query XXXX.fasta -out XXXX.xml
# -num_threads 5 -evalue 1E-6 -max_hsps 100000 -matrix BLOSUM62 -outfmt 5

## Setting the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input folder contains query sequence in single fasta')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output folder contains BLAST XML results')
parser.add_argument('-d', '--dbname', type=str,
                    help = 'the BLAST database name for searching against')
parser.add_argument('-t', '--threads', type=str,
                    help = 'assign CPU threads for BLAST')
args = parser.parse_args()

input_dir = args.indir
output_dir = args.outdir
ref_blast_db = args.dbname
num_threads = args.threads 



def RunBLAST(query_fasta_path, output_XML_folder, BLAST_DB_name):
    # output xml file name
    fasta_name = query_fasta_path.split('/')[-1]
    uniprot_id = fasta_name.split('.fasta')[0]
    output_XML_path = output_XML_folder + uniprot_id + '.XML'

    # cmd
    blast_cmd = 'blastp -db ' + BLAST_DB_name +\
                ' -query ' + query_fasta_path +\
                ' -out ' + output_XML_path +\
                ' -num_threads' + "\t" + num_threads + "\t" +\
                '-evalue 1E-6 -max_hsps 100000 -matrix BLOSUM80 -outfmt 5'

    os.system(blast_cmd)

    return 0


def main(in_dir, out_dir, blast_database_name):
    # fasta path
    fasta_path = in_dir

    # output path
    output_path = out_dir

    # BLAST all fasta in folder
    for root, dirs, files in os.walk(fasta_path):
        for single_fasta_file in files:
            fasta_abs_path = root + single_fasta_file

            RunBLAST(query_fasta_path=fasta_abs_path,
                     output_XML_folder=output_path,
                     BLAST_DB_name=blast_database_name)
    return 0


if __name__ == '__main__':
    main(in_dir= input_dir,
         out_dir= output_dir,
         blast_database_name= ref_blast_db)
