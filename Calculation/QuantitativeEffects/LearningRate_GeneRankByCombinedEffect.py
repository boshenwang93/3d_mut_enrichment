
import matplotlib.pyplot as plt
import os

list_CGC_confirmed_cancer_genes = []
f = open('cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    if line.startswith('Gene') == 0:
        gene = line.split(',')[0]
        list_CGC_confirmed_cancer_genes.append(gene)
f.close()
# print(list_CGC_confirmed_cancer_genes)


dic_protID_gene = {}
for root, subdirs, files in os.walk('/home/bwang/data/cancer/mutation/pancancer/eachIsoform'):
    for each_file in files:
        ensembl_protID = each_file.split('.')[0]
        each_file_abs = os.path.join(root, each_file)
        i = 1
        f = open(each_file_abs, 'r')
        for line in f:
            line = line.strip()
            i += 1
            if i == 3:
                gene = line.split("\t")[0]
                try:
                    gene = gene.split('_')[0]
                    dic_protID_gene[ensembl_protID] = gene
                except:
                    dic_protID_gene[ensembl_protID] = gene
        f.close()

# print(dic_protID_gene)


#######################################################
##### Site only Evidence ##############################
#######################################################
total = 0
CGC = 0
nonCGC = 0
list_CGC_siteOnly = []
list_nonCGC_siteOnly = []
list_unique_gene = []
f = open('ranked_breast_siteonly.csv', 'r')
for line in f:
    line = line.strip()

    protID = line.split('_')[-3]
    try:
        gene_name = dic_protID_gene[protID]
        if gene_name not in list_unique_gene:
            total += 1
            list_unique_gene.append(gene_name)
            if nonCGC <= 200:
                if gene_name in list_CGC_confirmed_cancer_genes:
                    CGC += 1
                else:
                    nonCGC += 1
                # done
                list_CGC_siteOnly.append(CGC)
                list_nonCGC_siteOnly.append(nonCGC)
    except:
        pass
f.close()

# print(len(list_CGC), len(list_nonGCG) )


#######################################################
##### Site only + Co-clustering #######################
#######################################################
total = 0
CGC = 0
nonCGC = 0
list_CGC_coclustering = []
list_nonCGC_coclustering = []
list_unique_gene = []
f = open('rankded_breast_all.csv', 'r')
for line in f:
    line = line.strip()
    protID = line.split('_')[-3]
    try:
        gene_name = dic_protID_gene[protID]
        if gene_name not in list_unique_gene:
            total += 1
            list_unique_gene.append(gene_name)
            if nonCGC <= 200:
                if gene_name in list_CGC_confirmed_cancer_genes:
                    CGC += 1
                else:
                    nonCGC += 1
                # done
                list_CGC_coclustering.append(CGC)
                list_nonCGC_coclustering.append(nonCGC)
    except:
        pass
f.close()


plt.scatter(list_nonCGC_siteOnly, list_CGC_siteOnly, color='gray', label='Site Only')
plt.scatter(list_nonCGC_coclustering, list_CGC_coclustering, color='red', label='Site+Co-clustering')

plt.title('True Hit Rate of CGC Gene (breast)')
plt.ylabel('CGC gene')
plt.xlabel('Background (non-CGC) gene')
plt.legend()
plt.show()
