
import numpy as np
import os
from collections import defaultdict


def sum_substitutionIn_sameResidue(input_all_effects_file, output_sameSite_effect_file):

    dic_mutsiteResidue_effectperSubs = defaultdict(list)
    dic_mutsiteResidue_ClusterInfluence = {}

    f = open(input_all_effects_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        strcuture_index = slices[0]
        seq_mutPattern = slices[1]
        raw_prob = float(slices[2])
        accumuEffect = float(slices[3])
        rough_allEffect_cluster = float(slices[4])
        onlyDelEffect_cluster = float(slices[5])
        avg_rough_allEff_cluster = float(slices[6])
        avg_onlyDel_cluster = float(slices[7])

        ensemblID = seq_mutPattern.split('_')[0]
        seqIndex = seq_mutPattern.split('_')[1]
        wildaa = seq_mutPattern.split('_')[2]
        mutaa = seq_mutPattern.split('_')[3]

        mutation_site =  strcuture_index + '_' + ensemblID + '_' +\
                        seqIndex + '_' + wildaa

        cluster_influence = str(rough_allEffect_cluster) + ',' +\
                            str(onlyDelEffect_cluster) + ',' +\
                            str(avg_rough_allEff_cluster) + ',' +\
                            str(avg_onlyDel_cluster)

        dic_mutsiteResidue_ClusterInfluence [mutation_site] = cluster_influence

        # if raw_prob > 0.48:
        dic_mutsiteResidue_effectperSubs [mutation_site].append(accumuEffect)
    f.close()

    # print(dic_mutsiteResidue_effectperSubs)

    outF = open(output_sameSite_effect_file, 'w')
    for each_mutationSite, list_accumuEffectPersubstitution in dic_mutsiteResidue_effectperSubs.items():
        sum_effect_allSubstitutions = 0
        for each_substitution_effect in list_accumuEffectPersubstitution:
            sum_effect_allSubstitutions += each_substitution_effect
        ## done
        new_line = each_mutationSite + ',' +\
                   str(sum_effect_allSubstitutions) + ',' +\
                   dic_mutsiteResidue_ClusterInfluence[each_mutationSite] + "\n"
        outF.write(new_line)
    outF.close()

    return 0


for root, subdirs, files in os.walk('/home/bwang/data/cancer/integrate/effects_perTissue_2layerPOC'):
    for each_file in files:
        input_file = os.path.join(root, each_file)
        output_dir = '/home/bwang/data/cancer/integrate/site_effect_2layer'
        outfile = os.path.join(output_dir, each_file)
        sum_substitutionIn_sameResidue(input_file, outfile)