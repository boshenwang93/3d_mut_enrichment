from collections import defaultdict
from multiprocessing import Process, Pool
import os


output_dir = '/home/bwang/data/cancer/integrate/effects_perTissue_3layerPOC'



# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}



list_aminc_acid = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']


dic_seqMutPattern_rawProb = {}
dic_strcMutPattern_rawProb = {}
dic_seqIndex_strcIndex = {}
dic_strcIndex_seqIndex = {}
f_prob = open('cosmic_variant_wProb.csv', 'r')
for line in f_prob:
    line = line.strip()
    if line.startswith('struc') == 0 :
        slices = line.split(',')

        ensemblID = slices[1]
        seqIndex = slices[2]
        wildaa = slices[3]
        mutaa = slices[4]

        strcID_wIndex = slices[0]

        raw_del_prob = slices[-2]
        raw_del_prob = float(raw_del_prob)

        seqMutPattern = ensemblID + '_' + seqIndex + '_' + wildaa + '_' + mutaa
        strcMutPattern = strcID_wIndex + '_' + wildaa + '_' + mutaa

        dic_seqMutPattern_rawProb[seqMutPattern] = raw_del_prob
        dic_strcMutPattern_rawProb [strcMutPattern] = raw_del_prob

        seq_wIndex = ensemblID + '_' + seqIndex + '_' + wildaa
        strc_wIndex =  strcID_wIndex + '_' + wildaa
        dic_seqIndex_strcIndex [seq_wIndex] = strc_wIndex
        dic_strcIndex_seqIndex [strc_wIndex] = seq_wIndex
        # print(strc_wIndex)
f_prob.close()

# print(dic_seqMutPattern_rawProb)
# {'ENSP00000413001_807_E_D': 0.812}
# {'AF_Q96MI9_395_C_R': 0.29}
# {'ENSP00000413001_395': 'AF_Q96MI9_395'}





#### Hash table for Residue => Structural Cluster
dic_hostResidue_listClusterResidue = defaultdict(list)
f = open('3layer_combined_alphaPOC.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    host_residue = slices[0]

    if len(host_residue.split('_')) == 3:
        host_residue  = 'AF_' + host_residue
    # print(host_residue)

    for i in range(1, len(slices)-1):
        guest_residue = slices[i]
        dic_hostResidue_listClusterResidue [host_residue].append(guest_residue)
        # print(guest_residue)
f.close()


print('cluster loaded')



def calculate_alleffect(input_mutation_file):

    file_name = input_mutation_file.split('/')[-1]
    out_file_path = os.path.join(output_dir, file_name)
    outF = open(out_file_path, 'w')

    ## load the has table for recurrence
    dic_seqMutPattern_recurrence = {}
    f = open(input_mutation_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        ensemblID = slices[0]
        seqIndex = slices[1]
        wildaa = slices[2]
        mutaa = slices[3]

        recurrence = slices[4].strip()
        recurrence = int(recurrence)

        seq_MutPattern = ensemblID + '_' + seqIndex + '_' + wildaa + '_' + mutaa
        dic_seqMutPattern_recurrence [seq_MutPattern] = recurrence
    f.close()



    f = open(input_mutation_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        ensemblID = slices[0]
        seqIndex = slices[1]
        wildaa = slices[2]
        mutaa = slices[3]

        recurrence = slices[4].strip()
        recurrence = int(recurrence)

        seq_MutPattern = ensemblID + '_' + seqIndex + '_' + wildaa + '_' + mutaa
        seqIDindex = ensemblID + '_' + seqIndex + '_' + wildaa
        try:
            ## obtain its deleterious probability
            deleterious_rawProb = dic_seqMutPattern_rawProb [seq_MutPattern]
            ## obtain its accumulative probability
            accumulative_deleterious_effect = recurrence * deleterious_rawProb

            cluster_all_accumuEffect = 0
            cluster_onlyDel_accmuEffect = 0

            ## map to structure ID and index
            strcIDindex = dic_seqIndex_strcIndex [seqIDindex]

            strc_ID = ''
            tmp_slices = strcIDindex.split('_')
            for i in range(0, len(tmp_slices)-2):
                strc_ID += tmp_slices[i] + '_'

            # print(seqIDindex)
            # print(strcIDindex)
            # print(strc_ID)

            ## obtain its structural cluster
            list_residues_strcCluster = dic_hostResidue_listClusterResidue [strcIDindex]
            for each_residue in list_residues_strcCluster:
                ##iterate all mutation pattern
                for each_aa in list_aminc_acid:
                    current_neighbor_strMutPattern = strc_ID + each_residue + '_' + each_aa

                    try:
                        current_neighbor_RawDelProb = dic_strcMutPattern_rawProb [current_neighbor_strMutPattern]
                        # print(current_neighbor_strMutPattern, current_neighbor_RawDelProb)

                        ## convert to ensembl to obtain recurrence
                        guest_strc_wIndex = strc_ID + each_residue
                        guest_seqwindex = dic_strcIndex_seqIndex [guest_strc_wIndex]
                        guest_seqMutPattern = guest_seqwindex + '_' + each_aa
                        guest_recurrence = dic_seqMutPattern_recurrence [guest_seqMutPattern]

                        cluster_all_accumuEffect += guest_recurrence * current_neighbor_RawDelProb
                        if current_neighbor_RawDelProb > 0.48:
                            cluster_onlyDel_accmuEffect  += guest_recurrence * current_neighbor_RawDelProb
                    except:
                        current_neighbor_RawDelProb = 0
            ## Search the structural cluster is DONE
            avg_all_cluster_impact = cluster_all_accumuEffect / len(list_residues_strcCluster)
            avg_onlyDel_cluster_impact = cluster_onlyDel_accmuEffect / len(list_residues_strcCluster)

            new_line = strcIDindex + ',' + seq_MutPattern + ',' + str(deleterious_rawProb) + ',' +\
                    str(accumulative_deleterious_effect) + ',' +\
                    str(cluster_all_accumuEffect) + ',' +\
                    str(cluster_onlyDel_accmuEffect) + ',' +\
                    str(avg_all_cluster_impact) + ',' +\
                    str(avg_onlyDel_cluster_impact) + "\n"
            outF.write(new_line)

        except:
            ## if no prediction, omit
            pass
    f.close()

    return 0

list_input_mut_file = []
for root, subdirs, files in os.walk('/home/bwang/data/cancer/integrate/count_perTissue'):
    for each_mut_file in files:
        if each_mut_file.endswith('csv'):
            each_file_abs = os.path.join(root, each_mut_file)
            list_input_mut_file.append(each_file_abs)

with Pool(processes= 8 ) as pool:
    pool.map(calculate_alleffect, list_input_mut_file)
