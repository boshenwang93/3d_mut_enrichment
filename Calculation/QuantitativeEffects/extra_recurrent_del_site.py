import os
import numpy as np

############################################################
#### Loading the CGC gene list #############################
#### Loading the ensembl ID => gene symbol #################
############################################################

list_CGC_confirmed_cancer_genes = []
f = open('cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    if line.startswith('Gene') == 0:
        gene = line.split(',')[0]
        list_CGC_confirmed_cancer_genes.append(gene)
f.close()
# print(list_CGC_confirmed_cancer_genes)



dic_protID_gene = {}
for root, subdirs, files in os.walk('/home/bwang/data/cancer/mutation/pancancer/eachIsoform'):
    for each_file in files:
        ensembl_protID = each_file.split('.')[0]
        each_file_abs = os.path.join(root, each_file)
        i = 1
        f = open(each_file_abs, 'r')
        for line in f:
            line = line.strip()
            i += 1
            if i == 3:
                gene = line.split("\t")[0]
                try:
                    gene = gene.split('_')[0]
                    dic_protID_gene [ensembl_protID] = gene
                except:
                    dic_protID_gene [ensembl_protID] = gene
        f.close()
##### LOADING DONE



def pickHighRank_site(input_effectsSite_file, percentile_assigned):

    list_accumuEffects = []
    f = open(input_effectsSite_file, 'r')
    for line in f:
        line = line.strip()

        str_seq_nested = line.split(',')[0]
        ensembl_protID = str_seq_nested.split('_')[-3]

        site_accumuEffect = line.split(',')[1]
        site_accumuEffect = float(site_accumuEffect)
        list_accumuEffects.append(site_accumuEffect)
    f.close()

    threshold_accumu = np.percentile(list_accumuEffects, (100-100*percentile_assigned))

    list_driver_gene = []
    list_overlapCGC_gene = []
    f = open(input_effectsSite_file, 'r')
    for line in f:
        line = line.strip()

        str_seq_nested = line.split(',')[0]
        ensembl_protID = str_seq_nested.split('_')[-3]

        site_accumuEffect = line.split(',')[1]
        site_accumuEffect = float(site_accumuEffect)
        if site_accumuEffect >= threshold_accumu:
            try:
                gene_symbol = dic_protID_gene [ensembl_protID]
                if gene_symbol not in list_driver_gene:
                    list_driver_gene.append(gene_symbol)
                if gene_symbol not in list_overlapCGC_gene:
                    if gene_symbol in list_CGC_confirmed_cancer_genes:
                        list_overlapCGC_gene.append(gene_symbol)
                out_line = gene_symbol + ',' + line
                # print(out_line)
            except:
                pass
        else:
            # print(line)
            pass
    f.close()

    count_gene = len(list_driver_gene)
    count_CGC = len(list_overlapCGC_gene)
    TPR = count_CGC / count_gene
    out_line = input_effectsSite_file + ',' + str(TPR) + ',' +\
               str(percentile_assigned) + ',' + str(count_gene) + ',' +\
               str(count_CGC)
    print(out_line)

    return 0


for root, subdirs, files in os.walk('/home/bwang/data/cancer/integrate/site_all_pancancer'):
    for each_file in files:
        each_file_abs = os.path.join(root, each_file)
        for i in range(1,11):
            pickHighRank_site(each_file_abs, 0.00006*i)
            print('#################')

