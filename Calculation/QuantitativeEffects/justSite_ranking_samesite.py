
import numpy as np
import os
from collections import defaultdict


list_CGC_confirmed_cancer_genes = []
f = open('cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    if line.startswith('Gene') == 0:
        gene = line.split(',')[0]
        list_CGC_confirmed_cancer_genes.append(gene)
f.close()


def select_driver_mutations(input_all_effects_file):
    ## initialize
    list_accumuEffect_itself = []

    ## we think the avg_onlyDel_inCluster is more effective
    list_mixed_effect = []

    ## the HASH TABLE mix_effect => list of mutations
    dic_mixEffect_listMutations = defaultdict(list)

    list_ensembl_woGene = []
    f = open(input_all_effects_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')
        seq_mutPattern = slices[0]
        ensemblID =seq_mutPattern.split('_')[0]

        # raw_prob = float(slices[1])
        accumuEffect = float(slices[1])
        rough_allEffect_cluster = float(slices[2])
        onlyDelEffect_cluster = float(slices[3])
        avg_rough_allEff_cluster = float(slices[4])
        avg_onlyDel_cluster = float(slices[5])

        mixEffect = accumuEffect * 1

        dic_mixEffect_listMutations [mixEffect].append(seq_mutPattern)

        list_mixed_effect.append(mixEffect)

        try:
            gene = dic_protID_gene [ensemblID]
        except:
            if ensemblID not in list_ensembl_woGene:
                list_ensembl_woGene.append(ensemblID)
    f.close()
    # print(list_ensembl_woGene)

    list_mixed_effect = np.unique(list_mixed_effect)
    list_sorted_mixEffect = np.sort(list_mixed_effect)[::-1]
    # print(list_sorted_mixEffect)


    for i in range(0, len(list_sorted_mixEffect)):
        tmp_list_mutationpatterns = dic_mixEffect_listMutations [list_sorted_mixEffect[i]]
        for e in tmp_list_mutationpatterns:
            new_line = e + ','  +\
                       str(list_sorted_mixEffect[i])
            print(new_line)
    return 0


select_driver_mutations(input_all_effects_file = '/home/bwang/project/3d_mut_enrichment/Calculation/QuantitativeEffects/site_del_3layer/pancancer_mut.csv')
