
import os

dic_protID_gene = {}
for root, subdirs, files in os.walk('/home/bwang/data/cancer/mutation/pancancer/eachIsoform'):
    for each_file in files:
        ensembl_protID = each_file.split('.')[0]
        each_file_abs = os.path.join(root, each_file)
        i = 1
        f = open(each_file_abs, 'r')
        for line in f:
            line = line.strip()
            i += 1
            if i == 3:
                gene = line.split("\t")[0]
                try:
                    gene = gene.split('_')[0]
                    dic_protID_gene [ensembl_protID] = gene
                except:
                    dic_protID_gene [ensembl_protID] = gene
        f.close()



list_unique_gene = []
f = open('ranked_prostate.csv', 'r')
for line in f:
    line = line.strip()

    protID = line.split('_')[-3]
    try:
        gene_name = dic_protID_gene[protID]
        if gene_name not in list_unique_gene:
            list_unique_gene.append(gene_name)
            print(gene_name)
    except:
        pass
f.close()