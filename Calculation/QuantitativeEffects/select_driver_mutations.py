
import os
import numpy as np

list_CGC_confirmed_cancer_genes = []
f = open('cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    if line.startswith('Gene') == 0:
        gene = line.split(',')[0]
        list_CGC_confirmed_cancer_genes.append(gene)
f.close()
# print(list_CGC_confirmed_cancer_genes)


dic_protID_gene = {}
for root, subdirs, files in os.walk('/home/bwang/data/cancer/mutation/pancancer/eachIsoform'):
    for each_file in files:
        ensembl_protID = each_file.split('.')[0]
        each_file_abs = os.path.join(root, each_file)
        i = 1
        f = open(each_file_abs, 'r')
        for line in f:
            line = line.strip()
            i += 1
            if i == 3:
                gene = line.split("\t")[0]
                try:
                    gene = gene.split('_')[0]
                    dic_protID_gene [ensembl_protID] = gene
                except:
                    dic_protID_gene [ensembl_protID] = gene
        f.close()
## protID => gene DONE
# for k,v in dic_protID_gene.items():
#     print(k,v)



def select_driver_mutations(input_all_effects_file, percentile_accumu, percentile_cluster):

    list_accumuEffect_itself = []
    list_rough_All_cluster = []
    list_onlyDel_accumu_cluster = []
    list_avg_rough_cluster = []
    list_avg_onlyDel_cluster = []

    ## we think the avg_onlyDel_inCluster is more effective
    list_mixed_effect = []

    list_ensembl_woGene = []
    f = open(input_all_effects_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')
        seq_mutPattern = slices[0]
        ensemblID =seq_mutPattern.split('_')[0]

        raw_prob = float(slices[1])
        accumuEffect = float(slices[2])
        rough_allEffect_cluster = float(slices[3])
        onlyDelEffect_cluster = float(slices[4])
        avg_rough_allEff_cluster = float(slices[5])
        avg_onlyDel_cluster = float(slices[6])

        mixEffect = accumuEffect * avg_onlyDel_cluster

        # print(raw_prob, accumuEffect, rough_allEffect_cluster, onlyDelEffect_cluster, avg_rough_allEff_cluster, avg_onlyDel_cluster)

        list_accumuEffect_itself.append(accumuEffect)
        list_rough_All_cluster.append(rough_allEffect_cluster)
        list_onlyDel_accumu_cluster.append(onlyDelEffect_cluster)
        list_avg_rough_cluster.append(avg_rough_allEff_cluster)
        list_avg_onlyDel_cluster.append(avg_onlyDel_cluster)
        list_mixed_effect.append(mixEffect)

        try:
            gene = dic_protID_gene [ensemblID]
        except:
            if ensemblID not in list_ensembl_woGene:
                list_ensembl_woGene.append(ensemblID)
    f.close()
    # print(list_ensembl_woGene)


    mixeffect_threshold =  np.percentile(list_mixed_effect, (100-100*percentile_accumu) )

    accumu_threshold = np.percentile(list_accumuEffect_itself, (100-100*percentile_accumu) )
    cluster_threshold1 = np.percentile(list_rough_All_cluster, (100-100*percentile_cluster) )
    cluster_threshold2 = np.percentile(list_onlyDel_accumu_cluster, (100-100*percentile_cluster))
    cluster_threshold3 = np.percentile(list_avg_rough_cluster, (100-100*percentile_cluster))
    cluster_threshold4 = np.percentile(list_avg_onlyDel_cluster, (100-100*percentile_cluster))

    # print(accumu_threshold, cluster_threshold1)

    list_drivergene1 = []
    list_drivergene2 = []
    list_drivergene3 = []
    list_drivergene4 = []
    f = open(input_all_effects_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')
        seq_mutPattern = slices[0]

        ensemblID =seq_mutPattern.split('_')[0]

        try:
            gene = dic_protID_gene[ensemblID]

            raw_prob = float(slices[1])
            accumuEffect = float(slices[2])
            rough_allEffect_cluster = float(slices[3])
            onlyDelEffect_cluster = float(slices[4])
            avg_rough_allEff_cluster = float(slices[5])
            avg_onlyDel_cluster = float(slices[6])

            if raw_prob > 0.48:
                if accumuEffect > accumu_threshold:
                    if rough_allEffect_cluster > cluster_threshold1:
                        if gene not in list_drivergene1:
                            list_drivergene1.append(gene)
                    if onlyDelEffect_cluster > cluster_threshold2:
                        if gene not in list_drivergene2:
                            list_drivergene2.append(gene)
                    if avg_rough_allEff_cluster > cluster_threshold3:
                        if gene not in list_drivergene3:
                            list_drivergene3.append(gene)
                    if avg_onlyDel_cluster > cluster_threshold4:
                        if gene not in list_drivergene4:
                            list_drivergene4.append(gene)
        except:
            pass
    f.close()

    # examine the prediction results
    count_gene1 = len(list_drivergene1)
    known_cgc1 = 0
    for i in list_drivergene1:
        if i in list_CGC_confirmed_cancer_genes:
            known_cgc1 += 1
    true_known_rate1 = known_cgc1 / count_gene1


    count_gene2 = len(list_drivergene2)
    known_cgc2 = 0
    for i in list_drivergene2:
        if i in list_CGC_confirmed_cancer_genes:
            known_cgc2 += 1
    true_known_rate2 = known_cgc2 / count_gene2

    count_gene3 = len(list_drivergene3)
    known_cgc3 = 0
    for i in list_drivergene3:
        if i in list_CGC_confirmed_cancer_genes:
            known_cgc3 += 1
    true_known_rate3 = known_cgc3 / count_gene3

    count_gene4 = len(list_drivergene4)
    known_cgc4 = 0
    for i in list_drivergene4:
        if i in list_CGC_confirmed_cancer_genes:
            known_cgc4 += 1
    try:
        true_known_rate4 = known_cgc4 / count_gene4
    except:
        true_known_rate4 = 0

    # new_line = str(percentile_accumu) + ',' + str(percentile_cluster) + ',' +\
    #            str(count_gene1) + ',' + str(true_known_rate1) + ','+\
    #            str(count_gene2) + ',' + str(true_known_rate2) + ','+\
    #            str(count_gene3) + ',' + str(true_known_rate3) + ','+\
    #            str(count_gene4) + ',' + str(true_known_rate4)

    new_line =  str(count_gene4) + ',' + str(true_known_rate4)

    # list_predictedgene_mixeffect = []
    # f = open(input_all_effects_file, 'r')
    # for line in f:
    #     line = line.strip()
    #     slices = line.split(',')
    #     seq_mutPattern = slices[0]

    #     ensemblID =seq_mutPattern.split('_')[0]

    #     try:
    #         gene = dic_protID_gene[ensemblID]

    #         raw_prob = float(slices[1])
    #         accumuEffect = float(slices[2])
    #         avg_onlyDel_cluster = float(slices[6])

    #         mix_effect = accumu_threshold * avg_onlyDel_cluster

    #         if raw_prob > 0.48:
    #             if mix_effect > mixeffect_threshold:
    #                 if gene not in list_predictedgene_mixeffect:
    #                     list_predictedgene_mixeffect.append(gene)
    #     except:
    #         pass
    # f.close()

    # count_gene_mix = len(list_predictedgene_mixeffect)
    # known_cgc_mix = 0
    # for i in list_predictedgene_mixeffect:
    #     if i in list_CGC_confirmed_cancer_genes:
    #         known_cgc_mix += 1
    # true_known_rate_mix = known_cgc_mix / count_gene_mix
    # new_line = str(percentile_accumu) + ',' +\
    #             str(count_gene_mix) + ',' + str(true_known_rate_mix)


    return new_line


for root, subdirs, files in os.walk('/home/bwang/project/3d_mut_enrichment/Calculation/QuantitativeEffects/effects_perTissue/'):
    for each_file in files:
        each_file_abs = os.path.join(root, each_file)

        results = select_driver_mutations(input_all_effects_file= each_file_abs,
                                percentile_accumu=0.03,
                                percentile_cluster=0.02)

        out_entry = each_file + ',' + results
        print(out_entry)