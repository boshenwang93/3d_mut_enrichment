from collections import defaultdict


##### Select top 500 mutations 
input_ranked_tissue_file = 'rankded_breast_all.csv'
list_breast_top_mutations = []
list_breast_top_PDB = []
dic_pdb_alldrivermut_breast = defaultdict(list)
i = 0
f = open(input_ranked_tissue_file, 'r')
for line in f:
    line = line.strip()
    i += 1
    pdb_and_seq_index = line.split(',')[0]
    if i<= 500:
        list_breast_top_mutations.append(pdb_and_seq_index)
        pdb_w_chain = pdb_and_seq_index.split('_')[0] + '_' +\
                    pdb_and_seq_index.split('_')[1]
        if pdb_w_chain not in list_breast_top_PDB:
            list_breast_top_PDB.append(pdb_w_chain)
        dic_pdb_alldrivermut_breast [pdb_w_chain].append(pdb_and_seq_index)
f.close()



##### Select top 500 mutations 
input_ranked_tissue_file = 'rankded_lung_all.csv'
list_lung_top_mutations = []
list_lung_top_PDB = []
dic_pdb_alldrivermut_lung = defaultdict(list)
i = 0
f = open(input_ranked_tissue_file, 'r')
for line in f:
    line = line.strip()
    i += 1
    pdb_and_seq_index = line.split(',')[0]
    if i<= 500:
        list_lung_top_mutations.append(pdb_and_seq_index)
        pdb_w_chain = pdb_and_seq_index.split('_')[0] + '_' +\
                    pdb_and_seq_index.split('_')[1]
        if pdb_w_chain not in list_lung_top_PDB:
            list_lung_top_PDB.append(pdb_w_chain)
        dic_pdb_alldrivermut_lung [pdb_w_chain].append(pdb_and_seq_index)
f.close()



##### Select top 500 mutations 
input_ranked_tissue_file = 'rankded_liver_all.csv'
list_liver_top_mutations = []
list_liver_top_PDB = []
dic_pdb_alldrivermut_liver = defaultdict(list)
i = 0
f = open(input_ranked_tissue_file, 'r')
for line in f:
    line = line.strip()
    i += 1
    pdb_and_seq_index = line.split(',')[0]
    if i<= 500:
        list_liver_top_mutations.append(pdb_and_seq_index)
        pdb_w_chain = pdb_and_seq_index.split('_')[0] + '_' +\
                    pdb_and_seq_index.split('_')[1]
        if pdb_w_chain not in list_liver_top_PDB:
            list_liver_top_PDB.append(pdb_w_chain)
        dic_pdb_alldrivermut_liver [pdb_w_chain].append(pdb_and_seq_index)
f.close()




##### Select top 500 mutations 
input_ranked_tissue_file = 'rankded_stomach_all.csv'
list_stomach_top_mutations = []
list_stomach_top_PDB = []
dic_pdb_alldrivermut_stomach = defaultdict(list)
i = 0
f = open(input_ranked_tissue_file, 'r')
for line in f:
    line = line.strip()
    i += 1
    pdb_and_seq_index = line.split(',')[0]
    if i<= 500:
        list_stomach_top_mutations.append(pdb_and_seq_index)
        pdb_w_chain = pdb_and_seq_index.split('_')[0] + '_' +\
                    pdb_and_seq_index.split('_')[1]
        if pdb_w_chain not in list_stomach_top_PDB:
            list_stomach_top_PDB.append(pdb_w_chain)
        dic_pdb_alldrivermut_stomach [pdb_w_chain].append(pdb_and_seq_index)
f.close()




## randomly select two tissues

for each_pdb in list_breast_top_PDB:
    if each_pdb in list_stomach_top_PDB:
        list_drivermut_tissueA = dic_pdb_alldrivermut_breast[each_pdb]
        list_drivermut_tissueB = dic_pdb_alldrivermut_stomach[each_pdb]
        
        different_site_count = 0
        for each_drivermut in list_drivermut_tissueA:
            if each_drivermut not in list_drivermut_tissueB:
                different_site_count += 1
                print(each_pdb, each_drivermut, "not in tissue B", list_drivermut_tissueB)
        
        different_percent_A = different_site_count / len(list_drivermut_tissueA)
        different_percent_B = different_site_count / len(list_drivermut_tissueB)

        print(each_pdb, len(list_drivermut_tissueA), len(list_drivermut_tissueB),
               different_percent_A, different_percent_B)
        print('********************************')