dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}

# list_aa = []
# for k,v in dic_tri_single.items():
#     list_aa.append(v)
# print(list_aa)

f = open('all_structure_cluster.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    host_residue = slices[0]
    new_host = host_residue[0:-3] + dic_tri_single[host_residue[-3:]]
    new_line = new_host + ','

    for i in range(1, len(slices)-1):
        guest_residue = slices[i]
        guest_index = guest_residue.split('_')[0]
        guest_aa = guest_residue.split('_')[1]
        new_guest = guest_index + '_' + dic_tri_single [guest_aa]
        new_line += new_guest + ','
    print(new_line)
f.close()