
import numpy as np
import os
from collections import defaultdict
import numpy as np


list_CGC_confirmed_cancer_genes = []
f = open('cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    if line.startswith('Gene') == 0:
        gene = line.split(',')[0]
        list_CGC_confirmed_cancer_genes.append(gene)
f.close()
# print(list_CGC_confirmed_cancer_genes)


dic_protID_gene = {}
for root, subdirs, files in os.walk('/home/bwang/data/cancer/mutation/pancancer/eachIsoform'):
    for each_file in files:
        ensembl_protID = each_file.split('.')[0]
        each_file_abs = os.path.join(root, each_file)
        i = 1
        f = open(each_file_abs, 'r')
        for line in f:
            line = line.strip()
            i += 1
            if i == 3:
                gene = line.split("\t")[0]
                try:
                    gene = gene.split('_')[0]
                    dic_protID_gene [ensembl_protID] = gene
                except:
                    dic_protID_gene [ensembl_protID] = gene
        f.close()


def sum_effect_perGene(input_all_effects_file):

    dic_protID_sumDeleffect = defaultdict(list)

    f = open(input_all_effects_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        seq_mutPattern = slices[0]
        raw_prob = float(slices[1])
        accumuEffect = float(slices[2])

        ensemblID = seq_mutPattern.split('_')[0]

        if raw_prob > 0.48:
            dic_protID_sumDeleffect [ensemblID].append(accumuEffect)
    f.close()

    # print(dic_protID_sumDeleffect)
    list_gene_sumEffects = []
    dic_sumEffect_gene = defaultdict(list)
    try:
        for protID, list_delEffects in dic_protID_sumDeleffect.items():
            gene_name = dic_protID_gene [protID]
            sum_delEffect = 0
            for e in list_delEffects:
                sum_delEffect += e
            # done
            list_gene_sumEffects.append(sum_delEffect)
        ##
            dic_sumEffect_gene [sum_delEffect].append(gene_name)
    except:
        pass

    list_gene_sumEffects = np.unique(list_gene_sumEffects)
    list_gene_sumEffects = np.sort(list_gene_sumEffects)[::-1]

    i = 0
    j = 0
    for i in range(0, len(list_gene_sumEffects)):
        for each_gene in dic_sumEffect_gene[list_gene_sumEffects[i]]:
            # new_line = str(list_gene_sumEffects[i]) + ',' + each_gene
            i += 1
            if each_gene in list_CGC_confirmed_cancer_genes:
                j += 1
            print(i,j)
    return 0


sum_effect_perGene(input_all_effects_file = '/home/bwang/project/3d_mut_enrichment/Calculation/QuantitativeEffects/testing/drivermutation_lung.csv' )