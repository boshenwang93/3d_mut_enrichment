
import os

list_CGC_confirmed_cancer_genes = []
f = open('cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    if line.startswith('Gene') == 0:
        gene = line.split(',')[0]
        list_CGC_confirmed_cancer_genes.append(gene)
f.close()
# print(list_CGC_confirmed_cancer_genes)


dic_protID_gene = {}
for root, subdirs, files in os.walk('/home/bwang/data/cancer/mutation/pancancer/eachIsoform'):
    for each_file in files:
        ensembl_protID = each_file.split('.')[0]
        each_file_abs = os.path.join(root, each_file)
        i = 1
        f = open(each_file_abs, 'r')
        for line in f:
            line = line.strip()
            i += 1
            if i == 3:
                gene = line.split("\t")[0]
                try:
                    gene = gene.split('_')[0]
                    dic_protID_gene [ensembl_protID] = gene
                except:
                    dic_protID_gene [ensembl_protID] = gene
        f.close()

# print(dic_protID_gene)



total = 0
CGC = 0
nonCGC = 0

list_CGC = []
list_nonGCG  = []

list_unique_gene = []
f = open('ranked_pancancer.csv', 'r')
for line in f:
    line = line.strip()

    protID = line.split('_')[-3]
    try:
        gene_name = dic_protID_gene[protID]
        new_line = gene_name + ',' + line
        print(new_line)
    except:
        pass
f.close()