from collections import defaultdict


def SummarizeClusterEffect():
    
    #################################################################
    #### Two Options ################################################
    #### Option 1. raw effect file Pan-Cancer   #####################
    #### Option 2. tissue specific                ###################
    #################################################################
    #### Here is option 1 PAN-CANCER
    dic_pdbID_seqID = {} 
    dic_Residue_ListAllMut_AccumuEffect = defaultdict(list)
    f = open('/mnt/data/project/cancer/Specific/PanCancer/Merged_Final.csv', 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        recurrence = slices[0].strip()
        deleterious_effect = slices[2].strip()
        recurrence = int(recurrence)
        deleterious_effect = float(deleterious_effect)

        accumulative_deleterious_effect = recurrence * deleterious_effect

        gene = slices[3]
        protID = slices[4]
        transID = slices[5]
        identifier_gene = gene + '_' + protID + '_' + transID


        wildAA = slices[6]
        seqIndex = slices[7]
        mutAA = slices[8]

        pdbWindex = slices[9]
        pdb_residue_identifier = pdbWindex + '_'+ wildAA
        dic_pdbID_seqID [pdb_residue_identifier] = identifier_gene
        
        dic_Residue_ListAllMut_AccumuEffect [pdb_residue_identifier] .append(accumulative_deleterious_effect)
    f.close()


    #### Here is option 2 
    # dic_pdbID_seqID = {} 
    # dic_Residue_ListAllMut_AccumuEffect = defaultdict(list)
    # f = open('/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/mutation/meninges.rankedMut', 'r')
    # for line in f:
    #     line = line.strip()
    #     slices = line.split(',')

    #     accumulative_deleterious_effect = slices[-1]
    #     accumulative_deleterious_effect = float(accumulative_deleterious_effect)

    #     mut_identifier =  slices[-3]
    #     residue_identifier = mut_identifier[0:-1]
    #     dic_Residue_ListAllMut_AccumuEffect [residue_identifier] .append(accumulative_deleterious_effect)
    # f.close()



    #### Each Residue Have 19 substitution, Summerize All possible
    dic_Residue_AccumuEffect = {}
    for each_residue, list_allSubs_accumuEffect in dic_Residue_ListAllMut_AccumuEffect.items():
        sum_effect = 0 
        # if len(list_allSubs_accumuEffect) >1:
        #     print(each_residue)
        for each_subs_accumuEffect in list_allSubs_accumuEffect:
            sum_effect += each_subs_accumuEffect
        
        dic_Residue_AccumuEffect [each_residue] = sum_effect

    # for k,v in dic_Residue_AccumuEffect.items():
    #     print(k, v)

    #### Hash table for Residue => Structural Cluster
    dic_hostResidue_listAttachedResidue = defaultdict(list)
    f = open('/mnt/data/project/cancer/Feature/StructuralCluster/Refined_Poc_Plus_3Layer.csv', 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')
        host_residue = slices[0]
        
        for i in range(1, len(slices)):
            guest_residue = slices[i]
            dic_hostResidue_listAttachedResidue [host_residue].append(guest_residue)
    f.close()

    # print(dic_hostResidue_listAttachedResidue)

    #### Mapping Back to Existing Mut Host Residue
    for each_residue, sum_effect in dic_Residue_AccumuEffect.items():
        cluster_sumEffect = 0 
        
        guestList = dic_hostResidue_listAttachedResidue [each_residue]

        actual_guest_residueWmut = []
        # print(each_residue, guestList)
        for each_guest in guestList:
            try:
                cluster_sumEffect += dic_Residue_AccumuEffect [each_guest]
                actual_guest_residueWmut.append(each_guest)
            except:
                cluster_sumEffect += 0
        
        out_line = each_residue  + ',' + dic_pdbID_seqID [each_residue] + ',' +\
                   str(cluster_sumEffect) 
                #    + ',' +\
                #    str(dic_Residue_AccumuEffect [each_residue]) + ','+\
                #    str(actual_guest_residueWmut)
        print(out_line)

    return 0 


SummarizeClusterEffect()