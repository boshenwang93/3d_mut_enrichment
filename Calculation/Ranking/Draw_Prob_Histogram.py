import matplotlib.pyplot as plt




list_accumulative_Variant = []
f =open('Variant_Accumulative.csv', 'r')
for line in f:
    line = line.strip()
    sumEffect = line.split(' ')[-1]

    sumEffect = float(sumEffect)
    list_accumulative_Variant.append(sumEffect)
f.close()

# list_Discard_Outlier = []
# for e in list_accumulative_Variant:
#     if e < 5:
#         list_Discard_Outlier.append(e)

# print(len(list_accumulative_Variant))
# print(len(list_Discard_Outlier))


list_deleterious_effect = [] 
f = open('/home/bwang/project/3d_mut_enrichment/Model/COSMIC_missenseEffect_FullPDB.csv', 'r')
for line in f:
    line = line.strip()
    prob = line.split(',')[1]

    try:
        prob = float(prob)
        list_deleterious_effect.append(prob)
    except:
        pass
f.close()

f = open('/home/bwang/project/3d_mut_enrichment/Model/COSMIC_missenseEffect_PartPDB.csv', 'r')
for line in f:
    line = line.strip()
    prob = line.split(',')[1]

    try:
        prob = float(prob)
        list_deleterious_effect.append(prob)
    except:
        pass
f.close()


plt.hist(list_deleterious_effect, bins = 500)
plt.show()

