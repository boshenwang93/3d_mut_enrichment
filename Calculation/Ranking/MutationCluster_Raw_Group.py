from collections import defaultdict


dic_pdbID_seqID = {} 
dic_Residue_ListAllMut_AccumuEffect = defaultdict(list) 
f = open('/mnt/data/project/cancer/Specific/PanCancer/Merged_Final.csv', 'r')
# f = open('sample.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    recurrence = slices[0].strip()
    deleterious_effect = slices[2].strip()
    recurrence = int(recurrence)
    deleterious_effect = float(deleterious_effect)

    accumulative_deleterious_effect = recurrence * deleterious_effect

    gene = slices[3]
    protID = slices[4]
    transID = slices[5]
    identifier_gene = gene + '_' + protID + '_' + transID

    wildAA = slices[6]
    seqIndex = slices[7]
    mutAA = slices[8]

    pdbWindex = slices[9]
    pdb_residue_identifier = pdbWindex + '_'+ wildAA
    dic_pdbID_seqID [pdb_residue_identifier] = identifier_gene
    
    dic_Residue_ListAllMut_AccumuEffect [pdb_residue_identifier] .append(accumulative_deleterious_effect)

f.close()

###############################################################
#### Each Residue Have 19 substitution, Summerize All possible
###############################################################
dic_Residue_AccumuEffect = {}
for each_residue, list_allSubs_accumuEffect in dic_Residue_ListAllMut_AccumuEffect.items():
    sum_effect = 0 
    # if len(list_allSubs_accumuEffect) >1:
    #     print(each_residue)
    for each_subs_accumuEffect in list_allSubs_accumuEffect:
        sum_effect += each_subs_accumuEffect
    
    dic_Residue_AccumuEffect [each_residue] = sum_effect

# for k,v in dic_Residue_AccumuEffect.items():
#     print(k, v)
#### This section is DONE



#### Hash table for Residue => Structural Cluster
#### Here is Redundant Info, Need to trim
#### Only include mutated residue within cluster
dic_hostResidue_listAttachedResidue = defaultdict(list)
f = open('/mnt/data/project/cancer/Feature/StructuralCluster/Refined_Poc_Plus_3Layer.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    host_residue = slices[0]
    
    for i in range(1, len(slices)):
        guest_residue = slices[i]
        dic_hostResidue_listAttachedResidue [host_residue].append(guest_residue)
f.close()

# # print(dic_hostResidue_listAttachedResidue)


############################################################
#### Obatin mutation cluster (SUBSET of structural cluster)#
############################################################

#### Mapping Back to Existing Mut Host Residue
dic_hostMut_guestMut = {} 
dic_pdbWchain_listMutCluster = defaultdict(list)
for each_residue, sum_effect in dic_Residue_AccumuEffect.items():

    list_tmp_mutCluster = [each_residue]
    
    guestList = dic_hostResidue_listAttachedResidue [each_residue]
    actual_guest_residueWmut = []
    # print(each_residue, guestList)
    for each_guest in guestList:
        if each_guest in dic_Residue_AccumuEffect:
            actual_guest_residueWmut.append(each_guest)
            list_tmp_mutCluster.append(each_guest)
    
    dic_hostMut_guestMut [each_residue] = actual_guest_residueWmut

    seqPDB_pair = dic_pdbID_seqID[each_residue]
    dic_pdbWchain_listMutCluster [seqPDB_pair] .append(list_tmp_mutCluster)


# for k, v in dic_pdbWchain_listMutCluster.items():
#     for e in v:
#         print(k, e)

#######################################################################
### Check the overlapping statistic for Mutation Cluster within one PDB
#######################################################################
for each_pdb, list_mutCluster in dic_pdbWchain_listMutCluster.items():
    count_mut_cluster = len(list_mutCluster)

    list_naive_mutCluster = []
    for e in list_mutCluster:
        list_naive_mutCluster.append(e)

    count_overlapped_mutCluster = 0

    list_need_delete_mutCluster = []
    list_new_merged_mutClusterIndex = []

    for i in range(0, count_mut_cluster):
        for j in range(i+1, count_mut_cluster):
            mut_cluster_i = list_mutCluster [i]
            mut_cluster_j = list_mutCluster [j]

            size_i = len(mut_cluster_i)
            size_j = len(mut_cluster_j)

            count_common_residue = 0 
            for each_residue in mut_cluster_i:
                if each_residue in mut_cluster_j:
                    count_common_residue += 1
            
            common_proportion_i = count_common_residue / size_i
            common_proportion_j = count_common_residue / size_j

            if common_proportion_i >= 0.7:
                count_overlapped_mutCluster += 1
                list_need_delete_mutCluster.append(mut_cluster_i)
                list_need_delete_mutCluster.append(mut_cluster_j)
                index_pair = [i,j]
                if index_pair not in list_new_merged_mutClusterIndex:
                    list_new_merged_mutClusterIndex.append(index_pair)

            if common_proportion_j >= 0.7:
                count_overlapped_mutCluster += 1
                list_need_delete_mutCluster.append(mut_cluster_i)
                list_need_delete_mutCluster.append(mut_cluster_j)
                index_pair = [i,j]
                if index_pair not in list_new_merged_mutClusterIndex:
                    list_new_merged_mutClusterIndex.append(index_pair)
    
    # print(each_pdb, list_need_delete_mutCluster)
    ## Done Iteration


    #### UPDATE the mutCluster in New list contains grouped cluster
    list_new_grouped_ClusterIndex = []
    for i in range(0, len(list_new_merged_mutClusterIndex)):

        tmp_new_group_index = []

        IndexPair1 = list_new_merged_mutClusterIndex[i]
        indexOne = IndexPair1[0]
        indexTwo = IndexPair1[1]

        tmp_new_group_index.append(indexOne)
        tmp_new_group_index.append(indexTwo)

        for j in range(i, len(list_new_merged_mutClusterIndex)):
            IndexPair2 = list_new_merged_mutClusterIndex[j]
            indexThree = IndexPair2[0]
            indexFour =  IndexPair2[1]
            
            if indexOne in IndexPair2:
                if indexThree not in tmp_new_group_index:
                    tmp_new_group_index.append(indexThree)
                if indexFour not in tmp_new_group_index:
                    tmp_new_group_index.append(indexFour)
            if indexTwo in IndexPair2:
                if indexThree not in tmp_new_group_index:
                    tmp_new_group_index.append(indexThree)
                if indexFour not in tmp_new_group_index:
                    tmp_new_group_index.append(indexFour)
        ## Done tmp list

        count_existing = 0
        for each_element in tmp_new_group_index:
            
            for each_existing in list_new_grouped_ClusterIndex:
                if each_element in each_existing:
                    count_existing += 1
        ## Done
        if count_existing == 0:
            list_new_grouped_ClusterIndex.append(tmp_new_group_index)
    ## Done

    # print(list_mutCluster)

    # After adding new Grouped cluster, Deleter overlapping cluster 
    for each_need_delete in list_need_delete_mutCluster:
        try:
            list_mutCluster.remove(each_need_delete)
        except:
            pass

    #### Adding the Grouped Cluster
    for each_grouped_index in list_new_grouped_ClusterIndex:
        # print(each_grouped_index)
        new_cluster = [] 
        for each_old_cluster in each_grouped_index:
            for residue in list_naive_mutCluster[each_old_cluster]:
                if residue not in new_cluster:
                    new_cluster.append(residue)
        # print(new_cluster)
        list_mutCluster.append(new_cluster)

    dic_pdbWchain_listMutCluster [each_pdb] = list_mutCluster

    for k,v in dic_pdbWchain_listMutCluster.items():
        for e in v:
            print(k, e)
    
    # print( each_pdb, count_mut_cluster, 
    #            count_overlapped_mutCluster,
    #            len(list_mutCluster))

    # print( list_mutCluster, 
    #        list_new_grouped_ClusterIndex, 
    #        list_new_merged_mutClusterIndex)