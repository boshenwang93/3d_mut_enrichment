import os 
from collections import defaultdict 

import csv 
list_known_genes = []
with open('/home/bwang/project/3d_mut_enrichment/Calculation/Paralog/cancer_gene_census.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        entrez_geneID = row[0]
        list_known_genes.append(entrez_geneID)



list_tissues_good = ['lung',
'cervix',
'oesophagus',
'liver',
'ovary',
'thyroid',
'endometrium',
'haematopoietic_and_lymphoid_tissue',
'central_nervous_system',
'pancreas',
'large_intestine',
'skin',
'biliary_tract',
'urinary_tract',
'kidney',
'prostate',
'upper_aerodigestive_tract',
'breast',
'stomach']

for root, subdir_list, file_list in os.walk('/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/addedCluster/'):
    for each_file in file_list:

        file_abs = root + each_file
        dict_gene_listCount = defaultdict(list)
        
        f = open(file_abs, 'r')
        for line in f:
            line = line.strip()
            slices = line.split(',')

            deleterious_prob = float( slices[-4] )
            accumu_effect_mut = float(slices[-3])
            if deleterious_prob != 0:
                recurrence_mut = int(accumu_effect_mut / deleterious_prob)
            else:
                recurrence_mut = 1
            gene_nested = slices[3]
            gene = gene_nested.split('_')[0]

            dict_gene_listCount [gene] .append(recurrence_mut)
        f.close()


        dict_gene_totalCount = {}
        list_geneCount = []
        for k, v in dict_gene_listCount.items():
            total_count = 0
            for e in v:
                total_count += e
            list_geneCount.append(total_count)
            dict_gene_totalCount [k] = total_count
        
        list_geneCount.sort(reverse=True)

        threshold_index = int( 0.02 * len(list_geneCount) ) 
        threshold_totalmut = list_geneCount[threshold_index]

        tissue_name = each_file.split(".")[0]
        list_unique_good_gene = []
        if tissue_name in list_tissues_good:
            for k, v in dict_gene_totalCount.items():
                if v >= threshold_totalmut:
                    list_unique_good_gene.append(k)

        ## Check overlapping known
        count_known = 0 
        for e in list_unique_good_gene:
            if e in list_known_genes:
                out_entry = e + ",Known\n" 
                count_known += 1 
                # print(tissue_name, out_entry)
            else:
                out_entry = e + ",Novel\n" 
                # print(tissue_name, out_entry)


        if len(list_unique_good_gene) > 0:
            known_percent = count_known / len(list_unique_good_gene)
            print(tissue_name, len(list_unique_good_gene), known_percent)

        #    
