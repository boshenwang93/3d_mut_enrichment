import os 
from multiprocessing import Process, Pool


#### Hash table for ENSEMBL Prot and Transcript
dic_prot_trans = {} 
dic_trans_prot = {}

f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
for line in f_tmp:
    line = line.strip()
    if line.startswith('>'):
        slides = line.split(' ')
        protID = slides[0].strip()
        protID = protID[1:]
        protID = protID.split('.')[0].strip()

        trans_nested = slides[4].strip()
        transID = trans_nested.split(':')[1].strip()
        transID = transID.split('.')[0].strip()

        dic_prot_trans [protID] = transID
        dic_trans_prot [transID] = protID
f_tmp.close()

#### Obtain PDB-mappable genes
list_geneTransID = [] 
tmpF= open('/home/bwang/project/3d_mut_enrichment/Model/COSMIC_missenseEffect_PartPDB.csv', 'r')
for line in tmpF:
        line = line.strip()
        if line.startswith('Rindex') == 0 :
            slices = line.split(',')

            geneSymbol = slices[2].strip()
            transID = slices[4].strip()
            geneTrans = geneSymbol + '_' + transID
            if geneTrans not in list_geneTransID:
                list_geneTransID.append(geneTrans)
tmpF.close()

tmpF= open('/home/bwang/project/3d_mut_enrichment/Model/COSMIC_missenseEffect_FullPDB.csv', 'r')
for line in tmpF:
        line = line.strip()
        if line.startswith('Rindex') == 0 :
            slices = line.split(',')

            geneSymbol = slices[2].strip()
            transID = slices[4].strip()
            geneTrans = geneSymbol + '_' + transID
            if geneTrans not in list_geneTransID:
                list_geneTransID.append(geneTrans)
tmpF.close()


#### Obtain the HASH Table   mutation => Count in Pan-Cancer
dic_variant_PanCancerCount = {} 
for each_gene in list_geneTransID:
    mut_count_file = '/mnt/data/project/cancer/CosmicV92/Mutation/Count/missense/' +\
                     each_gene + '.count'

    f = open(mut_count_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')
        mut_pattern_nested = slices[-2].strip()
        PanCancer_Count = slices[-1].strip()
 
        identifier = each_gene + '_'+ mut_pattern_nested
        dic_variant_PanCancerCount [identifier] = PanCancer_Count
f.close()

## raw material is done


#### Generate Pan-Cancer Final File
tmpF= open('/home/bwang/project/3d_mut_enrichment/Model/COSMIC_missenseEffect_PartPDB.csv', 'r')
for line in tmpF:
        line = line.strip()
        if line.startswith('Rindex') == 0 :
            slices = line.split(',')

            geneSymbol = slices[2].strip()
            transID = slices[4].strip()
            geneTrans = geneSymbol + '_' + transID

            wildAA = slices[5].strip()
            seqIndex = slices[6].strip()
            mutAA = slices[7].strip()
            mut_pattern = wildAA + seqIndex + mutAA

            identifier = geneTrans + '_' + mut_pattern

            PanCancer_count = dic_variant_PanCancerCount [identifier]
            new_line = PanCancer_count + ',' + line 
            print(new_line)
tmpF.close()