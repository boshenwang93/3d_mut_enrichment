from collections import defaultdict


def Ranking_Gene_Deleterious_Level(input_Variant_effect_file, cutoff_effect):

    list_unique_gene = []
    dic_gene_listAccumuEffect = defaultdict(list)
    dic_gene_listSingleEffect = defaultdict(list)

    f = open(input_Variant_effect_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        recurrence = slices[0].strip()
        deleterious_effect = slices[2].strip()
        recurrence = int(recurrence)
        deleterious_effect = float(deleterious_effect)

        accumulative_deleterious_effect = recurrence * deleterious_effect
       
        gene = slices[3]
        protID = slices[4]
        transID = slices[5]        
        identifier_gene = gene + '_' + protID + '_' + transID
        if deleterious_effect >= cutoff_effect:
            dic_gene_listAccumuEffect [identifier_gene] .append(accumulative_deleterious_effect)
            dic_gene_listSingleEffect [identifier_gene] .append(deleterious_effect)
        else:
            pass

        if identifier_gene not in list_unique_gene:
            list_unique_gene.append(identifier_gene)
        else:
            pass
    f.close()


    dic_gene_AccumuEffect = {} 
    for each_gene, list_effect_byVariant in dic_gene_listAccumuEffect.items():
        sum_effect = 0
        for each_variant_AccumuEffect in list_effect_byVariant:
            sum_effect += each_variant_AccumuEffect
        dic_gene_AccumuEffect [each_gene] = sum_effect
    
    dic_gene_AvgDeleterious = {}    
    list_effect_gene = [] 
    for each_gene, list_effect_single in dic_gene_listSingleEffect.items():
        sum_effect = 0
        for each_variant_SingleEffect in list_effect_single:
            sum_effect += each_variant_SingleEffect
        avg_effect = sum_effect / len(list_effect_single)        

        if len(list_effect_single) >= 5:
            list_effect_gene.append(avg_effect)
            dic_gene_AvgDeleterious [each_gene] = avg_effect

    #### Sorting and Mapping
    list_sorted_AvgEffect = list_effect_gene
    list_sorted_AvgEffect.sort()

    dic_AvgEffect_Ranking = {}
    for i in range(0, len(list_sorted_AvgEffect)):
        effect_value = list_sorted_AvgEffect[i]
        dic_AvgEffect_Ranking [effect_value] = i
    
    list_out_info = []
    for each_gene, avgEffect in dic_gene_AvgDeleterious.items():
        out_info = each_gene +',' + str(avgEffect) + ','+\
                   str(dic_AvgEffect_Ranking [avgEffect])
        # print(out_info)
        list_out_info.append(out_info)
    
    list_sorted_out = sorted(list_out_info, 
                             key=lambda x: float(x.split(',')[-1]))
    for i in range(0, len(list_sorted_out)):
        print(list_sorted_out[i])

    return 0 


Ranking_Gene_Deleterious_Level('/mnt/data/project/cancer/Specific/PanCancer/Merged_Final.csv',
                               cutoff_effect = 0.6)
