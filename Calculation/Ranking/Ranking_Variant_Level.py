import os 
from collections import defaultdict

def Ranking_Variant(given_count_effect_file, cutoff_effect):

    ## Obtain Raw Accumulative info (Effect ** Recurrence)
    dic_variant_AccumuEffect = {} 
    dic_variant_singleEffect = {}
    list_AccumuEffect = []

    f = open(given_count_effect_file, 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        recurrence = slices[0].strip()
        deleterious_effect = slices[2].strip()
        recurrence = int(recurrence)
        deleterious_effect = float(deleterious_effect)

        accumulative_deleterious_effect = recurrence * deleterious_effect

        gene = slices[3]
        protID = slices[4]
        transID = slices[5]
        identifier_gene = gene + '_' + protID + '_' + transID

        wildAA = slices[6]
        seqIndex = slices[7]
        mutAA = slices[8]
        pdbWindex = slices[9]

        if deleterious_effect >= cutoff_effect:
            identifier_variant = identifier_gene + '_' + wildAA + seqIndex + mutAA
            dic_variant_singleEffect [identifier_variant] = deleterious_effect
            dic_variant_AccumuEffect [identifier_variant] = accumulative_deleterious_effect
            list_AccumuEffect.append(accumulative_deleterious_effect)
        else:
            pass
    f.close()


    #### Sorting and Mapping accumuEffect => Ranking Index
    list_sorted_AccumuEffect = list_AccumuEffect
    list_sorted_AccumuEffect.sort()

    dic_AccumuEffect_Ranking = {}
    for i in range(0, len(list_sorted_AccumuEffect)):
        effect_value = list_sorted_AccumuEffect[i]
        dic_AccumuEffect_Ranking [effect_value] = i
    
    #### Ranking Variants
    list_gene_wPDB = []
    # list_gene_WithRank1per = []
    ## mapping back to the variants
    list_output_ranked_info = []
    for variant, effect_value in dic_variant_AccumuEffect.items():
        ranking = dic_AccumuEffect_Ranking [effect_value]
        percent_ranking = ranking / len(list_AccumuEffect)

        geneSymbol = variant.split('_')[0]
        if geneSymbol not in list_gene_wPDB:
            list_gene_wPDB.append(geneSymbol)

        top_per_rank = (1 - percent_ranking)*100 

        new_line = str(top_per_rank) + ',' + str(ranking) + ',' +\
                    variant + ',' + str(dic_variant_singleEffect[variant]) +',' +\
                    str(effect_value)
        list_output_ranked_info.append(new_line)

            # if geneSymbol not in list_gene_WithRank1per:
            #     list_gene_WithRank1per.append(geneSymbol)
    
    list_sorted_out = sorted(list_output_ranked_info, 
                             key=lambda x: float(x.split(',')[3]))

   
    for i in range(0, len(list_sorted_out)):
        out_line = list_sorted_out[i]
        print(out_line)

    return 0 


input_Variant_effect_file = '/mnt/data/project/cancer/Specific/PanCancer/Merged_Final.csv'
Ranking_Variant(input_Variant_effect_file)