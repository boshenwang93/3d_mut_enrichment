import os 



dic_hostResidue_clusterEffect = {} 
f = open('/mnt/data/project/cancer/Specific/PanCancer/cluster_3layer_mutual_accumuEffect.csv', 'r')
for line in f:
    line = line.strip()
    pdb_index = line.split(',')[0]
    pdb_index = pdb_index[0:-2]
    cluster_effect = line.split(',')[-1]

    dic_hostResidue_clusterEffect [pdb_index] = cluster_effect
f.close()

# print(dic_hostResidue_clusterEffect)

for root, subdir_list, file_list in os.walk('/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/mutation/'):
    for each_file in file_list:
        file_abs = root + each_file
        
        list_entry = [] 
        list_productEffect = []
        dic_variant_productEffect = {}

        tmpF = open(file_abs, 'r')
        for line in tmpF:
            line = line.strip()
            slices = line.split(',')

            pdb_index = slices[-3]
            mut_effect = slices[-1]
            
            try:
                cluster_effect = dic_hostResidue_clusterEffect [pdb_index]
            except:
                cluster_effect = 0 

            mut_effect = float(mut_effect)
            cluster_effect = float(cluster_effect)

            ### Option HERE product or Cluster
            product_effect = 1 * cluster_effect
            # product_effect = mut_effect * cluster_effect

            new_entry = line + ',' + str(cluster_effect) + ',' + str(product_effect)
            dic_variant_productEffect [new_entry] = product_effect

            list_entry.append(new_entry)
            list_productEffect.append(product_effect)
        tmpF.close()
        
        ## sorting Product Effect 
        list_sorted_productEffect = list_productEffect
        list_sorted_productEffect.sort()
        
        out_file_abs = '/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/addedCluster/' + each_file
        out_F = open(out_file_abs, 'w')

        dic_productEffect_Ranking = {}
        for i in range(0, len(list_sorted_productEffect)):
            effect_value = list_sorted_productEffect[i]
            dic_productEffect_Ranking [effect_value] = i
        
        list_output_ranked_info = []        
        for variant, productEffect in dic_variant_productEffect.items():
            ranking = dic_productEffect_Ranking [productEffect]
            percent_ranking = ranking / len(list_entry)

            top_per_rank = (1 - percent_ranking)*100 

            new_line = str(top_per_rank) + ',' + variant
            list_output_ranked_info.append(new_line)
        ## Done

        list_sorted_out = sorted(list_output_ranked_info, 
                             key=lambda x: float(x.split(',')[0]))
   
        for i in range(0, len(list_sorted_out)):
            out_line = list_sorted_out[i]
            out_line += "\n"
            out_F.write(out_line)
        out_F.close()
