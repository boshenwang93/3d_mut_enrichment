import os 

import csv 
list_known_genes = []
with open('/home/bwang/project/3d_mut_enrichment/Calculation/Paralog/cancer_gene_census.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        entrez_geneID = row[0]
        list_known_genes.append(entrez_geneID)



list_tissues_good = ['lung',
'cervix',
'oesophagus',
'liver',
'ovary',
'thyroid',
'endometrium',
'haematopoietic_and_lymphoid_tissue',
'central_nervous_system',
'pancreas',
'large_intestine',
'skin',
'biliary_tract',
'urinary_tract',
'kidney',
'prostate',
'upper_aerodigestive_tract',
'breast',
'stomach']

for root, subdir_list, file_list in os.walk('/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/addedCluster/'):
    for each_file in file_list:

        file_abs = root + each_file
        list_unique_good_gene = []

        f = open(file_abs, 'r')
        for line in f:
            line = line.strip()
            slices = line.split(',')

            deleterious_prob = float( slices[-4] )
            cluster_ranking = float(  slices[0] )
            mutation_accumu_ranking = float( slices[1] )
            
            gene_nested = slices[3]
            gene = gene_nested.split('_')[0]

            if deleterious_prob >= 0.7:
                if mutation_accumu_ranking <= 20:
                    if cluster_ranking <= 20:
                        if gene not in list_unique_good_gene:
                            list_unique_good_gene.append(gene)
        f.close()
        
        count_known = 0 
        out_file_abs = '/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/genes/' + each_file
        # outF = open(out_file_abs, 'w')
        for e in list_unique_good_gene:
            if e in list_known_genes:
                out_entry = e + ",Known\n" 
                # outF.write(out_entry)
                count_known += 1 
            else:
                out_entry = e + ",Novel\n" 
                # outF.write(out_entry)
        # outF.close()

        if len(list_unique_good_gene) > 0:
            known_percent = count_known / len(list_unique_good_gene)

            tissue_name = each_file.split(".")[0]
            new_line = tissue_name + "\t" + str(len(list_unique_good_gene)) + "\t"+ str(known_percent)
            if tissue_name in list_tissues_good:
                print(new_line)
