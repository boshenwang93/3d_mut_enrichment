import os 
from multiprocessing import Process, Pool


#### Hash table for ENSEMBL Prot and Transcript
dic_prot_trans = {} 
dic_trans_prot = {}

f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
for line in f_tmp:
    line = line.strip()
    if line.startswith('>'):
        slides = line.split(' ')
        protID = slides[0].strip()
        protID = protID[1:]
        protID = protID.split('.')[0].strip()

        trans_nested = slides[4].strip()
        transID = trans_nested.split(':')[1].strip()
        transID = transID.split('.')[0].strip()

        dic_prot_trans [protID] = transID
        dic_trans_prot [transID] = protID
f_tmp.close()



########## Parse Each Tissue Mutation 
def ParseMutation(assigned_tissue_mutation_file):
    
    #### Push All missense Variant Per Tissue into a list
    list_redundant_mutation = [] 

    f = open(assigned_tissue_mutation_file, 'r' )
    for line in f:
        line = line.strip()
        slices_line = line.split(",")

        gene_name_nested = slices_line[0].strip()
        gene_symbol = ''
        try:
            gene_symbol += gene_name_nested.split('_')[0].strip()
        except:
            gene_symbol += gene_name_nested
        
        ensembl_transcriptID = slices_line[6].strip()
        ensembl_transcriptID = ensembl_transcriptID.split('.')[0]
        

        mutation_type = slices_line[2].strip()
        if mutation_type == 'Substitution - Missense':
            mutation_pattern_nested = slices_line[3].strip()
            mutation_nested = mutation_pattern_nested.split('.')[1].strip()
            wild_AA = mutation_nested[0]
            seqIndex = mutation_nested[1:-1]
            mut_AA = mutation_nested[-1]

            try:
                protID = dic_trans_prot[ensembl_transcriptID]
                mut_entry = protID + '_' + wild_AA + '_' + seqIndex + '_' + mut_AA
                list_redundant_mutation.append(mut_entry)
            except:
                pass
    f.close()

    #### Count the mutation patterns 
    file_name = assigned_tissue_mutation_file.split('/')[-1]
    file_name = file_name.split('.')[0].strip()
    out_file = out_CountMut_folder + file_name + '.count'
    outF = open(out_file, 'w')

    tmpF = open(known_effect_file, 'r')
    for line in tmpF:
        line = line.strip()
        if line.startswith('Rindex') == 0 :
            slices = line.split(',')

            protID = slices[3]
            wildAA = slices[5]
            seqIndex = slices[6]
            mutAA = slices[7]

            mut_identifier = protID + '_' + wildAA + '_' + seqIndex + '_' + mutAA

            recurrence = 0 
            for e in list_redundant_mutation:
                if e == mut_identifier:
                    recurrence += 1
            
            if recurrence != 0:
                new_entry = str(recurrence) + ',' + line + "\n" 
                # print(new_entry)
                outF.write(new_entry)
    tmpF.close()
    outF.close()
    return 0 


in_RawMut_folder = '/mnt/data/project/cancer/Specific/tissueSpecific/rawMut/'
out_CountMut_folder = '/mnt/data/project/cancer/Specific/tissueSpecific/Count/PartPDB/'
known_effect_file = '/home/bwang/project/3d_mut_enrichment/Model/COSMIC_missenseEffect_PartPDB.csv'


list_file_path = []
for root, subdirs, files in os.walk(in_RawMut_folder):
    for each_file in files:
        file_abs = root + each_file 
        if each_file.endswith('mutation'):
            list_file_path.append(file_abs)


with Pool(processes= 15 ) as pool:
    pool.map(ParseMutation, list_file_path)


#################################################
#### Merge FullPDB & PartPDB ####################
#################################################

# import os 
# in_Full_folder =  '/mnt/data/project/cancer/Specific/tissueSpecific/Count/FullPDB/'
# in_Part_folder =  '/mnt/data/project/cancer/Specific/tissueSpecific/Count/PartPDB/'
# out_merge_folder = '/mnt/data/project/cancer/Specific/tissueSpecific/Count/Merged/'
# for root, subdirs, files in os.walk(in_Full_folder):
#     for each_file in files:
#         file_abs = root + each_file 
#         if each_file.endswith('.count'):
#             out_merged_file = out_merge_folder + each_file
#             outF = open(out_merged_file, 'w')

#             full_count_file = in_Full_folder + each_file
#             part_count_file = in_Part_folder + each_file

#             tmpF = open(full_count_file, 'r')
#             for line in tmpF:
#                 outF.write(line)
#             tmpF.close()
#             tmpF = open(part_count_file, 'r')
#             for line in tmpF:
#                 outF.write(line)
#             tmpF.close()

#             outF.close()
            