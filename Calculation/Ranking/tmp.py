

# f = open('/mnt/data/project/cancer/Specific/PanCancer/PartPDB_count_final.csv', 'r')
# for line in f:
#     line = line.strip()
#     print(line)
# f.close()

# f = open('/mnt/data/project/cancer/Specific/PanCancer/FullPDB_count_final.csv', 'r')
# for line in f:
#     line = line.strip()
#     print(line)
# f.close()

import scipy.stats as ss
import matplotlib.pyplot as plt 
import numpy as np 
import os 

##############################################################

list_cancer_Census_gene = [] 
f  = open('/home/bwang/project/3d_mut_enrichment/Calculation/Paralog/cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    geneSymbol = line.split(',')[0].strip()
    if geneSymbol not in list_cancer_Census_gene:
        list_cancer_Census_gene.append(geneSymbol)
f.close()

list_cancer_sumEffect = []
list_noncancer_sumEffect = []

f = open('test', 'r')
for line in f:
    line = line.strip()
    sumEffect = line.split(',')[-1].strip()
    gene = line.split('_')[0].strip()

    sumEffect = float(sumEffect)

    if sumEffect < 2000:
        if gene in list_cancer_Census_gene:
            list_cancer_sumEffect.append(sumEffect)
        else:
            list_noncancer_sumEffect.append(sumEffect)
    
    if sumEffect > 750:
        print(gene)
f.close()

data_to_plot = [list_cancer_sumEffect, list_noncancer_sumEffect]

# Create a figure instance
fig = plt.figure(1, figsize=(9, 6))

# Create an axes instance
ax = fig.add_subplot(111)

# Create the boxplot
bp = ax.boxplot(data_to_plot)

ax.set_xticklabels(['Known Cancer Gene',  'Gene Not Classified as Cancer Census'])

# Save the figure
fig.savefig('fig.png', bbox_inches='tight')



# list_ranking_rawCount = ss.rankdata(list_mutCount)
# list_ranking_effect = ss.rankdata(list_Effect)

# sum_rawcount_rank = 0
# sum_effect_rank = 0 
# for e in list_cancer_index:
#     sum_rawcount_rank += list_ranking_rawCount[e]
#     sum_effect_rank += list_ranking_effect[e]