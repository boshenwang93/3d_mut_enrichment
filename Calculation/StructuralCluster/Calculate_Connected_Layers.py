
import random

def Calculate_Total_Layer(firstLayer_info_file):
    
    list_unique_residue = []
    dic_residue_FirstLayer = {} 
    f = open(firstLayer_info_file, 'r')
    for line in f:
        line = line.strip() 
        slices = line.split(',')

        host_residue = slices[0]
        host_residue = host_residue[7:]
        
        list_attacedFirstLayer = []
        for i in range(1, len(slices)-1):
            list_attacedFirstLayer.append(slices[i])
        
        dic_residue_FirstLayer[host_residue] = list_attacedFirstLayer
        if host_residue not in list_unique_residue:
            list_unique_residue.append(host_residue)
    f.close()
    
    
    initial_residue = random.choice(list_unique_residue)
    total_residue = len(list_unique_residue)
    
    list_residue_accumulate = [initial_residue]
    accumulate_residue = 1 
    
    list_redundant_count =[] 
    while accumulate_residue < total_residue:
        for each_host in list_residue_accumulate:
            current_included_residue = len(list_residue_accumulate)
            list_redundant_count.append(current_included_residue)

            list_tmp_attached = dic_residue_FirstLayer [each_host]
            for each_candidate in list_tmp_attached:
                if each_candidate not in list_residue_accumulate:
                    list_residue_accumulate.append(each_candidate)
                    accumulate_residue += 1
            
    list_unique_count = []
    for e in list_redundant_count:
        if e not in list_unique_count:
            list_unique_count.append(e)
    print(len(list_unique_count))

    return 0



Calculate_Total_Layer (firstLayer_info_file = '/mnt/data/project/cancer/Feature/StructuralCluster/3layerClean/6FZV_A.1layer')