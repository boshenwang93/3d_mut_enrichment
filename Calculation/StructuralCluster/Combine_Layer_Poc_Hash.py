
import os
from collections import defaultdict

# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}



poc_file = '/home/bwang/data/cancer/calculation/residuecluster/PDB_pocket_layer.csv'
dic_hostResidue_pocAccompany = defaultdict(list)
f = open(poc_file, 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    host_residue = slices[0]

    for i in range(1, len(slices)-1):
        dic_hostResidue_pocAccompany [host_residue].append(slices[i])
f.close()

# print(dic_hostResidue_pocAccompany)
# '6M5O_B_293_R': ['268_F', '269_K', '271_A', '294_K', '309_Y', '311_F', '312_E']



layer_file = '/home/bwang/data/cancer/calculation/residuecluster/3layer.csv'
f = open(layer_file, 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    try:
        host_residue = slices[0]
        host_index = host_residue[0:-3]
        host_tri = host_residue[-3:]
        host_residue = host_index + dic_tri_single[host_tri]

        list_alpha_residue = []
        for i in range(1, len(slices)-1):
            layer_residue_index = slices[i][0:-3]
            layer_residue_tri =  slices[i][-3:]
            layer_residue_new = layer_residue_index + dic_tri_single[layer_residue_tri]
            list_alpha_residue.append(layer_residue_new)
        ## alpha-edge layer residues is DONE
        list_combined_alphaPOC_residue = list_alpha_residue

        # try:
        #     list_poc_residue = dic_hostResidue_pocAccompany [host_residue]
        #     for e in list_poc_residue:
        #         if e not in list_alpha_residue:
        #             list_combined_alphaPOC_residue.append(e)
        # except:
        #     ## ignore residue Does Not in pocket regions
        #     pass
        # ## add pocket residue DONE

        new_line = host_residue + ','
        for e in list_combined_alphaPOC_residue:
            new_line += e + ','
        print(new_line)

    except:
        pass
f.close()

# print(dic_hostResidue_AttachedLayer)
# 'Q9NVM9_92_THR': ['75_LEU', '91_TRP', '93_GLN', '94_GLU',]

