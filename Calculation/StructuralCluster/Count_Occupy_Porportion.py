
import random
import os 

def Calculate_Total_Layer(firstLayer_info_file):
    
    list_unique_residue = []
    dic_residue_FirstLayer = {} 
    f = open(firstLayer_info_file, 'r')
    for line in f:
        line = line.strip() 
        slices = line.split(',')

        host_residue = slices[0]
        host_residue = host_residue[7:]
        
        list_attacedFirstLayer = []
        for i in range(1, len(slices)-1):
            list_attacedFirstLayer.append(slices[i])
        
        dic_residue_FirstLayer[host_residue] = list_attacedFirstLayer
        if host_residue not in list_unique_residue:
            list_unique_residue.append(host_residue)
    f.close()

    total_residue = len(list_unique_residue)
    for k,v in dic_residue_FirstLayer.items():
        attached_residue = len(v)
        porportion = attached_residue / total_residue
        print(porportion)
    return 0 


assigned_folder = '/mnt/data/project/cancer/Feature/StructuralCluster/3layerClean/'
for root, dirs, files in os.walk(assigned_folder):
    for each_file in files:
        if each_file.endswith(".3layer"):
            file_abs = root + each_file
            Calculate_Total_Layer (file_abs)