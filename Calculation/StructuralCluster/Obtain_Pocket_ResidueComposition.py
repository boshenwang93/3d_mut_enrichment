from collections import defaultdict
import os
from multiprocessing import Process, Pool

in_folder =  '/home/bwang/data/cancer/calculation/structure/AF/part1'
out_folder = '/home/bwang/poc/'
count_threads = 6

# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}


def Parse_Pocket_ResidueComposition(castp_poc_file):

    dic_pocID_listResidueComposition = defaultdict(list)

    f = open(castp_poc_file, "r")
    for line in f:
        if line.startswith("ATOM"):
            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            residue_ThreeAbbv = ""
            for i in range(17, 20, 1):
                residue_ThreeAbbv += line[i]
            residue_ThreeAbbv = residue_ThreeAbbv.upper().strip()

            residueAA = dic_tri_single[residue_ThreeAbbv]

            residue_identifier = residue_seq_number + '_' + residueAA

            # pocket id
            poc_id = ""
            for i in range(68, 72, 1):
                poc_id += line[i]
            poc_id = poc_id.strip()

            dic_pocID_listResidueComposition [poc_id] .append(residue_identifier)
    f.close()

    #### Iterate Insert same pocket residue, But REDUNDANT
    dic_residue_connectedResidue = defaultdict(list)
    for k,v in dic_pocID_listResidueComposition.items():
        list_thisPoc_residue = v
        for each_res in list_thisPoc_residue:
            for other_res in list_thisPoc_residue:
                if each_res != other_res:
                    dic_residue_connectedResidue[each_res].append(other_res)

    ## To discard redundant entry
    dic_residue_UniquePocResidue = {}
    for k,v in dic_residue_connectedResidue.items():
        list_unique_poc = []
        for e in v:
            if e not in list_unique_poc:
                list_unique_poc.append(e)
        dic_residue_UniquePocResidue [k] = list_unique_poc
    ##

    pdbID = castp_poc_file.split('.poc')[0]
    pdbID = pdbID.split('/')[-1]
    ## for AlphaFold PDB ID
    pdbID = pdbID.split('F1')[0]
    pdbID = 'AF_' + pdbID[3:-1]

    out_file = os.path.join(out_folder, pdbID)
    outF = open(out_file, 'w')

    ## Print out the results
    for k,v in dic_residue_UniquePocResidue.items():
        out_line = pdbID + '_' + k + ','
        for e in v:
            out_line += e + ','

        out_line += "\n"
        outF.write(out_line)
        # print(out_line)
    outF.close()
    return dic_residue_UniquePocResidue



list_poc_files = []
for root, dirs, files in os.walk(in_folder):
    for each_dir in dirs:
        each_dir_abs = os.path.join(root, each_dir)

        for root2, subdir_list2, file_list2 in os.walk(each_dir_abs):
            for each_file in file_list2:
                if each_file.endswith(".poc"):
                    file_abs = os.path.join(each_dir_abs, each_file)
                    list_poc_files.append(file_abs)



with Pool(processes= count_threads) as pool:
    pool.map(Parse_Pocket_ResidueComposition, list_poc_files)

# def Parse_Poc_Area_Volume ():
#     dic_pocID_SASA = {}
#     dic_pocID_AccessibleVolume = {}
#     return 0