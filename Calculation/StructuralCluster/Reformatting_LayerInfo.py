import numpy as np

Raw_Layer_Info_File = '/home/bwang/data/cancer/calculation/structure/exact_residue_PDB.txt'
out_cleanLayer_file = '/home/bwang/2layer_partPDB.txt'

header = 'PDB' ## or 'AlphaFold'

outF = open(out_cleanLayer_file, 'w')

list_host_residue = []
dic_residue_firstLayerComp = {}
dic_residue_secondLayerComp = {}
dic_residue_thirdLayerComp = {}

f = open(Raw_Layer_Info_File, 'r')
for line in f:
    line = line.strip()
    line_slices = line.split(',')

    try:
        pdb_w_chain = line_slices[0].strip()
        # PDB OPTION
        if header == 'PDB':
            pdb_w_chain = pdb_w_chain.strip()
        # Alphafold OPTION
        elif header == 'AlphaFold':
            pdb_w_chain = pdb_w_chain.split('F1')[0]
            pdb_w_chain = pdb_w_chain[3:-1]

        nested_residue_interest = line_slices[1].strip()
        host_residue_index = nested_residue_interest.split(':')[1].strip()
        host_residue_aa = nested_residue_interest.split(':')[2].strip()
        host_residue_info = pdb_w_chain + '_' +\
            host_residue_index + '_' + host_residue_aa

        layer_info = line_slices[2].strip()

        list_host_residue.append(host_residue_info)

        list_surround_composition = []
        for i in range(3, len(line_slices)):
            residue_index = line_slices[i].split(':')[1].strip()
            residue_aa = line_slices[i].split(':')[2].strip()
            residue_aa = residue_aa[0:3]
            residue_sum = residue_index + '_' + residue_aa
            list_surround_composition.append(residue_sum)

        if layer_info == 'firstlayer':
            dic_residue_firstLayerComp[host_residue_info] = list_surround_composition
        elif layer_info == 'secondlayer':
            dic_residue_secondLayerComp[host_residue_info] = list_surround_composition
        elif layer_info == 'thirdlayer':
            dic_residue_thirdLayerComp[host_residue_info] = list_surround_composition

    except:
        pass
f.close()

print('reading is done')

list_unique_residue = np.unique(list_host_residue)
for each_residue in list_unique_residue:
    try:
        new_line = each_residue + ','

        for e in dic_residue_firstLayerComp[each_residue]:
            new_line += e + ','
        try:
            for e in dic_residue_secondLayerComp[each_residue]:
                new_line += e + ','
        except:
            new_line += ''
        # try:
        #     for e in dic_residue_thirdLayerComp[each_residue]:
        #         new_line += e + ','
        # except:
        #     new_line += ''
        # print(new_line)

        new_line += "\n"
        outF.write(new_line)
    except:
        pass


outF.close()
