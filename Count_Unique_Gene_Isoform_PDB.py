list_unique_gene = []
list_unique_prot = []
list_unique_pdb = []


f = open('PartPDB_Gene_BestIsoform', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    gene = slices[0]
    prot = slices[1]
    pdb = slices[2]

    if gene not in list_unique_gene:
        list_unique_gene.append(gene)
    if prot not in list_unique_prot:
        list_unique_prot.append(prot)
    if pdb not in list_unique_pdb:
        list_unique_pdb.append(pdb)
    else:
        print(pdb)
f.close()

print(len(list_unique_gene), len(list_unique_prot), len(list_unique_pdb))