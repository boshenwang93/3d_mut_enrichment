from collections import defaultdict 


###########################################################
#### COSMIC will map mutation to different isoforms #######
#### Choose the isoform that has most covered structure  ##
#### Revised by B.W. Aug 2021 #############################
###########################################################

raw_ENSEMBL_seq_Withallisoform = '/home/bwang/data/cancer/Homo_sapiens.GRCh38.pep.all.fa'
full_covered_identity_csv_file = '/home/bwang/data/structure/AF/mapStructure/Results/fully_covered_unique_PDB.csv'


########################################
########################################
#### Load the Gene ==> list of isoforms 
########################################

#### Hash table for ENSEMBL Prot and Transcript
dic_geneID_prot = defaultdict(list)
dic_prot_geneID = {}
f_tmp = open(raw_ENSEMBL_seq_Withallisoform, 'r')
for line in f_tmp:
    line = line.strip()
    try:
        if line.startswith('>'):
            slides = line.split(' ')

            protID = slides[0].strip()
            protID = protID[1:]
            # ignore the seq version
            protID = protID.split('.')[0].strip()

            geneSymbol_nested = slides[7].strip()
            geneID = geneSymbol_nested.split(':')[1].strip()

            dic_geneID_prot [geneID].append(protID)
            dic_prot_geneID [protID] = geneID
    except:
        ## ignore small amount of novel gene without gene symbol 
        # print(line)
        pass
f_tmp.close()

# print(dic_prot_geneID)

########################################
########################################
#### For Full Coverage PDB
########################################
list_unique_geneID = []
dic_protID_twoWayIdenSum = {}
dic_geneID_allSumIden = defaultdict(list)
dic_protID_PDBchain = {}

f = open(full_covered_identity_csv_file, 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    protID = slices[0].strip()
    protID = protID[1:]
    protID = protID.split('.')[0]

    pdbChain = slices[1].strip()
    pdbChain = pdbChain[1:7]

    iden_seq = slices[2].strip()
    iden_pdb = slices[3].strip()
    sum_iden = float(iden_pdb) + float(iden_seq)
    
    try:
        geneID = dic_prot_geneID[protID]
        if geneID not in list_unique_geneID:
            list_unique_geneID.append(geneID)    

        dic_protID_twoWayIdenSum [protID] = sum_iden
        dic_geneID_allSumIden [geneID].append(sum_iden)
        dic_protID_PDBchain [protID] = pdbChain
    except:
        ## ignore small amount of novel gene without gene symbol 
        pass
f.close()

# print((dic_geneID_allSumIden))


list_considered_genewIsoform = []
for each_gene in list_unique_geneID:
    max_sumIden = max(dic_geneID_allSumIden[each_gene])

    for each_isoform in dic_geneID_prot[each_gene]:
        try:
            if dic_protID_twoWayIdenSum [each_isoform] == max_sumIden:
                out_entry = each_gene + ',' + each_isoform + ',' + dic_protID_PDBchain[each_isoform]
                ## Random Select One if On-tier condition exist 
                if each_gene not in list_considered_genewIsoform:
                    list_considered_genewIsoform.append(each_gene)
                    print(out_entry)
        except:
            # print(each_isoform)
            pass