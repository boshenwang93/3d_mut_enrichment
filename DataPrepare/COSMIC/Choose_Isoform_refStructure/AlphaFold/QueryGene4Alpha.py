from collections import defaultdict 
import os


raw_ENSEMBL_seq_Withallisoform = '/home/bwang/data/cancer/Homo_sapiens.GRCh38.pep.all.fa'
full_covered_identity_csv_file = '/home/bwang/data/cancer/fullPDB_gene_isoform.csv'
part_covered_identity_csv_file = '/home/bwang/data/cancer/partPDB_gene_isoform.csv'


########################################
########################################
#### Load the Gene ==> list of isoforms 
########################################

#### Hash table for ENSEMBL Prot and Transcript
dic_geneID_prot = defaultdict(list)
dic_prot_geneID = {}
f_tmp = open(raw_ENSEMBL_seq_Withallisoform, 'r')
for line in f_tmp:
    line = line.strip()
    try:
        if line.startswith('>'):
            slides = line.split(' ')

            protID = slides[0].strip()
            protID = protID[1:]

            geneSymbol_nested = slides[7].strip()
            geneID = geneSymbol_nested.split(':')[1].strip()

            dic_geneID_prot [geneID].append(protID)
            dic_prot_geneID [protID] = geneID
    except:
        ## ignore small amount of novel gene without gene symbol 
        # print(line)
        pass
f_tmp.close()


######## KICK OFF gene with PDB covered
list_gene_wPDB = []
f = open(full_covered_identity_csv_file, 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    geneID = slices[0]
    list_gene_wPDB.append(geneID)
f.close()

f = open(part_covered_identity_csv_file, 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    geneID = slices[0]
    list_gene_wPDB.append(geneID)
f.close()

for k,v in dic_prot_geneID.items():
    if v not in list_gene_wPDB:
        origin_fasta_path = '/home/bwang/data/cancer/mapPDB/ensemblFasta/' + k + '*'
        dest_fasta_path = '/home/bwang/data/structure/AF/mapStructure/query/'

        cp_cmd = 'cp' + "\t" + origin_fasta_path + "\t" +\
                  dest_fasta_path
        os.system(cp_cmd)