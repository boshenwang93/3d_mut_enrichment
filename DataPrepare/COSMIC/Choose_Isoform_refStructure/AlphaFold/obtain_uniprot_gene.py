import os 


alphafold_dir = '/home/bwang/data/structure/AF/predictPDB/'

# list_unique_uniprot = []
# for root, subdirs, file_list in os.walk(alphafold_dir):
#     for each_file in file_list:
#         uniprot_ID = each_file.split('-')[1]
#         uniprot_ID = uniprot_ID.strip()
#         if uniprot_ID not in list_unique_uniprot:
#             list_unique_uniprot.append(list_unique_uniprot)





list_gene_wPDB = []
tmpF = open('/home/bwang/data/cancer/fullPDB_gene_isoform.csv', 'r')
for line in tmpF:
    line = line.strip()
    geneid = line.split(',')[0]
    if geneid not in list_gene_wPDB:
        list_gene_wPDB.append(geneid)
tmpF.close()

tmpF = open('/home/bwang/data/cancer/partPDB_gene_isoform.csv', 'r')
for line in tmpF:
    line = line.strip()
    geneid = line.split(',')[0]
    if geneid not in list_gene_wPDB:
        list_gene_wPDB.append(geneid)
tmpF.close()



list_all_gene = []
dic_gene_uniprot = {}
tmpF = open('uniprot_gene_mapping.tab', 'r')
for line in tmpF:
    line = line.strip()
    geneid = line.split("\t")[1]
    uniprotID = line.split("\t")[0]
    dic_gene_uniprot [geneid] = uniprotID
    if geneid not in list_all_gene:
        list_all_gene.append(geneid)
tmpF.close()



origin_AF_folder = '/home/bwang/data/structure/AF/predictPDB/'
selected_AF_folder = '/home/bwang/data/structure/AF/selected_AF/'

list_gene_requireAlpha = []
for each_gene in list_all_gene:
    if each_gene not in list_gene_wPDB:
        list_gene_requireAlpha.append(each_gene)
        out_line = each_gene + ',' + dic_gene_uniprot[each_gene]
        # print(out_line)

        ### select the homology modeling that we use 
        uniprot = dic_gene_uniprot [each_gene]
        AF_file_name = 'AF-' + uniprot + '-F1-model_v1.pdb'
        orig_path = os.path.join(origin_AF_folder, AF_file_name)
        cp_cmd = 'cp ' + orig_path + '\t' + selected_AF_folder
        os.system(cp_cmd)