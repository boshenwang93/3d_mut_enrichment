

import os 

f = open('alphafold_gene_list.csv', 'r')
for line in f:
    line = line.strip()
    ensembl_protID_WOversion = line.split(',')[1]
    
    file_name = ensembl_protID_WOversion + '*'
    old_path = os.path.join('/home/bwang/data/cancer/mapPDB/ensemblFasta', file_name)
    new_path = '/home/bwang/data/cancer/calculate/MSA/homology/query'
    cp_cmd = 'cp' + "\t" + old_path + "\t" + new_path
    os.system(cp_cmd)
f.close()