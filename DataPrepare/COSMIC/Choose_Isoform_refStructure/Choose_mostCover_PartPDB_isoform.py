from collections import defaultdict 


###########################################################
#### COSMIC will map mutation to different isoforms #######
#### Choose the isoform that has most covered structure  ##
#### Not in full-covered ++ random select one if on-tier ##
#### ++ minimum coverage >= 40%  ##########################
#### Revised by B.W. Aug 2021 #############################
###########################################################

raw_ENSEMBL_seq_Withallisoform = '/home/bwang/data/cancer/Homo_sapiens.GRCh38.pep.all.fa'
full_covered_identity_csv_file = '/home/bwang/data/cancer/mapPDB/pdbstructure/Results/fully_covered_unique_PDB.csv'
part_covered_identity_csv_file = '/home/bwang/data/cancer/mapPDB/pdbstructure/Results/partial_covered_nonoverlapping_PDB.csv'


########################################
########################################
#### Load the Gene ==> list of isoforms 
########################################

#### Hash table for ENSEMBL Prot and Transcript
dic_geneID_prot = defaultdict(list)
dic_prot_geneID = {}
f_tmp = open(raw_ENSEMBL_seq_Withallisoform, 'r')
for line in f_tmp:
    line = line.strip()
    try:
        if line.startswith('>'):
            slides = line.split(' ')

            protID = slides[0].strip()
            protID = protID[1:]
            # ignore the seq version
            protID = protID.split('.')[0].strip()

            geneSymbol_nested = slides[7].strip()
            geneID = geneSymbol_nested.split(':')[1].strip()

            dic_geneID_prot [geneID].append(protID)
            dic_prot_geneID [protID] = geneID
    except:
        ## ignore small amount of novel gene without gene symbol 
        # print(line)
        pass
f_tmp.close()




############################################
############################################
#### For Partial Coverage PDB 
#############################################

######## KICK OFF gene with Full PDB covered
list_fullPDB_geneID = []
f = open(full_covered_identity_csv_file, 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    protID = slices[0].strip()
    protID = protID[1:]
    protID = protID.split('.')[0]
    try:
        geneID = dic_prot_geneID[protID]
        if geneID not in list_fullPDB_geneID:
            list_fullPDB_geneID.append(geneID)
    except:
        pass
f.close()

# print(list_fullPDB_geneID)


list_unique_geneID = []
list_unique_protIsoform = [] 
dic_protIsoform_all = defaultdict(list)
dic_protID_PDBchain = defaultdict(list)
f = open(part_covered_identity_csv_file, 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    protID = slices[0].strip()
    protID = protID[1:]
    protID = protID.split('.')[0]

    if protID not in list_unique_protIsoform:
        list_unique_protIsoform.append(protID)

    iden_seq = slices[2].strip()
    iden_pdb = slices[3].strip()
    sum_iden = float(iden_pdb) + float(iden_seq)

    pdbChain = slices[1].strip()
    pdbChain = pdbChain[1:7]

    dic_protIsoform_all [protID] .append(sum_iden)
    dic_protID_PDBchain [protID].append(pdbChain)

    try:
        geneID = dic_prot_geneID [protID]
        if geneID not in list_unique_geneID:
            list_unique_geneID.append(geneID)
    except:
        ## ignore small amount of novel gene without gene symbol 
        # print(line)
        pass
f.close()

# print(dic_protID_PDBchain, len(dic_protID_PDBchain))
# print(dic_protIsoform_all, len(dic_protIsoform_all))

dic_protIsoform_AllSum = {}
dic_geneID_AllSum = defaultdict(list)
for k,v in dic_protIsoform_all.items():
    all_sum = 0
    for i in range(0, len(v)):
        all_sum += v[i] 
    dic_protIsoform_AllSum [k] = all_sum 
    
    try:
        geneID = dic_prot_geneID[k] 
        dic_geneID_AllSum [geneID].append(all_sum)
    except:
        ## ignore small amount of novel gene without gene symbol 
        pass



list_considered_genewIsoform = []
## Only select one isoform if on-tier
list_covered_gene = []

for each_gene in list_unique_geneID:
    max_sumIden = max(dic_geneID_AllSum[each_gene])

    for each_isoform in dic_geneID_prot[each_gene]:
        try:
            if dic_protIsoform_AllSum[each_isoform] == max_sumIden:
                # print(dic_protIsoform_AllSum[each_isoform])
                # print(each_isoform)
                for each_pdb in dic_protID_PDBchain[each_isoform]:
                    out_entry = each_gene + ',' + each_isoform + ',' + each_pdb
                    if each_gene not in list_fullPDB_geneID:
                        if each_gene not in list_covered_gene:
                            list_covered_gene.append(each_gene)
                            # if dic_protIsoform_AllSum[each_isoform] >= 1.37:
                            print(out_entry)
        except:
            # print(each_isoform)
            pass