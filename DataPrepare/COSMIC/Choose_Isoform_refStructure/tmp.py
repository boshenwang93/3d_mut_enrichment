# from collections import defaultdict


# list_unique_geneID_wFull = []
# list_unique_protID_wFull = []

# list_unique_geneID_wPart = []
# list_unique_protID_wPart = []

# list_unique_geneID_alphafold = []
# list_unique_protID_alphafold = []

# dic_geneID_listPartprotID = defaultdict(list)


# tmpF = open('/home/bwang/data/cancer/fullPDB_gene_isoform.csv', 'r')
# for line in tmpF:
#     line = line.strip()
#     geneid = line.split(',')[0]
#     protID = line.split(',')[1]

#     if geneid not in list_unique_geneID_wFull:
#         list_unique_geneID_wFull.append(geneid)
#     if protID not in list_unique_protID_wFull:
#         list_unique_protID_wFull.append(protID)
# tmpF.close()


# tmpF = open('/home/bwang/data/cancer/partPDB_gene_isoform.csv', 'r')
# for line in tmpF:
#     line = line.strip()
#     geneid = line.split(',')[0]
#     protID = line.split(',')[1]

#     if protID not in dic_geneID_listPartprotID [geneid]:
#         dic_geneID_listPartprotID [geneid].append (protID)

#     if geneid not in list_unique_geneID_wPart:
#         list_unique_geneID_wPart.append(geneid)

#     if protID not in list_unique_protID_wPart:
#         list_unique_protID_wPart.append(protID)
# tmpF.close()



# tmpF = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/AlphaFold/alphafold_gene_list.csv', 'r')
# for line in tmpF:
#     line = line.strip()
#     geneid = line.split(',')[0]
#     protID = line.split(',')[1]

#     if geneid not in list_unique_geneID_alphafold:
#         list_unique_geneID_alphafold.append(geneid)
#     if protID not in list_unique_protID_alphafold:
#         list_unique_protID_alphafold.append(protID)
# tmpF.close()


# ### 3129    3129  match the results 
# # print(len(list_unique_geneID_wFull), len(list_unique_protID_wFull))


# ### 1730    1730  match the results
# print(len(list_unique_geneID_wPart), len(list_unique_protID_wPart))

# ### 14496 14496 match the results
# print(len(list_unique_geneID_alphafold), len(list_unique_protID_alphafold))


# for each_full_gene in list_unique_geneID_wPart:
#     if each_full_gene in list_unique_geneID_alphafold:
#         print(each_full_gene)

# # for k,v in dic_geneID_listPartprotID.items():
# #     if len(v) > 1 :
# #         print(k,v)

import os

list_ensemblprotID = []
list_gene = []
list_pdb = []
tmpF = open('pdbensembl.csv', 'r')
for line in tmpF:
    line = line.strip()
    geneid = line.split(',')[0]
    protID = line.split(',')[1]
    pdb_w_chain = line.split(',')[2]

    if geneid not in list_gene:
        list_gene.append(geneid)
    if protID not in list_ensemblprotID:
        list_ensemblprotID.append(protID)
        file_name = protID + '*'
        origin_path = os.path.join('/home/bwang/data/cancer/mapPDB/ensemblFasta', file_name)
        dest_path = '/home/bwang/data/cancer/calculate/MSA/experimental/query'
        cp_cmd = 'cp ' + origin_path + "\t" + dest_path
        os.system(cp_cmd)

    if pdb_w_chain not in list_pdb:
        list_pdb.append(pdb_w_chain)
tmpF.close()

print(len(list_ensemblprotID), len(list_gene), len(list_pdb))
