import os
from multiprocessing import Process, Pool


COSMIC_mutation_tsv_file = '/home/bwang/data/cancer/subsetMutation.tsv'

out_file_folder = '/home/bwang/data/cancer/mutation/pancancer/eachIsoform/'


list_considered_ensemblID = []
for root, subdirs, files in os.walk('/home/bwang/data/cancer/calculation/MSA_value'):
    for each_file in files:
        if each_file.endswith('prob'):
            ensembl_ID = each_file.split('.prob')[0]
            ensembl_ID_wo_version = ensembl_ID.split('.')[0]
            if ensembl_ID_wo_version not in list_considered_ensemblID:
                list_considered_ensemblID.append(ensembl_ID_wo_version)

print(list_considered_ensemblID)

def Retrieve_MutationInfo_byIsoform(prot_isoform_ID):

    out_file_path = out_file_folder + prot_isoform_ID + '.allMut'
    outF = open(out_file_path, 'w')

    f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
    for line in f:
        line = line.strip()

        slices_line = line.split("\t")

        mutation_type = slices_line[21].strip()

        try:
            protID_mut = slices_line[35].strip()
            ensembl_protID_wVersion = protID_mut.split(':')[0]
            ensembl_protID_woVersion = ensembl_protID_wVersion.split('.')[0]
            if ensembl_protID_woVersion == prot_isoform_ID:
                newline = line + "\n"
                outF.write(newline)
        except:
            pass

    f.close()
    outF.close()

    return 0


# Retrieve_MutationInfo_byIsoform(prot_isoform_ID = 'ENSP00000349198')



with Pool(processes= 18 ) as pool:
    pool.map(Retrieve_MutationInfo_byIsoform, list_considered_ensemblID)