import os

from multiprocessing import Process, Pool



infile_folder = '/home/bwang/data/cancer/mutation/pancancer/eachIsoform/'
outfile_folder = '/home/bwang/data/cancer/mutation/pancancer/count'


list_considered_isoform = []
for root, subdirs, files in os.walk(infile_folder):
    for each_file in files:
        ensembl_protID =each_file.split('.allMut')[0]
        list_considered_isoform.append(ensembl_protID)


def Retrieve_MutationInfo_byIsoform(prot_isoform_ID):

    infile_name = prot_isoform_ID + '.allMut'
    each_mutation_tsv_file = os.path.join(infile_folder, infile_name)

    list_unique_missenseMut = []
    list_redundant_missenseMut = []

    f = open(each_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
    for line in f:
        line = line.strip()
        slices_line = line.split("\t")

        # gene_name = slices_line[0].strip()
        # ensembl_transcriptID = slices_line[1].strip()
        # HGNC_ID = slices_line[3].strip()

        # tissue = slices_line[7].strip()
        # cancer_type = slices_line[12].strip()
        # sampleInfo = slices_line[4].strip()

        mutation_pattern_nested = slices_line[20].strip()
        mut_pattern = mutation_pattern_nested.split('.')[1]
        mut_seqIndex = mut_pattern[1:-1]
        wild_AA = mut_pattern[0]
        mut_AA = mut_pattern[-1]

        rearranged_mutation = prot_isoform_ID + ',' + mut_seqIndex + ',' +\
                              wild_AA + ',' + mut_AA

        mutation_type = slices_line[21].strip()

        # p.M217R
        # Substitution - Missense
        if mutation_type == 'Substitution - Missense':
            list_redundant_missenseMut.append(rearranged_mutation)
            if rearranged_mutation not in list_unique_missenseMut:
                list_unique_missenseMut.append(rearranged_mutation)
    f.close()

    #### Counting recurrence
    dic_missenseMut_count = {}
    for each_missenseMut in list_unique_missenseMut:
        i = 0
        for sub_mut in list_redundant_missenseMut:
            if sub_mut == each_missenseMut:
                i += 1
        dic_missenseMut_count [each_missenseMut] = i

    #### write to output file
    outfile_name = prot_isoform_ID + '.count'
    out_file_path = os.path.join(outfile_folder, outfile_name)
    outF = open(out_file_path, 'w')
    for k,v in dic_missenseMut_count.items():
        out_line = k + ',' + str(v) + "\n"
        outF.write(out_line)
    outF.close()

    return 0




with Pool(processes= 18 ) as pool:
    pool.map(Retrieve_MutationInfo_byIsoform, list_considered_isoform)