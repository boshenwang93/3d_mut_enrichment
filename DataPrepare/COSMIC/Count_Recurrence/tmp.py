

count_lessthree = 0
f = open('pancancer_mut_count.csv', 'r')
for line in f:
    line = line.strip()
    recurrence = line.split(',')[-1]
    recurrence = int (recurrence)
    if recurrence < 3:
        count_lessthree += 1
f.close()

print(count_lessthree)