import os 


def CountSeqLength (single_fasta_file):

    seq_content = ''
    f = open(single_fasta_file, 'r')
    for line in f:
        line = line.strip()
        if line.startswith('>') == 0 :
            seq_content += line
    f.close()

    seq_length = len(seq_content)

    return seq_length

for root, subdir_list, file_list in os.walk('/mnt/data/project/cancer/CosmicV92/single_ensembl_prot/'):
    for each_file in file_list:
        file_abs_path = root + each_file 
        seq_length = CountSeqLength(file_abs_path)
        ensemblID = each_file.split(".")[0].strip()
        out_entry = ensemblID + ',' + str(seq_length)
        print(out_entry)