import os 


def CountSeqLength (single_fasta_file):

    total_mut = 0 
    f = open(single_fasta_file, 'r')
    for line in f:
        line = line.strip()
        current_mut_count = line.strip(',')[-1].strip()
        current_mut_count = int(current_mut_count)
        total_mut += current_mut_count 
    f.close()

    return total_mut

for root, subdir_list, file_list in os.walk('/mnt/data/project/cancer/CosmicV92/Mutation/Count/insertion/'):
    for each_file in file_list:
        file_abs_path = root + each_file 
        sum_mut = CountSeqLength(file_abs_path)
        ensemblID = each_file.split(".count")[0].strip()
        out_entry = ensemblID + ',' + str(sum_mut) + ',insertion'
        print(out_entry)