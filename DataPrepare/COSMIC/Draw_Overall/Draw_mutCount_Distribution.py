import os
import argparse
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter
from scipy.optimize import leastsq

###########################################################
#### Plot the histogram ###################################
###########################################################

list_abs_count = []
list_avg_count = [] 

f = open('/mnt/data/project/cancer/CosmicV92/mut_Count_ByIsoform.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    abs_count = slices[-2]
    avg_count = slices[-1]
    abs_count = int(abs_count)
    avg_count = float(avg_count)

    geneID = slices[1]
    protID = slices[2]
    identifier = geneID + '_' + protID

    list_abs_count.append(abs_count)
    list_avg_count.append(avg_count)
f.close()

#### Draw AVG Mutation Count
num_bins = 15
bins = np.arange(0.0, 2, 0.01)

plt.hist(list_avg_count, bins= bins, 
         facecolor='blue', alpha=0.5)
plt.xticks(fontsize=15)
plt.title('Mutation Rate Per Protein Isoform')


# add a 'best fit' line
# y = mlab.normpdf( bins, mu, sigma)
# l = plt.plot(bins, y, 'r--', linewidth=2)


plt.show()



# bins = np.arange(0, 3000, 100)
# plt.hist(list_abs_count, bins= bins, 
#          facecolor='blue')
# plt.xticks(fontsize=15)
# plt.show()