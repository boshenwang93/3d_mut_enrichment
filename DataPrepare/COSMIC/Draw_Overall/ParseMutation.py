

#### Mutation Types 
# ['Mutation Description', 
#  ''
# 'Unknown', 
# 'Substitution - Missense', 
# 'Substitution - coding silent', 
# 'Deletion - Frameshift', 
# 'Substitution - Nonsense', 
#  'Insertion - Frameshift', 
#  'Deletion - In frame', 
#  'Nonstop extension', 
#  'Complex - frameshift', 
#  'Insertion - In frame', 
#  'Frameshift', 
#  'Complex - deletion inframe', 
#  'Whole gene deletion', 
#  'Complex - compound substitution',
#   'Complex - insertion inframe']



# list_HGNC_FullCoverage = [] 
# f_map = open('UP_HGNC_FullCoverage.tab','r')
# for line in f_map:
#     line = line.strip()
#     hgnc_id = line.split(":")[-1].strip()
#     list_HGNC_FullCoverage.append(hgnc_id)
# f_map.close()

def ParseMutation(COSMIC_mutation_tsv_file):

    list_unique_type = []
    list_unique_transcript_ID = []
    list_cancer_subtype = []
    list_tissue = [] 
    list_sampleID = []

    count_transcript_ID = 0 
    count_total_mutation = 0
    count_sub_missense = 0 
    count_sub_silent = 0
    count_sub_nonsense = 0 
    count_frameshift = 0
    count_fs_deletion = 0
    count_fs_insertion = 0
    count_inframe_deletion = 0
    count_inframe_insertion = 0
    count_nonstop_extension = 0 
    count_complex_frameshift = 0 
    count_complex_deletionInframe = 0 
    count_complex_insertionInframe = 0 
    count_complex_compoundsub = 0 
    count_genedeletion = 0 

    count_TBD = 0 

    f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
    for line in f:
        line = line.strip()
        slices_line = line.split("\t")

        gene_name = slices_line[0].strip()
        ensembl_transcriptID = slices_line[1].strip()
        HGNC_ID = slices_line[3].strip()
        tissue = slices_line[7].strip()
        cancer_type = slices_line[12].strip()
        
        sampleInfo = slices_line[4].strip()

        mutation_pattern = slices_line[20].strip()
        mutation_type = slices_line[21].strip()
        
        if sampleInfo not in list_sampleID:
            list_sampleID.append(sampleInfo)
        # if tissue not in list_tissue:
        #     list_tissue.append(tissue)
        
        # if cancer_type not in list_cancer_subtype: 
        #     list_cancer_subtype.append(cancer_type)


        # if ensembl_transcriptID not in list_unique_transcript_ID:
        #     list_unique_transcript_ID.append(ensembl_transcriptID)
        #     count_transcript_ID += 1 
        #     # print(count_transcript_ID)
        #     # print(ensembl_transcriptID)

        # if mutation_type not in list_unique_type:
        #     list_unique_type.append(mutation_type)
        
        count_total_mutation += 1
        # print(count_total_mutation)
        
        out_entry = gene_name + ',' + HGNC_ID + ',' + \
                    mutation_type + ',' + mutation_pattern + ',' +\
                    tissue + ',' + cancer_type + ',' +\
                    ensembl_transcriptID + ',' + sampleInfo
        # print(out_entry)

        # #############  For point mutation 
        # #### missense will cause amino acid type changes 
        # #### silent: No amino acid change at protein level
        # #### nonsense: Protein would stop 
        # if mutation_type =='Substitution - Missense':
        #     count_sub_missense += 1
        #     # print(out_entry)
        # # elif mutation_type == 'Substitution - coding silent':
        # #     count_sub_silent += 1
        # elif mutation_type == 'Substitution - Nonsense':
        #     count_sub_nonsense += 1
            # print(out_entry) 


        # ########## For frameshift condition
        # elif mutation_type == 'Frameshift':
        #     count_frameshift += 1
        # elif mutation_type == 'Deletion - Frameshift':
        #     count_fs_deletion += 1
        # elif mutation_type == 'Insertion - Frameshift':
        #     count_fs_insertion += 1

        # elif mutation_type == 'Deletion - In frame':
        #     count_inframe_deletion += 1
        # elif mutation_type == 'Insertion - In frame':
        #     count_inframe_insertion += 1
            # print(out_entry)

        
        # ########## For complex condition 
        # elif mutation_type == 'Nonstop extension':
        #     count_nonstop_extension += 1
        # elif mutation_type == 'Complex - frameshift':
        #     count_complex_frameshift += 1        
        # elif mutation_type == 'Complex - deletion inframe':
        #     count_complex_deletionInframe += 1        
        # elif mutation_type == 'Complex - compound substitution':
        #     count_complex_compoundsub += 1        
        # elif mutation_type == 'Complex - insertion inframe':
        #     count_complex_insertionInframe += 1
        
        # ######### Whole gene loss
        # elif mutation_type == 'Whole gene deletion':
        #     count_genedeletion += 1 

        # ## exception condition
        # elif mutation_type in ['', 'Unknown']:
        #     count_TBD += 1 

            # if HGNC_ID in list_HGNC_FullCoverage:
                # print(out_entry)

    f.close()

    for e in list_sampleID:
        print(e)

    # print(list_unique_type)
    
    # print(  count_total_mutation )

    # print(  count_sub_missense,
    #         count_sub_silent ,
    #         count_sub_nonsense,
    #         count_frameshift ,
    #         count_fs_deletion ,
    #         count_fs_insertion ,
    #         count_inframe_deletion ,
    #         count_inframe_insertion ,
    #         count_nonstop_extension,
    #         count_complex_frameshift,
    #         count_complex_deletionInframe,
    #         count_complex_insertionInframe,
    #         count_complex_compoundsub,
    #         count_genedeletion)
            
    # print(count_TBD)
    
    # out_F = open('ensembl_transcriptID', 'w')
    # for each_ensembl_TranscriptID in list_unique_transcript_ID:
    #     out_line = each_ensembl_TranscriptID + "\n"
    #     out_F.write(out_line)
    # out_F.close()

    return 0 


ParseMutation(COSMIC_mutation_tsv_file =\
              '/mnt/data/database/cancer/COSMIC/v92/CosmicMutantExport.tsv')