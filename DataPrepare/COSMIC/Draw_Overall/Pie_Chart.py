import matplotlib.pyplot as plt
import numpy as np

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'Missense', 'Silent', 'Nonsense', \
         'Frameshift', \
         'Del_FS', \
         'Ins_FS', \
         'Del_InF', 'Ins_InF', \
         'Extension', \
         'ComFS', \
         'CompDel', \
         'CompSub',\
         'CompIns',\
         'Gene deletion'


sizes = [ 12371234,3964490,912230,
         4363,432424,230146,118706,43320,15799,
          5117,677,198,30,2851]

# explode = ( 0, 0, 0.1,  0, 0, 0,  0, 0, 0, 0, 0,  0, 0, 0)  # only "explode" the 1st slice
# fig1, ax1 = plt.subplots()
# ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
#         shadow=True, startangle=90)
# ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.



fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))

def func(pct, allvals):
    absolute = int(pct/100.*np.sum(allvals))
    return "{:.1f}%\n".format(pct)


wedges, texts, autotexts = ax.pie(sizes, autopct=lambda pct: func(pct, sizes),
                                  textprops=dict(color="w"))

ax.legend(wedges, labels,
          title="Mutation Types",
          loc="center left",
          bbox_to_anchor=(1,0, 1, 1))

# plt.setp(autotexts, size=15, weight="bold")
plt.setp(autotexts, size=18)


ax.set_title("Mutation Types in COSMIC V92", fontsize=26, weight='bold')


plt.show()
