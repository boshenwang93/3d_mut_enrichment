
from multiprocessing import Process, Pool


combined_mutation_file_path =\
            '/mnt/data/project/cancer/CosmicV92/Mutation/insertionInframe.csv'
out_folder =\
            '/mnt/data/project/cancer/CosmicV92/Mutation/ByIsoform/insertion/'


list_unique_isoform = [] 

f = open(combined_mutation_file_path, 'r')
for line in f:
    line = line.strip()

    geneNested = line.split(',')[0].strip().upper()
    
    geneID = ''
    try:
        geneID += geneNested.split("_")[0].strip()
    except:
        geneID += geneNested
    
    transcriptID_wVersion = line.split(',')[-2].strip().upper()
    transcriptID = ''
    try:
        transcriptID = transcriptID_wVersion.split('.')[0].strip().upper()
    except:
        transcriptID = transcriptID_wVersion
    
    isoformID = geneID + "_" + transcriptID
    if isoformID not in list_unique_isoform:
        list_unique_isoform.append(isoformID)
f.close()


def GenerateMutationByEachGene(each_isoform):

    new_out_file_path =  out_folder +  each_isoform + ".mutation"

    outF = open(new_out_file_path, 'w')

    f = open(combined_mutation_file_path, 'r')
    for line in f:
        line = line.strip()
        geneNested = line.split(',')[0].strip().upper()
        geneID = ''
        try:
            geneID += geneNested.split("_")[0].strip()
        except:
            geneID += geneNested
        transcriptID_wVersion = line.split(',')[-2].strip().upper()
        transcriptID = ''
        try:
            transcriptID = transcriptID_wVersion.split('.')[0].strip().upper()
        except:
            transcriptID = transcriptID_wVersion
        
        isoformID = geneID + "_" + transcriptID
    
        if isoformID == each_isoform:
            newline = line + "\n"
            outF.write(newline)
    f.close()

    outF.close()

    return 0 



with Pool(processes= 30 ) as pool:
    pool.map(GenerateMutationByEachGene, list_unique_isoform)