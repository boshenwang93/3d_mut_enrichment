import os
from multiprocessing import Process, Pool


COSMIC_mutation_tsv_file = '/home/bwang/cancer/subsetMutation.tsv'

out_file_folder = '/home/bwang/data/cancer/subtype_specific/raw/'


list_considered_subtypes = ['carcinoma', 'malignant_melanoma', 'sarcoma', 'lymphoid_neoplasm', 'glioma', 'haematopoietic_neoplasm', 'carcinoid-endocrine_tumour', 'choriocarcinoma', 'rhabdomyosarcoma', 'adenoma', 'Ewings_sarcoma-peripheral_primitive_neuroectodermal_tumour', 'neuroblastoma']

list_consdiered_tissues = ['endometrium', 'large_intestine', 'stomach', 'upper_aerodigestive_tract', 'urinary_tract', 'lung', 'biliary_tract', 'skin', 'breast', 'liver', 'ovary', 'kidney', 'soft_tissue', 'haematopoietic_and_lymphoid_tissue', 'cervix', 'thyroid', 'oesophagus', 'prostate', 'central_nervous_system', 'pancreas', 'small_intestine', 'placenta', 'bone', 'salivary_gland', 'autonomic_ganglia', 'adrenal_gland']


def Retrieve_MutationInfo_byTissue(tissue_ID):
    
    out_file_name =  tissue_ID + '.allMut'
    out_file_path = os.path.join(out_file_folder, out_file_name)
    outF = open(out_file_path, 'w')

    f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
    for line in f:
        line = line.strip()
        slices_line = line.split("\t")

        primary_site_tissue = slices_line[7].strip()
        primary_histology_subtype = slices_line[11].strip()

        mutation_type = slices_line[21].strip()

        if primary_histology_subtype == tissue_ID:
            newline = line + "\n"
            outF.write(line)
    f.close()
    outF.close()

    return 0


with Pool(processes= 10 ) as pool:
    pool.map(Retrieve_MutationInfo_byTissue, list_considered_subtypes)