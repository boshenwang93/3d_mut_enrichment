import os
from multiprocessing import Process, Pool
import argparse


#### Setting the args for customized option
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                     help='input folder contains each protID mutation tsv file')
parser.add_argument('-o', '--outdir', type=str,
                     help='output parent folder for each prot Mutation file under each Tissue/Subtype')

args = parser.parse_args()

input_dir = args.indir
output_dir = args.outdir



list_considered_subtypes = ['carcinoma', 'malignant_melanoma', 'sarcoma', 'lymphoid_neoplasm', 'glioma', 'haematopoietic_neoplasm', 'carcinoid-endocrine_tumour', 'choriocarcinoma', 'rhabdomyosarcoma', 'adenoma', 'Ewings_sarcoma-peripheral_primitive_neuroectodermal_tumour', 'neuroblastoma']

list_consdiered_tissues = ['endometrium', 'large_intestine', 'stomach', 'upper_aerodigestive_tract', 'urinary_tract', 'lung', 'biliary_tract', 'skin', 'breast', 'liver', 'ovary', 'kidney', 'soft_tissue', 'haematopoietic_and_lymphoid_tissue', 'cervix', 'thyroid', 'oesophagus', 'prostate', 'central_nervous_system', 'pancreas', 'small_intestine', 'placenta', 'bone', 'salivary_gland', 'autonomic_ganglia', 'adrenal_gland']


for each_tissue in list_considered_subtypes:
    new_folder = os.path.join(output_dir, each_tissue)
    mkdir_cmd =  'mkdir' + "\t" + new_folder
    os.system(mkdir_cmd)


def Retrieve_MutationInfo_byIsoform(prot_isoform_ID):
    mutation_tsv_file = os.path.join(input_dir, prot_isoform_ID)

    for each_tissue in list_considered_subtypes:

        out_file_dir = os.path.join(output_dir, each_tissue)
        out_file_path = os.path.join(out_file_dir, prot_isoform_ID)
        outF = open(out_file_path, 'w')

        f = open(mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
        for line in f:
            line = line.strip()

            slices_line = line.split("\t")
            mutation_type = slices_line[21].strip()
            primary_site_tissue = slices_line[7].strip()
            primary_histology_subtype = slices_line[11].strip()

            if primary_site_tissue  == each_tissue:
                newline = line + "\n"
                outF.write(newline)
        f.close()
        outF.close()

    return 0



list_considered_ensemblID = []
for root, subdirs, files in os.walk(input_dir):
    for each_file in files:
        if each_file.startswith('ENSP'):
            list_considered_ensemblID.append(each_file)



with Pool(processes= 18 ) as pool:
    pool.map(Retrieve_MutationInfo_byIsoform, list_considered_ensemblID)