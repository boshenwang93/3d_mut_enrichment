import os
import argparse
from multiprocessing import Process, Pool



#### Setting the args for customized option
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                     help='input folder contains each protID mutation tsv file')
parser.add_argument('-o', '--outdir', type=str,
                     help='output parent folder for COUNTING MUTATION RECURRENCE under each Tissue/Subtype')

args = parser.parse_args()

input_dir = args.indir
output_dir = args.outdir


list_considered_subtypes = ['carcinoma', 'malignant_melanoma', 'sarcoma', 'lymphoid_neoplasm', 'glioma', 'haematopoietic_neoplasm', 'carcinoid-endocrine_tumour', 'choriocarcinoma', 'rhabdomyosarcoma', 'adenoma', 'Ewings_sarcoma-peripheral_primitive_neuroectodermal_tumour', 'neuroblastoma']

list_consdiered_tissues = ['endometrium', 'large_intestine', 'stomach', 'upper_aerodigestive_tract', 'urinary_tract', 'lung', 'biliary_tract', 'skin', 'breast', 'liver', 'ovary', 'kidney', 'soft_tissue', 'haematopoietic_and_lymphoid_tissue', 'cervix', 'thyroid', 'oesophagus', 'prostate', 'central_nervous_system', 'pancreas', 'small_intestine', 'placenta', 'bone', 'salivary_gland', 'autonomic_ganglia', 'adrenal_gland']



for each_tissue in list_considered_subtypes:
    infile_folder = os.path.join(input_dir, each_tissue)

    outfile_name = each_tissue + '.csv'
    outfile_count_csv = os.path.join(output_dir, outfile_name)
    outF = open(outfile_count_csv, 'w')

    for root, subdirs, files in os.walk(infile_folder):
        for each_protID_tsv in files:
            prot_isoform_ID = each_protID_tsv.split('.')[0]
            each_mutation_tsv_file = os.path.join(root, each_protID_tsv)

            list_unique_missenseMut = []
            list_redundant_missenseMut = []

            f = open(each_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
            for line in f:
                line = line.strip()
                slices_line = line.split("\t")

                mutation_pattern_nested = slices_line[20].strip()
                mut_pattern = mutation_pattern_nested.split('.')[1]
                mut_seqIndex = mut_pattern[1:-1]
                wild_AA = mut_pattern[0]
                mut_AA = mut_pattern[-1]

                rearranged_mutation = prot_isoform_ID + ',' + mut_seqIndex + ',' +\
                                        wild_AA + ',' + mut_AA

                mutation_type = slices_line[21].strip()

                # p.M217R
                # Substitution - Missense
                if mutation_type == 'Substitution - Missense':
                    list_redundant_missenseMut.append(rearranged_mutation)
                    if rearranged_mutation not in list_unique_missenseMut:
                        list_unique_missenseMut.append(rearranged_mutation)
            f.close()

            #### Counting recurrence
            dic_missenseMut_count = {}
            for each_missenseMut in list_unique_missenseMut:
                i = 0
                for sub_mut in list_redundant_missenseMut:
                    if sub_mut == each_missenseMut:
                        i += 1
                dic_missenseMut_count [each_missenseMut] = i

            #### write to output file
            for k,v in dic_missenseMut_count.items():
                out_line = k + ',' + str(v) + "\n"
                outF.write(out_line)
    outF.close()
