import os
from collections import defaultdict 

COSMIC_mutation_tsv_file = '/home/bwang/cancer/subsetMutation.tsv'

# list_consider_protID = []
# f = open('considered_ensembl_protID', 'r')
# for line in f:
#     line = line.strip()
#     list_consider_protID.append(line)
# f.close()


list_subtypes = []
list_tissues = []
dict_tissue_protIDs = defaultdict(list)
dict_subtypes_protIDs = defaultdict(list)

f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
for line in f:
    line = line.strip()

    slices_line = line.split("\t")
    
    primary_site_tissue = slices_line[7].strip()
    primary_histology_subtype = slices_line[11].strip()
    mutation_type = slices_line[21].strip()
 
    # if primary_histology_subtype not in list_subtypes:
    #     list_subtypes.append(primary_histology_subtype)
       
    # if primary_site_tissue not in list_tissues:
    #     list_tissues.append(primary_site_tissue)
    
    try:
        protID_mut = slices_line[35].strip()
        ensembl_protID_wVersion = protID_mut.split(':')[0]
        ensembl_protID_woVersion = ensembl_protID_wVersion.split('.')[0]
        if ensembl_protID_woVersion not in dict_subtypes_protIDs [primary_histology_subtype]:
            dict_subtypes_protIDs [primary_histology_subtype].append(ensembl_protID_woVersion)
        # if ensembl_protID_woVersion not in dict_tissue_protIDs [primary_site_tissue]:
        #     dict_tissue_protIDs [primary_site_tissue] .append(ensembl_protID_woVersion)

        # if ensembl_protID_woVersion in list_consider_protID:
        #     print(line)
    except:
        pass

f.close()

list_good_subclassification = []
for k,v in dict_subtypes_protIDs.items():
    if len(v) > 4000:
        list_good_subclassification.append(k)

print(list_good_subclassification)


