
import numpy as np
import os 

list_included_mut_type = ['Substitution - Missense', 
                          'Substitution - Nonsense', 
                          'Deletion - In frame', 
                          'Insertion - In frame']

list_all_4types_protID = []
list_unique_protID = []

COSMIC_mutation_tsv_file = '/home/bwang/data/cancer/CosmicGenomeScreensMutantExport.tsv'
f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
for line in f:
    line = line.strip()
    
    slices_line = line.split("\t")

    gene_name = slices_line[0].strip()

    ensembl_transcriptID = slices_line[1].strip()
    ensembl_transcriptID = ensembl_transcriptID.split('.')[0].strip()

    HGNC_ID = slices_line[3].strip()
    tissue = slices_line[7].strip()
    cancer_type = slices_line[12].strip()
    sampleInfo = slices_line[4].strip()
    mutation_pattern = slices_line[20].strip()
    mutation_type = slices_line[21].strip()
    
    if mutation_type in list_included_mut_type:
        try:
            protID_mut = slices_line[35].strip()
            ensembl_protID_wVersion = protID_mut.split(':')[0]
            list_all_4types_protID.append(ensembl_protID_wVersion)

        except:
            pass
f.close()

list_unique_protID = np.unique(list_all_4types_protID)


for e in list_unique_protID:
    fasta_file_name = e[0:16] + "*"
    old_path = os.path.join('/home/bwang/data/cancer/mapPDB/ensemblFasta', fasta_file_name)
    new_path = '/home/bwang/data/cancer/mapPDB/selectEnsembl'

    cp_cmd = 'cp' + "\t" + old_path + "\t" + new_path
    os.system(cp_cmd)