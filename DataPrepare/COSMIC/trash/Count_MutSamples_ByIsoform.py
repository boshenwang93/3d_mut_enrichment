import os 
from multiprocessing import Process, Pool

out_file_folder = '/mnt/data/project/cancer/Feature/Mut/RawRedun_Mutation/PartPDB/count/'
raw_mutation_folder = '/mnt/data/project/cancer/Feature/Mut/RawRedun_Mutation/PartPDB/missense/'

def CountResidueMutFrequency(individual_gene_mut_file):

    list_MutationPattern = []

    f = open(individual_gene_mut_file, 'r')
    for line in f:
        line = line.strip()
        genenested = line.split(',')[0].strip().upper()
        try:
            geneID = genenested.split('_')[0].strip()
        except:
            geneID = genenested

        isoformID = line.split(',')[-2].strip().upper()
        isoformID = isoformID.split('.')[0].strip()

        mut_pattern = line.split(',')[3].strip().upper()

        if mut_pattern not in list_MutationPattern:
            list_MutationPattern.append(mut_pattern)
    f.close()

    #### Count recurrence for each mutation pattern 
    #### output to assigned path 
    out_file_path =  out_file_folder + geneID + '_' + isoformID + '.count'
    # out_F = open(out_file_path, 'w')

    for each_mut_pattern in list_MutationPattern:
        count_freq = 0

        f = open(individual_gene_mut_file, 'r')
        for line in f:
            line = line.strip()
            mut_pattern = line.split(',')[3].strip().upper()
            if mut_pattern == each_mut_pattern:
                count_freq += 1 
        f.close()
        
        out_line = geneID + ',' + isoformID  + ',' +\
                   each_mut_pattern + ',' + str(count_freq) + "\n"
        print(out_line, end='')
    #     out_F.write(out_line)

    # out_F.close()

    return 0

list_files = [] 
for root, subdirs, file_list in os.walk(raw_mutation_folder):
    for each_file in file_list:
        if each_file.endswith('mutation'):
            file_abs_path = root + each_file
            list_files.append(file_abs_path)


with Pool(processes= 30 ) as pool:
    pool.map(CountResidueMutFrequency, list_files)