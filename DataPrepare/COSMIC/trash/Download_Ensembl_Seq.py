import requests, sys


    
server = "https://rest.ensembl.org"
ext = "/sequence/id/ENSP00000415089?"

r = requests.get(server+ext, headers={ "Content-Type" : "application/json"})
    
if not r.ok:
    r.raise_for_status()
    sys.exit()
    
decoded = r.json()
print(repr(decoded))
     

