
list_unique_gene_name = [] 

f = open('MissenseVariant_FullCov_Prot.csv', 'r')
for line in f:
    line = line.strip()
    gene_name = line.split(',')[0].strip()
    gene_name = gene_name.upper()
    if gene_name not in list_unique_gene_name:
        list_unique_gene_name.append(gene_name)
f.close()


dic_gene_UP = {}
i = 0 
f_map = open('/home/bwang/project/cancer/DataPrepare/UP_GENE.mapping', 'r')
for line in f_map:
    line = line.strip()
    uniprot = line.split("\t")[0].strip()
    geneName =  line.split("\t")[1].strip()

    if geneName in list_unique_gene_name:
        dic_gene_UP [geneName] = uniprot
        i +=1
f_map.close()


f = open('MissenseVariant_FullCov_Prot.csv', 'r')
for line in f:
    line = line.strip()
    gene_name = line.split(',')[0].strip()
    gene_name = gene_name.upper()
    
    try:
        uniprot = dic_gene_UP [gene_name]
        new_line = uniprot + ',' + line 
        print(new_line)
    except:
        pass
f.close()