
import csv 

count_tier1 = 0 
count_tier2 = 0
with open('/data/database/Cancer/COSMIC/cancer_gene_census.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if row[4] == '2':
            count_tier2 += 1
        elif row[4] == '1':
            count_tier1 += 1
        
        entrez_geneID = row[2]
        print(entrez_geneID)

