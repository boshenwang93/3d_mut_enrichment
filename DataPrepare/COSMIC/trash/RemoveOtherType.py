
list_protID_wPDB = [] 
f = open('/mnt/data/project/cancer/searchPDB/Part_Coverage_random.csv', 'r')
for line in f:
    line = line.strip()
    protID = line.split(',')[0].strip()
    protID = protID[1:]
    if protID not in list_protID_wPDB:
        list_protID_wPDB.append(protID)
f.close()

f = open('/mnt/data/project/cancer/searchPDB/Full_Coverage_randomOne.csv', 'r')
for line in f:
    line = line.strip()
    protID = line.split(',')[0].strip()
    protID = protID[1:]
    if protID not in list_protID_wPDB:
        list_protID_wPDB.append(protID)
f.close()


#### Hash table for ENSEMBL Prot and Transcript
dic_prot_geneID = {} 
f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
for line in f_tmp:
    line = line.strip()
    if line.startswith('>'):
        slides = line.split(' ')

        protID = slides[0].strip()
        protID = protID[1:]
        protID = protID.split('.')[0].strip()

        geneSymbol_nested = slides[7].strip()
        geneID = geneSymbol_nested.split(':')[1].strip()

        dic_prot_geneID [protID] = geneID
f_tmp.close()


list_geneID_wPDB = []
for e in list_protID_wPDB:
    geneID = dic_prot_geneID [e]
    if geneID not in list_geneID_wPDB:
        list_geneID_wPDB.append(geneID)


print(len(list_geneID_wPDB))
print(len(list_protID_wPDB))

# COSMIC_mutation_tsv_file =\
#               '/mnt/data/project/cancer/CosmicV92/Raw_FourTypes.tsv'


# f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
# for line in f:
#     line = line.strip()
#     slices_line = line.split("\t")

#     gene_name = slices_line[0].strip()
#     HGNC_ID = slices_line[3].strip()

    
#     if gene_name in list_geneID_wPDB:
#         print(line)
# f.close()