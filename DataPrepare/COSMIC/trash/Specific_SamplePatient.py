from multiprocessing import Process, Pool
import os 


COSMIC_mutation_tsv_file = '/mnt/data/project/cancer/CosmicV92/Raw_FourTypes_wPDB.tsv'
out_folder = '/mnt/data/project/cancer/Specific/sampleSpecific/'


list_included_mut_type = ['Substitution - Missense', 
                          'Substitution - Nonsense', 
                          'Deletion - In frame', 
                          'Insertion - In frame']


def ParseMutation(assigned_specific_sample):
    
    out_single_mut_file_path = out_folder + assigned_specific_sample + '.mutation'
    outF = open(out_single_mut_file_path, 'w')

    f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
    for line in f:
        line = line.strip()
        slices_line = line.split("\t")

        gene_name = slices_line[0].strip()
        ensembl_transcriptID = slices_line[1].strip()
        HGNC_ID = slices_line[3].strip()
        tissue = slices_line[7].strip()
        cancer_type = slices_line[12].strip()
        sampleInfo = slices_line[4].strip()
        mutation_pattern = slices_line[20].strip()
        mutation_type = slices_line[21].strip()
        
        out_entry = gene_name + ',' + HGNC_ID + ',' + \
                    mutation_type + ',' + mutation_pattern + ',' +\
                    tissue + ',' + cancer_type + ',' +\
                    ensembl_transcriptID + ',' + sampleInfo + "\n"
        
        if assigned_specific_sample == sampleInfo:
            if mutation_type in list_included_mut_type:
                outF.write(out_entry)
    f.close()
    outF.close()

    return 0

list_unique_tissue = []
f = open('/mnt/data/project/cancer/CosmicV92/Sample_Patient.table', 'r')
for line in f:
    line = line.strip()
    list_unique_tissue.append(line)
f.close()

with Pool(processes= 15 ) as pool:
    pool.map(ParseMutation, list_unique_tissue)