import os.path
from os import path

#### Hash table for ENSEMBL Prot and Transcript
dic_prot_trans = {} 
dic_trans_prot = {}
f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
for line in f_tmp:
    line = line.strip()
    if line.startswith('>'):
        slides = line.split(' ')
        protID = slides[0].strip()
        protID = protID[1:]
        protID = protID.split('.')[0].strip()

        trans_nested = slides[4].strip()
        transID = trans_nested.split(':')[1].strip()
        transID = transID.split('.')[0].strip()

        dic_prot_trans [protID] = transID
        dic_trans_prot [transID] = protID
f_tmp.close()



dest_folder = '/mnt/data/project/cancer/Feature/Mut/RawRedun_Mutation/PartPDB/missense/'
## Obtain PDB Gene BestIsoform
f = open('/home/bwang/project/3d_mut_enrichment/PartPDB_Gene_BestIsoform', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    gene = slices[0]
    prot = slices[1]
    prot = prot[1:]
    
    transcriptID = dic_prot_trans [prot]

    gene_trans = gene + '_' + transcriptID
    # gene_trans = gene_trans.upper()
    mut_file_name = gene_trans + '.mutation'
    mut_file_path = '/mnt/data/project/cancer/CosmicV92/Mutation/ByIsoform/missense/' +\
                    mut_file_name
    
    

    if path.exists(mut_file_path) == 1:
        cp_cmd = 'cp ' + mut_file_path + "\t" + dest_folder 
        os.system(cp_cmd)
    else:
        pass
        # print(mut_file_path)

f.close()