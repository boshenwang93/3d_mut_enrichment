
# dic_ensembl_mis = {} 
# dic_ensembl_nonsense = {}
# dic_ensembl_deletion = {}
# dic_ensembl_insertion = {} 

# dic_ensembl_geneID = {} 

# f_tmp = open('/mnt/data/project/cancer/CosmicV92/ensembl_missense_Count.csv', 'r')
# for line in f_tmp:
#     line = line.strip()
#     seqID = line.split(',')[0].strip()
#     ensembl = seqID.split('_')[-1].strip()
#     geneID = seqID.split("_")[0].strip()

#     count = line.split(',')[1].strip()
#     count = int(count)
#     dic_ensembl_mis [ensembl] = count 
#     dic_ensembl_geneID [ensembl] = geneID
# f_tmp.close()

# f_tmp = open('/mnt/data/project/cancer/CosmicV92/ensembl_nonsense_Count.csv', 'r')
# for line in f_tmp:
#     line = line.strip()
#     seqID = line.split(',')[0].strip()
#     ensembl = seqID.split('_')[-1].strip()
#     geneID = seqID.split("_")[0].strip()

#     count = line.split(',')[1].strip()
#     count = int(count)
#     dic_ensembl_nonsense [ensembl] = count 
# f_tmp.close()

# f_tmp = open('/mnt/data/project/cancer/CosmicV92/ensembl_deletion_Count.csv', 'r')
# for line in f_tmp:
#     line = line.strip()
#     seqID = line.split(',')[0].strip()
#     ensembl = seqID.split('_')[-1].strip()
#     geneID = seqID.split("_")[0].strip()

#     count = line.split(',')[1].strip()
#     count = int(count)
#     dic_ensembl_deletion [ensembl] = count 
# f_tmp.close()

# f_tmp = open('/mnt/data/project/cancer/CosmicV92/ensembl_insertion_Count.csv', 'r')
# for line in f_tmp:
#     line = line.strip()
#     seqID = line.split(',')[0].strip()
#     ensembl = seqID.split('_')[-1].strip()
#     geneID = seqID.split("_")[0].strip()

#     count = line.split(',')[1].strip()
#     count = int(count)
#     dic_ensembl_insertion [ensembl] = count 
# f_tmp.close()

# ####################################################
# dic_ensembl_seqLength = {} 
# f_tmp = open('/mnt/data/project/cancer/CosmicV92/ensembl_Seq_Length.csv', 'r')
# for line in f_tmp:
#     line = line.strip()
#     seqID = line.split(',')[0].strip()
#     count = line.split(',')[1].strip()
#     count = int(count)
#     dic_ensembl_seqLength [seqID] = count 
# f_tmp.close()

# #### Hash table for ENSEMBL Prot and Transcript
# dic_prot_trans = {} 
# dic_trans_prot = {}
# f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
# for line in f_tmp:
#     line = line.strip()
#     if line.startswith('>'):
#         slides = line.split(' ')
#         protID = slides[0].strip()
#         protID = protID[1:]
#         protID = protID.split('.')[0].strip()

#         trans_nested = slides[4].strip()
#         transID = trans_nested.split(':')[1].strip()
#         transID = transID.split('.')[0].strip()

#         dic_prot_trans [protID] = transID
#         dic_trans_prot [transID] = protID
# f_tmp.close()


# for k,v in dic_ensembl_mis.items():
#     ensembl_T_ID = k 
#     sum_mut_count = v 
#     geneID = dic_ensembl_geneID[ensembl_T_ID]


#     try:
#         sum_mut_count += dic_ensembl_nonsense[ensembl_T_ID]
#     except:
#         pass
#     try:
#         sum_mut_count += dic_ensembl_deletion[ensembl_T_ID]
#     except:
#         pass
#     try:
#         sum_mut_count += dic_ensembl_insertion[ensembl_T_ID]
#     except:
#         pass
    
#     try:    
#         prot_ID = dic_trans_prot[ensembl_T_ID]
#         seq_length = dic_ensembl_seqLength [prot_ID]
#         mut_count_unified = sum_mut_count / seq_length

#         out_entry = geneID + ',' + ensembl_T_ID + ',' + prot_ID + ',' +\
#                     str(seq_length) + ',' + str(sum_mut_count) + ',' +\
#                     str(mut_count_unified)
#         # print(out_entry)

#         if sum_mut_count >= 1000:
#             print(out_entry)

#     except:
#         pass
#         # print(geneID)