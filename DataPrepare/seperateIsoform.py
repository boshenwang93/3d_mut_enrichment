
from multiprocessing import Process, Pool


combined_mutation_file_path =\
            '/home/bwang/tmp/Model/cancer_redundant.csv'
out_folder =\
            '/home/bwang/tmp/Model/ByGene/'


list_unique_isoform = [] 

f = open(combined_mutation_file_path, 'r')
for line in f:
    line = line.strip()
    gene = line.split(',')[0]
    if gene not in list_unique_isoform:
        list_unique_isoform.append(gene)
    
    print(len(list_unique_isoform))
f.close()


def GenerateMutationByEachGene(each_isoform):

    new_out_file_path =  out_folder +  each_isoform + ".mutation"

    outF = open(new_out_file_path, 'w')

    f = open(combined_mutation_file_path, 'r')
    for line in f:
        line = line.strip()
        gene = line.split(',')[0]

        if gene == each_isoform:
            newline = line + "\n"
            outF.write(newline)
    f.close()

    outF.close()

    return 0 



with Pool(processes= 30 ) as pool:
    pool.map(GenerateMutationByEachGene, list_unique_isoform)