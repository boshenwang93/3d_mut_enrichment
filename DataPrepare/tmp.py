# #### Hash table for ENSEMBL Prot and Transcript
# dic_prot_trans = {} 
# dic_trans_prot = {}

# f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
# for line in f_tmp:
#     line = line.strip()
#     if line.startswith('>'):
#         slides = line.split(' ')
#         protID = slides[0].strip()
#         protID = protID[1:]
#         protID = protID.split('.')[0].strip()

#         trans_nested = slides[4].strip()
#         transID = trans_nested.split(':')[1].strip()
#         transID = transID.split('.')[0].strip()

#         dic_prot_trans [protID] = transID
#         dic_trans_prot [transID] = protID
# f_tmp.close()


# #### Hash table for seqIndex => PDB residue Index 
# dic_seqIndex_PDBIndex = {}
# f = open('/mnt/data/project/cancer/Feature/RawFeature/ResidueIndexMapping.csv', 'r')
# for line in f:
#     line = line.strip()
#     slices = line.split(',')

#     seqID = slices[0]
#     wild_type = slices[1]
#     seqIndex = slices[2]

#     pdbID = slices[3]
#     pdbIndex = slices[4]

#     seq_info = seqID + '_' + wild_type + '_' + seqIndex
#     pdb_info = pdbID + '_' + pdbIndex

#     dic_seqIndex_PDBIndex [seq_info] = pdb_info
# f.close()

# # print(dic_seqIndex_PDBIndex)



# f = open('/mnt/data/project/cancer/Feature/Mut/PartPDB_unique_missenseVariant.csv', 'r')
# for line in f:
#     line = line.strip()

#     geneSymbol = line.split(',')[0].strip()
#     transID = line.split(',')[1].strip()

#     mut_pattern_nested = line.split(',')[2].strip()
#     wild_AA = mut_pattern_nested[2]
#     mut_AA = mut_pattern_nested[-1]
#     seqIndex = mut_pattern_nested[3:-1]
    
#     protID = dic_trans_prot [transID]

#     try:
#         seqIndex = int(seqIndex)    
        
#         ID_seqIndex = protID + '_' + wild_AA + '_' + str(seqIndex)
#         ID_pdbIndex = dic_seqIndex_PDBIndex [ID_seqIndex]

#         new_line = geneSymbol + ',' + protID + ',' + transID + ',' +\
#                    wild_AA + ',' + str(seqIndex) + ',' + mut_AA + ',' +\
#                    ID_pdbIndex
#         print(new_line)
#     except:
#         pass

# f.close()

import numpy as np
from collections import defaultdict 


dic_gene_alternativeProtID = defaultdict(list)

list_included_mut_type = ['Substitution - Missense', 
                          'Substitution - Nonsense', 
                          'Deletion - In frame', 
                          'Insertion - In frame']

list_all_4types_protID = []
list_unique_protID = []

COSMIC_mutation_tsv_file = '/home/bwang/data/cancer/CosmicGenomeScreensMutantExport.tsv'
f = open(COSMIC_mutation_tsv_file, 'r' ,encoding='ISO-8859-1')
for line in f:
    line = line.strip()
    
    slices_line = line.split("\t")

    gene_name = slices_line[0].strip()

    ensembl_transcriptID = slices_line[1].strip()
    ensembl_transcriptID = ensembl_transcriptID.split('.')[0].strip()

    HGNC_ID = slices_line[3].strip()
    tissue = slices_line[7].strip()
    cancer_type = slices_line[12].strip()
    sampleInfo = slices_line[4].strip()
    mutation_pattern = slices_line[20].strip()
    mutation_type = slices_line[21].strip()


    try:
        gene_name_backbone = gene_name.split('_')[0]
    except:
        gene_name_backbone = gene_name

    
    if mutation_type in list_included_mut_type:
        try:
            protID_mut = slices_line[35].strip()
            ensembl_protID_wVersion = protID_mut.split(':')[0]    
            # print(gene_name, ensembl_protID_wVersion)
            if ensembl_protID_wVersion not in dic_gene_alternativeProtID[gene_name_backbone]:
                dic_gene_alternativeProtID [gene_name_backbone] .append(ensembl_protID_wVersion)

            list_all_4types_protID.append(ensembl_protID_wVersion)

        except:
            pass
f.close()

for gene, list_protID in dic_gene_alternativeProtID.items():
    print(gene, list_protID)




# list_unique_protID = np.unique(list_all_4types_protID)

# for e in list_unique_protID:
#     print(e)


