import os 
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input_dir', type=str,
                    help='Input folder containing Euka Ref Prot fasta.gz')
parser.add_argument('-o', '--output_file_path', type=str,
                    help='Output fasta file contain all assemble Euka Prot')
args=parser.parse_args()
individual_folder = args.input_dir
assemble_fasta_path = args.output_file_path


#### Unzipping all .gz file
def Unzip_gz(in_dir):
    
    os.chdir(in_dir)
    unzip_cmd = 'gzip -d  *.gz'
    os.system(unzip_cmd)

    return 0


#### Read all individual fasta file and ASSEBBLE
def Assemble_fasta(in_dir, out_file):

    out_F = open(out_file, 'w')

    ## iteratively the whole in_dir
    for root, list_subdirs, list_files in os.walk(in_dir):
        for each_file in list_files:
            os.chdir(root)
            each_file_path = os.path.abspath(each_file)

            tmp_content = ''
            with open(each_file_path, 'r') as in_F:
                tmp_content += in_F.read()
            in_F.close()

            out_F.write(tmp_content)
            
    out_F.close()
    return 0

def main():
    
    Unzip_gz(in_dir = individual_folder)

    Assemble_fasta(in_dir = individual_folder,
                   out_file = assemble_fasta_path)
    return 0


if __name__ == "__main__":
    main()