#! /usr/bin/python3

import re
import argparse

## Set args parameters 
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--infile', type=str,
                    help = 'input combined Uniprot fasta file')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output folder which contains single fasta')
parser.add_argument('-l', '--log', type=str,
                    help = 'log file record the Exception entry')
args = parser.parse_args()

in_uniprot_fasta_path = args.infile
out_fasta_folder = args.outdir
log_file_path = args.log


#### For the fasta file containing multiple entries 
#### Seperate each single one to a single file named as Uniprot ID

def SeperateFasta(Combinated_Fasta_File, 
                  out_fasta_dir,
                  log_file):
    ## open the file handle for LOG file
    F_log = open(log_file, 'w')

    ## read whole content into a string variable 
    with open(Combinated_Fasta_File) as f:
        all_context = f.read()
    f.close()
    
    ## split each sequence context 
    list_slices = re.split('>', all_context)
    
    ## iterate each sequence 
    for each_sequence in list_slices:
        sequence_with_header = '>' + each_sequence
        list_lines_seq = re.split("\n", sequence_with_header)

        ## to obtain header and actual sequence(Remove \n)
        uniprot_header = ''
        seq_context = ''

        for eachline in list_lines_seq:
            if eachline.startswith('>') == 0 :
                if eachline != '':
                    seq_context += eachline
            else:
                uniprot_header += eachline[1:]

        ## generate the new file NAMED as uniprot_header 
        try:
            ## new file name & path 
            ####@@@@!!!! Condition 1. in uniprot Full header
            uniprot_ID = uniprot_header.split("|")[1].strip()
            
            ## Condition 2. in bare header
            # uniprot_ID = uniprot_header

            new_file_name = uniprot_ID + '.fasta'
            new_file_path = out_fasta_dir + new_file_name

            # file content 
            new_content = '>' + uniprot_ID + "\n" + seq_context
            
            # write to new file 
            out = open(new_file_path, 'w')
            out.write(new_content)
            out.close()
        except:
            error_entry = uniprot_header + "\n"
            F_log.write(error_entry)

    F_log.close()
    return 0

if __name__ == '__main__':
    SeperateFasta(Combinated_Fasta_File = in_uniprot_fasta_path,
                  out_fasta_dir = out_fasta_folder,
                  log_file = log_file_path)