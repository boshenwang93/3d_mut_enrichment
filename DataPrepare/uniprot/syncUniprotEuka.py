import argparse
import os 
from ftplib import FTP

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--outdir', type=str,
                     help='out folder to store uniprot reference')
args=parser.parse_args()
out_dir = args.outdir


#### It's Wired, Need Manually Obtain uniprot ip in US
#### Here is the ip belonging to GWU port
ftp = FTP('141.161.180.197')
ftp.login()
ftp.cwd('pub/databases/')
ftp.cwd('uniprot/current_release/knowledgebase/reference_proteomes/Eukaryota/')

list_file = ftp.nlst()

for each_file in list_file:
    local_file_path = os.path.join(out_dir, each_file)
    if each_file.endswith('.fasta.gz'):
        if each_file.endswith('additional.fasta.gz') == 0:
            if each_file.endswith('DNA.fasta.gz') == 0: 
                out_F = open(local_file_path, 'wb')
                ftp.retrbinary('RETR ' + each_file, out_F.write)
                out_F.close()
ftp.quit()