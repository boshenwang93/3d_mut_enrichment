#! /usr/bin/python3

from collections import defaultdict
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--INPUTPDB', type=str,
                    help = 'Input original PDB file')
parser.add_argument('-o', '--OUTCA', type=str,
                     help = 'output PDB pseudo CA atom to represent surface residue Mass center')
args = parser.parse_args()

input_pdb = args.INPUTPDB
out_pseudoPDB_file = args.OUTCA


###########################################################
##### Calculate the mass center for all residue ###########
##### Apr 2023 Written by B.W. ############################
########################################################### 

###########################################################
##### works for monomer ###################################
####  Calculate mass center for surface residue ###########
###########################################################

dic_atom_weight = {
    "C": 12,
    "N": 14,
    "O": 16,
    "S": 32,
}


# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}

dic_single_tri = {
    "A":  "ALA", 
    "R":  "ARG",
    "N":  "ASN",
    "D":  "ASP",
    "C":  "CYS",
    "Q":  "GLN",
    "E":  "GLU",
    "G":  "GLY",
    "H":  "HIS",
    "I":  "ILE",
    "L":  "LEU",
    "K":  "LYS",
    "M":  "MET",
    "F":  "PHE",
    "P":  "PRO",
    "S":  "SER",
    "T":  "THR",
    "W":  "TRP",
    "Y":  "TYR",
    "V":  "VAL"
}


def obtainXYZ(original_pdb_file):
    list_unique_residues = []
    dic_residue_atoms = defaultdict(list)

    dic_atomID_X = {}
    dic_atomID_Y = {}
    dic_atomID_Z = {}

    dic_atomID_elementType = {}

    f = open(original_pdb_file,'r')
    for line in f:
        line_record_identify = ""
        for i in range(0,6,1):
            line_record_identify += line[i]
        line_record_identify = line_record_identify.strip()

        if line_record_identify == "ATOM":
            # residue name
            residue = ""
            for i in range(17, 20, 1):
                residue += line[i]
            residue = residue.strip()
            residue = dic_tri_single[residue]

            # chain identifier
            chain = line[21]

            # residue sequence number
            residue_seq_number = ""
            for i in range(22, 26, 1):
                residue_seq_number += line[i]
            residue_seq_number = residue_seq_number.strip()

            # atom name
            atom = ""
            for i in range(12, 16, 1):
                atom += line[i]
            atom = atom.strip()

            residue_id = residue + "," + residue_seq_number + "," + chain
            if residue_id not in list_unique_residues:
                list_unique_residues.append(residue_id)
            
            atom_entry = residue_id + "," + atom
            dic_residue_atoms [residue_id].append(atom_entry)

            atom_type = line[77]

            #atom x, y, z
            x = ""
            for i in range(30,38,1):
                x += line[i]
            x = x.strip()
            x = float(x)

            y = ""
            for i in range(38,46,1):
                y += line[i]
            y = y.strip()
            y = float(y)

            z = ""
            for i in range(46,54,1):
                z += line[i]
            z = z.strip()
            z = float(z)

            dic_atomID_X [atom_entry] = x 
            dic_atomID_Y [atom_entry] = y
            dic_atomID_Z [atom_entry] = z
            dic_atomID_elementType [atom_entry] = atom_type
    f.close()

    return dic_residue_atoms, dic_atomID_X, dic_atomID_Y, dic_atomID_Z, dic_atomID_elementType



##### Calculate the mass center for a given Surface residue 
def calculate_mass_center( hash_residue_atoms,
                           hash_atom_X, hash_atom_Y, hash_atom_Z, 
                           hash_atom_atomType ):

    outF = open(out_pseudoPDB_file, 'w')

    for each_residue, list_itsAtom in hash_residue_atoms.items():
        
        residue_singleLetter = each_residue.split(',')[0]
        residue_seq_number = each_residue.split(',')[1]
        residue_chain = each_residue[-1]
        
        total_x = 0
        total_y = 0
        total_z = 0 

        mass_total = 0
        for each_Atom in list_itsAtom:
            
            element_type = hash_atom_atomType [each_Atom]
            mass_total += dic_atom_weight [element_type]

            total_x += hash_atom_X [each_Atom] * dic_atom_weight [element_type] 
            total_y += hash_atom_Y [each_Atom] * dic_atom_weight [element_type] 
            total_z += hash_atom_Z [each_Atom] * dic_atom_weight [element_type] 
        ## mass of surface atom is done
        
        center_x = total_x / mass_total
        center_y = total_y / mass_total
        center_z = total_z / mass_total

        ## round to 3digits
        center_x =  str(round(center_x, 3))
        center_y =  str(round(center_y, 3))
        center_z =  str(round(center_z, 3))

        # print(center_x, center_y, center_z)
## ATOM   1612  CA  PRO B   3      37.616  50.679  35.127  1.00 56.64           C

        #### Generate pseudo CA atom to represent CENTER of MASS for surface
        whitespace_0 = ''
        for i in range(0,4-len(residue_seq_number)):
            whitespace_0 += ' '

        # whitespace_1 = ''
        # for i in (0,4-len(residue_seq_number)-1):
        #     whitespace_1 += ' '

        whitespace_insertion = ''
        for i in range(0,4):
            whitespace_insertion += ' '

        whitespace_x = ''
        for i in range(0, 8-len(center_x)):
            whitespace_x += ' '
        whitespace_y = ''
        for i in range(0, 8-len(center_y)):
            whitespace_y += ' '
        whitespace_z = ''
        for i in range(0, 8-len(center_z)):
            whitespace_z += ' '
        
        pseudo_CA_entry = 'ATOM   '+ whitespace_0 + residue_seq_number + \
                          '  CA  ' + dic_single_tri[residue_singleLetter] + ' ' + residue_chain +\
                            whitespace_0 + residue_seq_number +\
                            whitespace_insertion +\
                           whitespace_x +center_x + \
                           whitespace_y +center_y + \
                           whitespace_z + center_z +  "\n"
        outF.write(pseudo_CA_entry)

    outF.close()

    return 0 


def Merge():
    dic_res_atoms, dic_atomID_X, dic_atomID_Y, dic_atomID_Z, dic_atomID_elementType =\
                obtainXYZ(original_pdb_file = input_pdb)
    
    for k,v in dic_res_atoms.items():
        print(k,v)
    calculate_mass_center( dic_res_atoms, dic_atomID_X,
                        dic_atomID_Y, dic_atomID_Z, dic_atomID_elementType)
    return 0 
Merge()
