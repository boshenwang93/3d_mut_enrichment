
dic_variant_prob = {}
f = open('/home/bwang/project/3d_mut_enrichment/Model/merged_effect_prob.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    deleterious_probability = slices[1]
    deleterious_probability = float(deleterious_probability)

    gene = slices[2]
    transcriptID = slices[4]
    wildAA = slices[5]
    seqIndex = slices[6]
    mutAA = slices[7]

    variantIndex = gene + '_' + wildAA + seqIndex + mutAA
    dic_variant_prob [variantIndex] = deleterious_probability

    # print(variantIndex)
f.close()


f = open('/mnt/data/project/cancer/GoldStandard/cmc_export.v92.tsv', 'r')
for line in f:
    line = line.strip()
    slices = line.split("\t")
    
    try:
        geneName = slices[0].strip()
        transcriptID = slices[1].strip()
        transcriptID = transcriptID.split('.')[0]

        aa_mut = slices[7].strip()
        mutAA = aa_mut[2:]

        variantID = geneName + '_' + mutAA
        # print(variantID)

        confident_tier = slices[-1].strip()
        
        if confident_tier == '2':
            # print(variantID)
            if dic_variant_prob [variantID] <= 0.5:
                print(geneName, variantID, dic_variant_prob [variantID])
    
    except:
        pass
f.close()