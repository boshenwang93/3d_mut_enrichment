

#### Hash table for residue index mapping between Seq and PDB
# dic_seqIndex_pdbIndex = {}
# f = open('/tmp/Feature/ResidueMappingIndex.csv', 'r')
# for line in f:
#     line = line.strip()
#     slices = line.split(',')

#     seqID = slices[0]
#     wild_type = slices[1]
#     seqIndex = slices[2]

#     pdbID = slices[3]
#     pdbIndex = slices[4]

#     seq_info = seqID + '_' + wild_type + '_' + seqIndex
#     pdb_info = pdbID + '_' + pdbIndex

#     dic_seqIndex_pdbIndex [seq_info] = pdb_info
# f.close()
# # print(dic_seqIndex_pdbIndex)



# #### Hash table for ENSEMBL Prot and Transcript
# dic_prot_trans = {} 
# dic_trans_prot = {}
# f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
# for line in f_tmp:
#     line = line.strip()
#     if line.startswith('>'):
#         slides = line.split(' ')
#         protID = slides[0].strip()
#         protID = protID[1:]
#         protID = protID.split('.')[0].strip()

#         trans_nested = slides[4].strip()
#         transID = trans_nested.split(':')[1].strip()
#         transID = transID.split('.')[0].strip()

#         dic_prot_trans [protID] = transID
#         dic_trans_prot [transID] = protID
# f_tmp.close()


#### Hash table for Frequency /  Mutation Recurrence
# dic_geneTrasMut_Freq = {}
# f_tmp = open('/mnt/data/project/cancer/Feature/RawFeature/missense_Frequency.csv', 'r')
# for line in f_tmp:
#     line = line.strip()
#     slides = line.split(',')
#     geneID = slides[0].strip()
#     transID = slides[1].strip()
#     mut_pattern = slides[2].strip()

#     identifier = geneID + '_' + transID + '_' + mut_pattern

#     freq = slides[3].strip()

#     dic_geneTrasMut_Freq [identifier] = freq
# f_tmp.close()

# print(dic_geneTrasMut_Freq)






#### Hash table for Entropy 
dic_seqIndex_entropy = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/omega30_mappable_entropy.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    seqID = slices[0]
    seqIndex = slices[1]

    entropy = slices[2]

    seq_info = seqID + '_' + seqIndex
    dic_seqIndex_entropy [seq_info] = entropy
f.close()

# print(dic_seqIndex_entropy)

#### Hash table for All a.a. Probability SITE-SPECIFIC
dic_residue_prob = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/omega30_mappable_prob.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    seqID = slices[0]
    wild_type = slices[2]
    seqIndex = slices[1]

    prob = slices[3]

    seq_info = seqID + '_' + wild_type + '_' + seqIndex
    dic_residue_prob [seq_info] = prob
f.close()

# print(dic_residue_prob)

# print('sequence info DONE')

# dictionary Residue Tri_Letter => One Letter
dic_tri_single = {
    "ALA": "A",
    "ARG": "R",
    "ASN": "N",
    "ASP": "D",
    "CYS": "C",
    "GLN": "Q",
    "GLU": "E",
    "GLY": "G",
    "HIS": "H",
    "ILE": "I",
    "LEU": "L",
    "LYS": "K",
    "MET": "M",
    "PHE": "F",
    "PRO": "P",
    "SER": "S",
    "THR": "T",
    "TRP": "W",
    "TYR": "Y",
    "VAL": "V"
}

#### Hash table for atom contact
dic_pdbIndex_atomContact = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/AtomContact.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    try:
        pdb_w_chain = slices[0]
        index_nested = slices[1]
        index = index_nested.split(':')[1].strip()
        pdbIndex = pdb_w_chain + '_' + index 
        
        atom_info = ''
        for i in range(2,len(slices)):
            atom_info += slices[i] + ','
        
        dic_pdbIndex_atomContact [pdbIndex] = atom_info
    except:
        pass
f.close()

#### Hash table for residue layer
dic_pdbIndex_3layer = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/Residue3layer.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    
    try:
        pdb_w_chain = slices[0]
        index_nested = slices[1]
        index = index_nested.split(':')[1].strip()
        pdbIndex = pdb_w_chain + '_' + index 
        
        layer_info = ''
        for i in range(2,len(slices)):
            layer_info += slices[i] + ','
        
        dic_pdbIndex_3layer [pdbIndex] = layer_info
    except:
        pass
f.close()


# print("Atom Connect done")

### Hash table for Salt bridge
dic_pdbIndex_saltbridge = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/SaltBridge.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    pdb_w_chain = slices[0]
    index = slices[2]
    pdbIndex = pdb_w_chain + '_' + index 

    salt = slices[-1]
    dic_pdbIndex_saltbridge [pdbIndex] = salt
f.close()

# print(dic_pdbIndex_saltbridge)

#### Hash table for SASA, location
dic_pdbIndex_SASA = {}
dic_pdbIndex_location = {} 
f = open('/mnt/data/project/cancer/Feature/RawFeature/SASA_Geo.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    pdbChain = slices[0]
    aaIndex = slices[2]
    pdbIndex = pdbChain + "_" + aaIndex 

    location = slices[-2]
    sasa = slices[-1]

    dic_pdbIndex_location [pdbIndex] = location 
    dic_pdbIndex_SASA [pdbIndex] = sasa
f.close()

# print("Other Structure DOne")



dic_mut_charge = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/Charge.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    wildAA = slices[0]
    mutAA = slices[1]
    mut_pattern = wildAA + mutAA 

    charge = slices[2]
    dic_mut_charge [mut_pattern] = charge
f.close()


dic_mut_B62 = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/B62score.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    wildAA = slices[0]
    mutAA = slices[1]
    mut_pattern = wildAA + mutAA 

    blosum = slices[2]
    dic_mut_B62 [mut_pattern] = blosum
f.close()


dic_mut_atomcharge = {}
f = open('/mnt/data/project/cancer/Feature/RawFeature/AtomChange.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')
    wildAA = slices[0]
    mutAA = slices[1]

    mut_pattern = wildAA + mutAA 

    delte_atom = ''
    for i in range(2, len(slices)):
        stringValue = str(slices[i])
        delte_atom += stringValue + ','
    
    dic_mut_atomcharge [mut_pattern] = delte_atom
f.close()

# print("change Done")


#### Original Mutation File
f = open('/mnt/data/project/cancer/Feature/PartPDB_missense_mappable.csv', 'r')
for line in f:
    line = line.strip().upper()
    slices = line.split(',')

    geneID = slices[0].strip()
    transID = slices[2].strip()
    protID = slices[1].strip()

    wild_type = slices[3].strip()
    mut_type = slices[5].strip()
    seq_index = slices[4].strip()

    pdb_seqindex_identifier = slices[-1].strip()

    identifier_entropy = protID + '_' + seq_index 
    identifier_wildAA = protID + '_' + wild_type + '_' + seq_index
    identifier_mutAA = protID + '_' + mut_type + '_' + seq_index 
    
    identifier_change = wild_type + mut_type

    # print(identifier_entropy)

    try:

        saltbridgeCount = 0 
        try:
            saltbridgeCount +=int(dic_pdbIndex_saltbridge [pdb_seqindex_identifier])
        except:
            pass 
        saltbridgeCount = str(saltbridgeCount) 

        new_line = line + ',' +\
                dic_seqIndex_entropy [identifier_entropy] + ',' +\
                dic_residue_prob [identifier_wildAA] + ',' +\
                dic_residue_prob[identifier_mutAA] + ',' +\
                dic_mut_B62 [identifier_change] + ',' +\
                dic_mut_charge [identifier_change] + ',' +\
                dic_mut_atomcharge [identifier_change] +\
                saltbridgeCount + ',' +\
                dic_pdbIndex_SASA [pdb_seqindex_identifier] + ',' +\
                dic_pdbIndex_location [pdb_seqindex_identifier] + ',' +\
                dic_pdbIndex_atomContact [pdb_seqindex_identifier] +\
                dic_pdbIndex_3layer [pdb_seqindex_identifier]
        new_line = new_line.upper()
        print(new_line)
    except:
        pass

f.close()

