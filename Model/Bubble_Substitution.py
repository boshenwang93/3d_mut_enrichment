from collections import defaultdict 

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

list_amino_acid = [
    "R",    "H",    "K",    "D",    "E",
    "S",  "T",    "N",    "Q",
    "C",  "G",    "P",
    "A",    "I",    "L",    "M",    "F",    "W",    "Y",    "V"
]


dic_mutPat_listCount = defaultdict(list)

f = open('/mnt/data/project/cancer/Specific/PanCancer/Merged_Final.csv')
for line in f:
    line = line.strip()
    slices = line.split(',')

    count_entry = slices[0]
    count_entry = int(count_entry)

    wildAA = slices[6]
    mutAA = slices[8]
    
    mut_pattern = wildAA + mutAA

    dic_mutPat_listCount [mut_pattern].append(count_entry)

f.close()


dic_mutPat_count = {}

total_freq = 0 
for k, v in dic_mutPat_listCount.items():
    sum_count = 0 
    for e in v:
        sum_count += e
        total_freq += e 
    
    dic_mutPat_count [k] = sum_count


dict_sorted =  dict(sorted(dic_mutPat_count.items(), key=lambda item: item[1],reverse=True) )
for k,v in dict_sorted.items():
    print(k,v)

# print(total_freq)
# print(dic_mutPat_count)


list_Xlabel = [] 
list_Ylabel = [] 

x = []
y = []
z = [] 
for i in range(0,20):
    for j in range(0,20):
        list_Xlabel.append(list_amino_acid[i])
        list_Ylabel.append(list_amino_acid[j])
        assigned_mutPat = list_amino_acid[i] + list_amino_acid[j]
        try: 
            freq = dic_mutPat_count[assigned_mutPat]
        except:
            freq = 0
        scaled_freq = freq / 100 

        x.append(i)
        y.append(j)
        z.append(scaled_freq)

# Change global size playing with s

#Setting the Dimentions of the Graph
plt.figure(figsize = (10,10))
#selecting the current axis
ax = plt.gca()
#sets the ratio to 5
ax.set_aspect(1)


plt.scatter(x, y, s=z)
plt.xticks(x , list_Xlabel)
plt.yticks(y, list_Ylabel)

# show the graph
plt.show()



# x = np.random.rand(40)
# y = np.random.rand(40)
# z = np.random.rand(40)

# print(z)