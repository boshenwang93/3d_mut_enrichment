import scipy.stats as ss
import matplotlib.pyplot as plt 
import numpy as np 
import os 

dict_protID_seqLength = {}
def CountSeqLength (single_fasta_file):

    seq_content = ''
    f = open(single_fasta_file, 'r')
    for line in f:
        line = line.strip()
        if line.startswith('>') == 0 :
            seq_content += line
    f.close()

    seq_length = len(seq_content)

    return seq_length

for root, subdir_list, file_list in os.walk('/mnt/data/project/cancer/CosmicV92/single_ensembl_prot/'):
    for each_file in file_list:
        file_abs_path = root + each_file 
        seq_length = CountSeqLength(file_abs_path)
        ensemblID = each_file.split(".")[0].strip()

        dict_protID_seqLength [ensemblID] = int(seq_length)

##############################################################

list_cancer_Census_gene = [] 
f  = open('/home/bwang/project/3d_mut_enrichment/Calculation/Paralog/cancer_gene_census.csv', 'r')
for line in f:
    line = line.strip()
    geneSymbol = line.split(',')[0].strip()
    if geneSymbol not in list_cancer_Census_gene:
        list_cancer_Census_gene.append(geneSymbol)
f.close()


list_cancer_index = [] 
list_mutCount = []
list_Effect = []
list_gene = [] 

list_cancer_ratio = []
list_nocancer_ratio = []

list_cancer_count = []
list_noncancer_count = [] 
f = open('gene_Level_Deleterious_Effect.csv', 'r')
i = 0 
for line in f:
    line = line.strip()
    slices = line.split(',')
    geneID = slices[0].strip()
    protID = slices[1].strip()
    raw_count = int(slices[-2])
    effect = float(slices[-1])


    list_gene.append(geneID)
    list_mutCount.append(raw_count)
    list_Effect.append(effect)
    
    ratio = effect / raw_count

    try:
        avg_mut_count = raw_count / dict_protID_seqLength[protID]

        if geneID in list_cancer_Census_gene:
            list_cancer_index.append(i)
            list_cancer_ratio.append(ratio)

            if avg_mut_count <= 3:
                list_cancer_count.append(avg_mut_count)
        else:
            list_nocancer_ratio.append(ratio)
            if avg_mut_count <= 3:
                list_noncancer_count.append(avg_mut_count)
        i += 1 
    except:
        pass
f.close()



data_to_plot = [list_cancer_count, list_noncancer_count]

# Create a figure instance
fig = plt.figure(1, figsize=(9, 6))

# Create an axes instance
ax = fig.add_subplot(111)

# Create the boxplot
bp = ax.boxplot(data_to_plot)

ax.set_xticklabels(['Known Cancer Gene',  'Gene Not Classified as Cancer Census'])

# Save the figure
fig.savefig('fig4.png', bbox_inches='tight')



print(ss.kstest(list_noncancer_count, list_cancer_count,alternative='greater'))



list_ranking_rawCount = ss.rankdata(list_mutCount)
list_ranking_effect = ss.rankdata(list_Effect)

sum_rawcount_rank = 0
sum_effect_rank = 0 
for e in list_cancer_index:
    sum_rawcount_rank += list_ranking_rawCount[e]
    sum_effect_rank += list_ranking_effect[e]

# print(sum_rawcount_rank)
# print(sum_effect_rank)




