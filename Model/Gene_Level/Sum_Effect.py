

# f = open('cosmic_missense_effect.csv', 'r')
# for line in f:
#     line = line.strip()
#     slices = line.split(',')

#     protID = slices[1].strip()
#     protID = protID[1:-1]

#     geneID = slices[2].strip()
#     geneID = geneID[1:-1]

#     mut_pattern = slices[4][1:-1] + slices[5] + slices[6][1:-1]
    
#     mut_count = slices[9].strip()
#     mut_count = int(mut_count)

#     pdbIndex = slices[10].strip()[1:-1]

#     deleterous_effect = slices[-2].strip()
#     deleterous_effect = float(deleterous_effect)
    
#     sum_effect = mut_count * deleterous_effect 

#     out_entry = protID + ',' + geneID + ',' + pdbIndex + ',' + mut_pattern + ',' +\
#                 str(mut_count) + ',' + str(deleterous_effect) + ',' +\
#                 str(sum_effect)
#     print(out_entry)
# f.close()


list_unique_gene = []
f = open('cosmic_missense_sumEffect.csv', 'r')
for line in f:
    line = line.strip()
    slices = line.split(',')

    geneID = slices[1].strip()
    if geneID not in list_unique_gene:
        list_unique_gene.append(geneID)
f.close()


for each_geneID in list_unique_gene:

    gene_deleterious_score = 0 
    gene_mut_count = 0 

    f = open('cosmic_missense_sumEffect.csv', 'r')
    for line in f:
        line = line.strip()
        slices = line.split(',')

        protID = slices[0].strip()
        geneID = slices[1].strip()

        if geneID == each_geneID:
            single_mut_sumEffect = slices[-1].strip()
            single_mut_sumEffect = float(single_mut_sumEffect)

            single_mut_count = slices[-3].strip()
            single_mut_count = int(single_mut_count)

            gene_deleterious_score += single_mut_sumEffect
            gene_mut_count += single_mut_count
    f.close()

    out_entry = each_geneID + ',' + protID + ',' + str(gene_mut_count) +','+\
                str(gene_deleterious_score)
    print(out_entry)