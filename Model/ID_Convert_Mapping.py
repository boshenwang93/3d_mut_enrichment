
from collections import defaultdict

#### Hash table for ENSEMBL Prot and Transcript
dic_prot_trans = {} 
dic_trans_prot = {}
dic_prot_geneSymbol = {}
dic_geneSymbol_listProt = defaultdict(list)

f_tmp = open('/home/bwang/project/3d_mut_enrichment/DataPrepare/COSMIC/trash/ensembl_pep.fa', 'r')
for line in f_tmp:
    line = line.strip()
    if line.startswith('>'):
        slides = line.split(' ')
        protID = slides[0].strip()
        protID = protID[1:]
        protID = protID.split('.')[0].strip()

        trans_nested = slides[4].strip()
        transID = trans_nested.split(':')[1].strip()
        transID = transID.split('.')[0].strip()

        geneSymbol_nested = slides[7].strip()
        geneSymbol = geneSymbol_nested.split(':')[1].strip()


        dic_prot_trans [protID] = transID
        dic_trans_prot [transID] = protID
        dic_prot_geneSymbol [protID] = geneSymbol
        dic_geneSymbol_listProt [geneSymbol] .append(protID)
f_tmp.close()



#########################################################
##### For Full Coverage PDB #############################
#########################################################

# list_FullprotID = [] 
# dic_protID_sumIden = {} 
# f = open('/mnt/data/project/cancer/searchPDB/Full_Coverage_randomOne.csv', 'r')
# for line in f:
#     line = line.strip()
#     protID = line.split(',')[0].strip()
#     protID = protID[1:]
#     if protID not in list_FullprotID:
#         list_FullprotID.append(protID)
    
#     idenSeq = line.split(',')[2].strip()
#     idenPDB = line.split(',')[3].strip()
#     idenPDB = float(idenPDB)
#     idenSeq = float(idenSeq)
#     sumIden = idenPDB + idenSeq
#     dic_protID_sumIden [protID] = sumIden
# f.close()


# list_gene_FullPDB = []
# for each_protID in list_FullprotID:
#     gene = dic_prot_geneSymbol [each_protID]
#     if gene not in list_gene_FullPDB:
#         list_gene_FullPDB.append(gene)


# dic_gene_BestIsoform = defaultdict(list) 
# for each_gene in list_gene_FullPDB:
#     list_sumIden = [] 
#     for eachProt in dic_geneSymbol_listProt [each_gene]:
#         try:
#             sumIden = dic_protID_sumIden [eachProt]
#             list_sumIden.append(sumIden)
#         except:
#             pass
    
#     max_sumIden = max(list_sumIden)

#     for eachProt in dic_geneSymbol_listProt [each_gene]:
#         try:
#             sumIden = dic_protID_sumIden [eachProt]        
#             if sumIden == max_sumIden:
#                 dic_gene_BestIsoform [each_gene].append(eachProt)
#             else:
#                 pass
#                 # print(eachProt, 'discarded')
#         except:
#             pass

# for gene, listProtID in dic_gene_BestIsoform.items():
#     chosen_isoform = listProtID[0]

#     f = open('/mnt/data/project/cancer/searchPDB/Full_Coverage_randomOne.csv', 'r')
#     for line in f:
#         line = line.strip()
#         protID = line.split(',')[0].strip()
#         protID = protID[1:]

#         if chosen_isoform == protID:
#             new_line = gene + ',' + line 
#             # print(new_line)
#     f.close()




#########################################################
##### For Part Coverage PDB #############################
#########################################################
list_PartprotID = []
dic_protID_iden = defaultdict(list)
f = open('/mnt/data/project/cancer/searchPDB/Part_Coverage_random.csv', 'r')
for line in f:
    line = line.strip()
    protID = line.split(',')[0].strip()
    protID = protID[1:]
    if protID not in list_PartprotID:
        if protID not in list_PartprotID:
            list_PartprotID.append(protID)
        else:
            pass
    
    idenSeq = line.split(',')[2].strip()
    idenPDB = line.split(',')[3].strip()
    idenPDB = float(idenPDB)
    idenSeq = float(idenSeq)
    sumIden = idenPDB + idenSeq
    dic_protID_iden [protID].append(sumIden)
f.close()


dic_prot_AllCoveredIden = {}
for each_prot in list_PartprotID:
    sum_cover = 0 
    list_sumiden = dic_protID_iden [each_prot]
    for e in list_sumiden:
        sum_cover += e 
    
    dic_prot_AllCoveredIden [each_prot] = sum_cover


list_gene_PartPDB = [] 
for each_protID in list_PartprotID:
    gene = dic_prot_geneSymbol [each_protID]
    if gene not in list_gene_PartPDB:
        list_gene_PartPDB.append(gene)


dic_gene_BestIsoform = defaultdict(list) 
for each_gene in list_gene_PartPDB:
    list_sumIden = [] 
    for eachProt in dic_geneSymbol_listProt [each_gene]:
        try:
            sumIden = dic_prot_AllCoveredIden [eachProt]
            list_sumIden.append(sumIden)
        except:
            pass
    
    max_sumIden = max(list_sumIden)

    for eachProt in dic_geneSymbol_listProt [each_gene]:
        try:
            sumIden = dic_prot_AllCoveredIden [eachProt]        
            if sumIden == max_sumIden:
                dic_gene_BestIsoform [each_gene].append(eachProt)
            else:
                pass
                # print(eachProt, 'discarded')
        except:
            pass


for gene, listProtID in dic_gene_BestIsoform.items():
    chosen_isoform = listProtID[0]

    f = open('/mnt/data/project/cancer/searchPDB/Part_Coverage_random.csv', 'r')
    for line in f:
        line = line.strip()
        protID = line.split(',')[0].strip()
        protID = protID[1:]

        if chosen_isoform == protID:
            new_line = gene + ',' + line 
            print(new_line)
    f.close()