
# f = open('COSMIC_missenseEffect_FullPDB.csv', 'r')
# for line in f:
#     line = line.strip()
#     if line.startswith('Rindex') == 0:
#         print(line)
# f.close()

i = 0 
list_protein_unique = []
list_unique_PDB= [] 
f = open('merged_effect_prob.csv', 'r')
for line in f:
    line = line.strip()
    gene = line.split(',')[2]
    if gene not in list_protein_unique:
        list_protein_unique.append(gene)
    pdbChain = line.split(',')[8]
    # pdbChain = pdbChain[0:6]
    if pdbChain not in list_unique_PDB:
        list_unique_PDB.append(pdbChain)
    i+=1
    # print(gene, pdbChain)
f.close()
print(len(list_protein_unique), len(list_unique_PDB))


# import matplotlib.pyplot as plt
# from matplotlib import colors
# from matplotlib.ticker import PercentFormatter

# list_ppph2_prob = []
# f = open('pph2-short.txt', 'r')
# for line in f:
#     line = line.strip()
#     slices = line.split("\t")
    
#     try:
#         prob = slices[10].strip()
#         prob = float(prob)
#         list_ppph2_prob.append(prob)
#     except:
#         pass
# f.close()


# n_bins = 100
# plt.hist(list_ppph2_prob, bins = n_bins)
# plt.title("Polyphen-2 Score Distribution")
# plt.show()

