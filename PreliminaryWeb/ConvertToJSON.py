import os 
import json 

######################################################
#### Convert the result to JSON format 
###################################################

out_folder = '/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/json/'

######################################
#### For tissue => Driver Genes 
#####################################
for root, subdirs, file_list in os.walk('/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/genes/'):
    for eachfile in file_list:
        if eachfile.endswith('rankedMut'):
            os.chdir(root)
            tissueName = eachfile.split('.rankedMut')[0]
            list_driver_genes = [] 

            json_content = "{" 

            f = open(eachfile, 'r')
            i = 0 
            for line in f:
                line = line.strip()
                i += 1
                chunks = line.split(',')
                geneName = chunks[0]
                novel_known = chunks[1]

                geneRank = "#" + str(i) +" ranked Driver Gene" 

                current_entry = '"'+ geneRank + '"' + ":{" +\
                                 '"gene Symbol":' + '"' + geneName + '",' +\
                                 '"Novel Cancer gene?":' + '"' + novel_known + '"' +\
                                  "},"
                json_content +=  current_entry
            f.close()
            json_content = json_content[0:-1] +  "}"
            
            # json_content = json.dumps(json_content)
            
            # outJson_path = out_folder + tissueName + '.json'
            # outF = open(outJson_path, 'w')
            # outF.write(json_content)
            # outF.close()


######################################
#### For genes => Driver mutations 
#####################################

for root, subdirs, file_list in os.walk('/mnt/data/project/cancer/Specific/tissueSpecific/Ranked/addedCluster/'):
    for eachfile in file_list:
        if eachfile.endswith('rankedMut'):
            os.chdir(root)
            tissueName = eachfile.split('.rankedMut')[0]
            list_driver_genes = [] 

            json_content = "{" 

            f = open(eachfile, 'r')
            i = 0 
            for line in f:
                line = line.strip()

                i += 1
                chunks = line.split(',')
                ranked_percent = chunks[0]
                gene_protID_transID_mutPat= chunks[3]
                PDB_index = chunks[4]

                geneSymbol = gene_protID_transID_mutPat.split('_')[0]
                protID = gene_protID_transID_mutPat.split('_')[1]
                mutPattern = gene_protID_transID_mutPat.split('_')[-1]

                mutationRank = "#" + str(i) +" ranked mutation" 
                
                driver_results = '' 
                ranked_percent = float(ranked_percent)
                if ranked_percent <= 5:
                    driver_results += 'Yes, driver'
                else:
                    driver_results += 'No, passenger'


                current_entry = '"'+ mutationRank + '"' + ":{" +\
                                 '"gene Symbol":' + '"' + geneSymbol + '",' +\
                                 '"protein Isoform":' + '"' + protID + '",' +\
                                 '"mutation pattern":' + '"' + mutPattern + '",' +\
                                 '"PDB_index":' + '"' + PDB_index + '",' +\
                                 '"Driver Mutation?":' + '"' + driver_results + '"' +\
                                  "},"
                json_content +=  current_entry
            f.close()
            json_content = json_content[0:-1] +  "}"
            
            # json_content = json.dumps(json_content)
            print(json_content)

            outJson_path = out_folder + tissueName + '.json'
            outF = open(outJson_path, 'w')
            outF.write(json_content)
            outF.close()