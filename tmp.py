import os
from multiprocessing import Process, Pool


COSMIC_mutation_tsv_file = '/home/bwang/data/cancer/CosmicGenomeScreensMutantExport.tsv'

out_file_folder = '/home/bwang/data/cancer/mutation/pancancer/eachIsoform/'


list_considered_ensemblID = []
for root, subdirs, files in os.walk('/home/bwang/data/cancer/calculation/MSA_value'):
    for each_file in files:
        if each_file.endswith('prob'):
            ensembl_ID = each_file.split('.prob')[0]
            ensembl_ID_wo_version = ensembl_ID.split('.')[0]
            if ensembl_ID_wo_version not in list_considered_ensemblID:
                list_considered_ensemblID.append(ensembl_ID_wo_version)

for e in list_considered_ensemblID:
    print(e)